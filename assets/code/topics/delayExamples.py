from mojo.subscriber import Subscriber, WindowController
from vanilla import FloatingWindow, TextBox


class YourTool(Subscriber, WindowController):

    debug = True

    def build(self):
        self.w = FloatingWindow((160, 37), "")
        self.w.txt = TextBox((10, 10, -10, 17), "Observing stuff", alignment='center')

    def started(self):
        self.w.open()

    ## -- CALLBACK DELAYS EXAMPLES -- ##

    currentGlyphDidChangeDelay = 2   # seconds
    def currentGlyphDidChange(self, info):
        print('currentGlyphDidChange', info)

    fontLayersDidRemoveLayerDelay = 2   # seconds
    def fontLayersDidRemoveLayer(self, info):
        print('fontLayersDidRemoveLayer', info)

    spaceCenterDidChangeSelectionDelay = 2   # seconds
    def spaceCenterDidChangeSelection(self, info):
        print('spaceCenterDidChangeSelection', info)


if __name__ == '__main__':
    YourTool(currentGlyph=True)
