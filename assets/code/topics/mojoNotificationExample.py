from mojo.events import addObserver

class MyInterface:

    def __init__(self):
        addObserver(self, "currentGlyphChangedCallback", "currentGlyphChanged")

    def currentGlyphChangedCallback(self, notification):
        glyph = notification['glyph']
        print("current glyph is '%s'" % glyph.name)

MyInterface()