from mojo.subscriber import Subscriber, WindowController
from datetime import datetime
from vanilla import FloatingWindow, TextBox

class MouseFollower(Subscriber, WindowController):

    debug = True

    def build(self):
        self.w = FloatingWindow((220, 37), "")
        self.w.txt = TextBox((10, 10, -10, 17), "Check the output window!", alignment='center')

    def started(self):
        self.w.open()

    def currentGlyphDidChange(self, info):
        glyph = info['glyph']
        now = datetime.now()
        print(f'{now:%Y-%m-%d %H:%M:%S %f} – {glyph} did change!')


if __name__ == '__main__':
    MouseFollower(currentGlyph=True)
