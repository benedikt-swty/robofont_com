from mojo.subscriber import Subscriber, WindowController
from datetime import datetime
from vanilla import FloatingWindow, TextBox


class AutoSaver(Subscriber, WindowController):

    debug = True

    def build(self):
        self.w = FloatingWindow((160, 37), "")
        self.w.txt = TextBox((10, 10, -10, 17),
                             "Autosaving... 💽",
                             alignment='center')

    def started(self):
        self.w.open()

    currentGlyphDidChangeDelay = 30

    def currentGlyphDidChange(self, info):
        glyph = info['glyph']
        glyph.font.save()
        now = datetime.now()
        print(f'{now:%Y-%m-%d %H:%M:%S} – AUTOSAVED!')


if __name__ == '__main__':
    AutoSaver(currentGlyph=True)
