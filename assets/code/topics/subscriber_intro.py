from mojo.subscriber import Subscriber, WindowController
from vanilla import FloatingWindow, TextBox

BASE_2_ACCENTED = {
    'a': ('agrave', 'aacute', 'acircumflex', 'atilde'),
}
class MarginsListener(Subscriber, WindowController):
    debug = True

    def build(self):
        self.w = FloatingWindow((160, 37), "")
        self.w.txt = TextBox((10, 10, -10, 17), "I am listening", alignment='center')

    def started(self):
        self.w.open()

    def currentGlyphMetricsDidChange(self, info):
        glyph = info['glyph']
        if glyph.name in BASE_2_ACCENTED:
            for eachGlyphName in BASE_2_ACCENTED[glyph.name]:
                glyph.font[eachGlyphName].leftMargin = glyph.leftMargin
                glyph.font[eachGlyphName].rightMargin = glyph.rightMargin


if __name__ == '__main__':
    MarginsListener(currentGlyph=True)