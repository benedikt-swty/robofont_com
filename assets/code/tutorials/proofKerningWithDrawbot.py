myFont = CurrentFont()
flatKerning = myFont.getFlatKerning()

myFont.testInstall(decompose=True, checkOutlines=True)
fontName = f'{myFont.info.familyName}-{myFont.info.styleName}'

newPage(1500, 1000)
translate(100, 100)
font(fontName, 1000)

pair = ('A', 'T')
correction = flatKerning[pair]

first, second = pair
fill(1, 0, 0, .4)
rect(myFont[first].width,
     myFont.info.descender,
     correction,
     myFont.info.unitsPerEm)

fill(0)
text(f"{first}{second}", (0, 0))
