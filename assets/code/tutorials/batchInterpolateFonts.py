import os
from vanilla.dialogs import getFolder

# get masters and destination folder
font1 = OpenFont()
font2 = OpenFont()
folder = getFolder("Select a folder to save the interpolations")[0]

# instance names and interpolation factors
instances = [
    ("Light", 0.25),
    ("Regular", 0.5),
    ("Bold", 0.75),
]

# generate instances
print('generating instances...\n')
for instance in instances:
    styleName, factor = instance
    print("\tgenerating %s (%s)..." % (styleName, factor))

    # make a new font
    dst = NewFont()

    # interpolate the glyphs
    dst.interpolate(factor, font1, font2)

    # interpolate the kerning
    # comment this line out of you're just testing
    # dst.kerning.interpolate(font1.kerning, font2.kerning, value)

    # set font name
    dst.info.familyName = "MyFamily"
    dst.info.styleName = styleName
    dst.changed()

    # save instance
    fileName = '%s_%s.ufo' % (dst.info.familyName, dst.info.styleName)
    ufoPath = os.path.join(folder, fileName)
    print('\tsaving instances at %s...' % ufoPath)
    dst.save(ufoPath)

    # done with instance
    dst.close()
    print()

print('...done.\n')