size(1000, 200)

# define two rgb colors
color1 = 0.5, 0.0, 0.3
color2 = 1.0, 0.6, 0.1

# total number of steps
steps = 12

# initial position
x, y = 0, 0

# calculate size for steps
w = width() / steps
h = height()

# iterate over the total amount of steps
for i in range(steps):

    # calculate interpolation factor for this step
    factor = i * 1.0 / (steps - 1)

    # interpolate each rgb channel separately
    r = color1[0] + factor * (color2[0] - color1[0])
    g = color1[1] + factor * (color2[1] - color1[1])
    b = color1[2] + factor * (color2[2] - color1[2])

    # draw a rectangle with the interpolated color
    fill(r, g, b)
    stroke(r, g, b)
    rect(x, y, w, h)

    # increase x-position for the next step
    x += w