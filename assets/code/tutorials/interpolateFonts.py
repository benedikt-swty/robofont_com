# get fonts
font1 = OpenFont()
font2 = OpenFont()

# define interpolation factor
factor = 0.5

# make destination font
font3 = NewFont()

# this interpolates the glyphs
font3.interpolate(factor, font1, font2)

# this interpolates the kerning
# comment this line out of you're just testing
font3.kerning.interpolate(font1.kerning, font2.kerning, factor)

# done!
font3.changed()