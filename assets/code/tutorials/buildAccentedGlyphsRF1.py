# get current font
font = CurrentFont()

# a dictionary of glyph constructions
accentsDict = {
    # accented glyph : [base glyph, [(accent, anchor)]],
    'agrave' : ['a', [('grave', 'top')]],
    'aacute' : ['a', [('acute', 'top')]],
    # ...add more accented glyphs here...
}

# iterate over all accented glyphs
for accentedGlyph in accentsDict.keys():

    # get base glyph and accents/anchors
    baseGlyph, accents = accentsDict[accentedGlyph]

    # build accented glyph using components
    font.compileGlyph(accentedGlyph, baseGlyph, accents)