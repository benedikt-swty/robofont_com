import webbrowser
import urllib
import json
import os
import ssl

import vanilla

from mojo.extensions import ExtensionBundle


mechanicRegistry = "https://robofontmechanic.com/api/v2/registry.json"
response = urllib.request.urlopen(
    mechanicRegistry,
    timeout=5,
    context=ssl._create_unverified_context()
)
data = json.loads(response.read())
mechanicExtensions = [os.path.basename(item["extensionPath"]) for item in data["extensions"]]


instructionText = """Thanks for downloading and launching the Mechanic Extensions Survey!

We are asking for your help to get a better picture of the RoboFont public extension ecosystem.

Roughly 70% of Mechanic extensions need an update to run on RoboFont 4. That’s a lot of work, and thanks to this survey we’ll know what we should prioritize."""


def stringToAttribute(value):
    attr = value.lower()
    for search in (" ", "-", ".robofontext"):
        attr = attr.replace(search, "")
    return attr


class ExtensionSurvey:

    def __init__(self):

        self.survey = vanilla.Group((0, 0, 0, 0))
        self.extensionAttributes = []
        top = 10
        self.survey.notUsed = vanilla.TextBox((200, top, 200, 22), "Never Used It", sizeStyle="mini")
        self.survey.everyDay = vanilla.TextBox((-200, top, -10, 22), "Every Day", sizeStyle="mini", alignment="right")
        top += 20
        middle = 210
        for extensionFileName in  ExtensionBundle.allExtensions():
            if extensionFileName not in mechanicExtensions:
                continue
            extensionName = ExtensionBundle(extensionFileName).name
            attr = stringToAttribute(extensionFileName)
            self.extensionAttributes.append(attr)
            obj = vanilla.TextBox((10, top, middle, 22), f"{extensionName}:", alignment="right")
            setattr(self.survey,f"{attr}_text", obj)
            obj = vanilla.Slider((middle + 20, top, -10, 22), minValue=0, maxValue=6, value=3, tickMarkCount=7, stopOnTickMarks=True, sizeStyle="small")
            setattr(self.survey,f"{attr}_slider", obj)

            top += 25

        self.w = vanilla.Window((400, min((top, 400))), "Extension Usage Survey", minSize=((400, min((top, 300)))))

        view = vanilla.Group((0, 0, 0, 0))
        view.wrappedView = self.survey
        view.getNSView().setFlipped_(True)
        view.setPosSize((0, 0, 0, top))
        self.survey.setPosSize((0, 0, 0, top))
        self.w.scroll = vanilla.ScrollView((0, 0, 0, -40), view.getNSView(), drawsBackground=False, hasHorizontalScroller=False)
        self.w.scroll.getNSScrollView().documentView().scrollPoint_((0, 0))

        self.w.send = vanilla.Button((10, -30, -10, 22), "Send Survey", callback=self.sendCallback)
        self.w.open()

        self.sheet = vanilla.Sheet((300, 300), parentWindow=self.w)
        self.sheet.text = vanilla.TextBox((10, 10, -10, -30), instructionText)
        self.sheet.button = vanilla.Button((-70, -30, -10, 22), "OK", self.sheetOKCallback)
        self.sheet.open()

    def sheetOKCallback(self, sender):
        self.sheet.close()

    def sendCallback(self, sender):
        text = []

        for attr in self.extensionAttributes:
            name = getattr(self.survey, f"{attr}_text").get()
            value = int(getattr(self.survey, f"{attr}_slider").get())

            text.append(f"{name}\t{value}")

        text = "\n".join(text)

        webbrowser.open(f'mailto:survey@robofont.com?subject=Extension Survey&body={text.encode("ascii", "ignore").decode()}', new=1)


ExtensionSurvey()
