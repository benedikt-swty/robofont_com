import vanilla
from defconAppKit.controls.glyphLineView import GlyphLineView
from mojo.subscriber import Subscriber, registerCurrentGlyphSubscriber
from mojo.roboFont import CurrentGlyph, AllFonts

class MultiFontPreview(Subscriber):

    debug = True

    def build(self):
        self.w = vanilla.Window((400, 400), minSize=(100, 100))
        self.w.glyphLineView = GlyphLineView(
            posSize=(0, 0, 0, 0),
            pointSize=None,
            autohideScrollers=False,
            showPointSizePlacard=True)

    def started(self):
        self.update(CurrentGlyph())
        self.w.open()

    def roboFontDidSwitchCurrentGlyph(self, info):
        self.update(CurrentGlyph())

    def update(self, glyph):
        if glyph is None:
            glyphs = []
        else:
            glyphName = glyph.name
            glyphs = [font[glyphName].naked()
                      for font in AllFonts()
                      if glyphName in font]
        self.w.glyphLineView.set(glyphs)


if __name__ == '__main__':
    registerCurrentGlyphSubscriber(MultiFontPreview)
