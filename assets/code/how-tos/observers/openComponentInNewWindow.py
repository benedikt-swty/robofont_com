from mojo.UI import OpenGlyphWindow
from mojo.subscriber import Subscriber, registerGlyphEditorSubscriber

class BaseGlyphJumper(Subscriber):

    debug = True

    def glyphEditorDidKeyDown(self, info):
        # do nothing if the pressed character is not 'C'
        if not info["deviceState"]["keyDown"] == "C":
            return

        # get the current glyph
        glyph = info["glyph"]

        # get the parent font
        font = glyph.font

        # loop over all components the glyph
        for component in glyph.components:

            # skip components which are not selected
            if not component.selected:
                continue

            # skip if the component’s base glyph is not in the font
            if component.baseGlyph not in font:
                continue

            # get the component’s base glyph
            baseGlyph = font[component.baseGlyph]

            # open the base glyph in a new glyph window
            OpenGlyphWindow(baseGlyph, newWindow=True)


if __name__ == '__main__':
    registerGlyphEditorSubscriber(BaseGlyphJumper)