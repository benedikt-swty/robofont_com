from mojo.subscriber import Subscriber, registerRoboFontSubscriber


class CustomFontOverviewContextualMenu(Subscriber):

    debug = True

    def fontOverviewWantsContextualMenuItems(self, info):
        myMenuItems = [
            ("option 1", self.option1Callback),
            ("option 2", self.option2Callback),
            ("submenu", [("option 3", self.option3Callback)])
        ]
        print(info)

        info["additionContextualMenuItems"].extend(myMenuItems)

    def option1Callback(self, sender):
        print("option 1 selected")

    def option2Callback(self, sender):
        print("option 2 selected")

    def option3Callback(self, sender):
        print("option 3 selected")


if __name__ == '__main__':
    registerRoboFontSubscriber(CustomFontOverviewContextualMenu)
