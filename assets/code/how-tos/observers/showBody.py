from vanilla import CheckBox
from mojo.UI import getDefault
from mojo.roboFont import CurrentGlyph
from mojo.subscriber import Subscriber, registerGlyphEditorSubscriber


class DrawBox(Subscriber):

    debug = True

    # read some user settings for use later
    color = getDefault("glyphViewMarginColor")
    height = getDefault("glyphViewDefaultHeight") / 2.0
    useItalicAngle = getDefault("glyphViewShouldUseItalicAngleForDisplay")

    def build(self):
        glyphEditor = self.getGlyphEditor()
        self.container = glyphEditor.extensionContainer(
            identifier="com.roboFont.DrawBox.background",
            location="background",
            clear=True)

        self.boxLayer = self.container.appendRectangleSublayer(
            position=(0, 0),
            fillColor=(1, 1, 0, 0.25)
        )
        self.setBox()

        self.drawCheck = CheckBox((-120, -32, 120, 22),
                                  "Draw Box",
                                  callback=self.drawCheckCallback)
        glyphEditor.addGlyphEditorSubview(
            view=self.drawCheck,
            identifier="com.roboFont.DrawBox.drawCheckBox"
        )

    def destroy(self):
        self.container.clearSublayers()

    def setBox(self):
        glyph = CurrentGlyph()

        self.boxLayer.setPosition((0, glyph.font.info.descender))
        self.boxLayer.setSize((glyph.width, glyph.font.info.unitsPerEm))

        if self.useItalicAngle:
            angle = glyph.font.info.italicAngle
            if angle is not None:
                print('transforming...')
                self.boxLayer.addSkewTransformation((-angle, 0))

    def drawCheckCallback(self, sender):
        if sender['state']:
            self.boxLayer.setVisible(True)
            self.setBox()
        else:
            self.boxLayer.setVisible(False)

    def glyphEditorDidSetGlyph(self, info):
        self.setBox()


if __name__ == '__main__':
    registerGlyphEditorSubscriber(DrawBox)
