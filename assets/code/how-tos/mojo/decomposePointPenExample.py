from mojo.pens import DecomposePointPen

f = CurrentFont()

# get a source glyph with components
srcGlyph = f['etilde']

# create a new empty glyph as destination for the decomposed copy
dstGlyph = RGlyph()

# copy the width of the source glyph to the destination glyph
dstGlyph.width = srcGlyph.width

# get a pen to draw into the destination glyph
dstPen = dstGlyph.getPointPen()

# create a decompose pen which writes its result into the destination pen
decomposePen = DecomposePointPen(f, dstPen)

# draw the source glyph into the decompose pen
# which draws into the destination pen
# which draws into the destination glyph
srcGlyph.drawPoints(decomposePen)

# insert the decomposed glyph into the font under a different name
f.insertGlyph(dstGlyph, name='test')
f['test'].markColor = 1, 0, 0, 1