from mojo.UI import AllSpaceCenters, CurrentSpaceCenter
from mojo.subscriber import Subscriber, registerSpaceCenterSubscriber
from vanilla import CheckBox

cachedSettings = {}

class SynchronizeSpaceCenters(Subscriber):

    debug = True

    def spaceCenterDidOpen(self, info):
        sp = CurrentSpaceCenter()
        if not sp:
            print("sorry no space center!")
            return
        sp.synchronizeToggle = CheckBox((-22, -22, 22, 22),
                                        title=None,
                                        callback=self.synchronizeCheckBoxCallback,
                                        value=True)

    def spaceCenterDidChangeText(self, info):
        self.synchronize()

    def synchronizeCheckBoxCallback(self, sender):
        state = sender.get()

        # synchronize checkboxes
        for sc in AllSpaceCenters():
            sc.synchronizeToggle.set(state)

        # synchronize space centers
        if state:
            self.synchronize()

    def synchronize(self):
        sp = CurrentSpaceCenter()

        # no space center open
        if not sp:
            return

        # space center does not have a synchronize checkbox
        if not sp.synchronizeToggle:
            return

        # space center synchronization is turned off
        if not sp.synchronizeToggle.get():
            return

        # collect new settings
        settings = {
            'text':       sp.getRaw(),
            'before':     sp.getPre(),
            'after':      sp.getAfter(),
            'suffix':     sp.getSuffix(),
            'fontSize':   sp.getPointSize(),
            'lineHeight': sp.getLineHeight(),
        }

        # stop if the settings haven't changed
        global cachedSettings
        if settings == cachedSettings:
            return

        # apply settings to all other space centers
        for sc in AllSpaceCenters():
            if sc == sp:
                continue
            sc.setRaw(settings['text'])
            sc.setPre(settings['before'])
            sc.setAfter(settings['after'])
            sc.setSuffix(settings['suffix'])
            sc.setPointSize(settings['fontSize'])
            sc.setLineHeight(settings['lineHeight'])

        # save settings for next time
        cachedSettings = settings


if __name__ == '__main__':
    registerSpaceCenterSubscriber(SynchronizeSpaceCenters)
