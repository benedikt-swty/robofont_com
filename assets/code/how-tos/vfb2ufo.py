from subprocess import Popen

# path to vfb2ufo on your machine
ufo2vfbLocation = "/usr/local/bin/vfb2ufo"

# path to input VFB file
vfbPath = u"/myFolder/myFont.vfb"

# path to output UFO file
ufoPath = u"/anotherFolder/myFont.ufo"

# call the vfb2ufo program
p = Popen([ufo2vfbLocation, vfbPath, ufoPath]) # "-64", etc
p.wait()

# add a note to the converted UFO
f = OpenFont(ufoPath, showInterface=False)
f.info.note = 'converted from vfb with vfb2ufo'
f.close(save=True)