---
layout: page
title: Downloads
menuOrder: 6
---

Current release
---------------

<a class="buy-button" href="https://static.typemytype.com/robofont/RoboFont.dmg">download</a>
<a class="buy-button" href="{{ site.purchaselink }}">buy</a>

<table>
  <tr>
    <th>version</th>
    <th width='33%'>build</th>
    <th width='33%'>date</th>
  </tr>
  <tr>
    {% assign currentRelease = site.data.downloads.release[0] %}
    <td>RoboFont {{ currentRelease.version }}</td>
    <td><code><a href='{{ currentRelease.url }}'>{{ currentRelease.build }}</a></code></td>
    <td><code>{{ currentRelease.date | date: "%d %b %Y" }}</code></td>
  </tr>
</table>

{% assign betaCount = site.data.downloads.beta | size %}

{% if betaCount > 0 %}

Beta releases
-------------

{% assign currentBeta = site.data.downloads.beta[0] %}

<a class="buy-button" href="{{ currentBeta.url }}">download latest beta</a>

<table>
  <tr>
    <th>version</th>
    <th width='33%'>build</th>
    <th width='33%'>date</th>
  </tr>
  {% assign betas = site.data.downloads.beta | sort: "build" %}
  {% for release in betas reversed %}{% if forloop.index < 2 %}
  <tr>
    <td>RoboFont {{ release.version }}</td>
    <td><code><a href='{{ release.url }}'>{{ release.build }}</a></code></td>
    <td><code>{{ release.date | date: "%d %b %Y" }}</code></td>
  </tr>
  {% endif %}{% endfor %}
</table>

{% endif %}

Previous releases
-----------------

<table>
  <tr>
    <th>version</th>
    <th width='33%'>build</th>
    <th width='33%'>date</th>
  </tr>
  {% assign releases = site.data.downloads.release | sort: "build" %}
  {% for release in releases reversed %}
  {% if forloop.first == false %}
  <tr>
    <td>RoboFont {{ release.version }}</td>
    <td><code><a href='{{ release.url }}'>{{ release.build }}</a></code></td>
    <td><code>{{ release.date | date: "%d %b %Y" }}</code></td>
  </tr>
  {% endif %}
  {% endfor %}
</table>
