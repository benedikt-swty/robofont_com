---
layout: page
title: Styles overview
treeTitle: Stylezz
excludeNavigation: true
tags:
  - documentation
level: beginner
draft: true
---

* Table of Contents
{:toc}

## Bring Me A Shrubbery!

**Monty Python** (also known as **The Pythons**) were a British surreal comedy group who created their sketch comedy show *Monty Python's Flying Circus*, which first aired on the BBC in 1969. A self-contained comedy team responsible for both writing and performing their work, the Pythons had creative control which allowed them to experiment with form and content, discarding rules of television comedy.

{% image extraDocs/styles-test/monty3.jpeg caption='Knights Who Say Ni!' %}

The film opens with a [credit sequence](http://youtu.be/djKPvXDwXcs) that becomes increasingly ridiculous, first becoming laden with strange {% internallink "reference/api/custom-observers" text='pseudo-Swedish commentary' %} about moose, and eventually being replaced with flashy titles in which everyone's name has been changed to something involving llamas.

### And Now for Something Completely Different

Libero qui maiores ab ipsam temporibus molestiae ut blanditiis. Consequatur ipsa consequuntur commodi sed earum aperiam deleniti.

Here is an unordered list:

- Vivamus vehicula aliquet augue.
- Fusce et ligula imperdiet, imperdiet massa viverra, cursus magna.
- Donec vel iaculis justo.
  - Cras sagittis sodales mi, sit amet aliquet nisl lacinia nec.
- Sed vestibulum tincidunt urna, vitae eleifend enim vehicula non.

Here is an ordered list:

1. Vivamus vehicula aliquet augue.
2. Fusce et ligula imperdiet, imperdiet massa viverra, cursus magna.
3. Donec vel iaculis justo.
   4. Cras sagittis sodales mi, sit amet aliquet nisl lacinia nec.
5. Sed vestibulum tincidunt urna, vitae eleifend enim vehicula non.

And here is a definition list:

fontTools
: dealing with binaries (OpenType, TrueType etc)

extractor
: extracting data from font binaries into UFO objects

feaTools2
: interacting with GPOS and GSUB features

- - -

You don't frighten us you English pig dogs – go and boil your bottoms. Son of a silly person, *I'll blow my nose at you, so called Arthur king*. I'll fart in your general direction. Your mother was a hamster.

|-----------------+------------+-----------------+----------------|
| Default aligned |Left aligned| Center aligned  | Right aligned  |
|-----------------|:-----------|:---------------:|---------------:|
| First body part |Second cell | Third cell      | fourth cell    |
| Second line     |foo         | **strong**      | baz            |
| Third line      |quux        | baz             | bar            |
|-----------------+------------+-----------------+----------------|

*He was not in the least bit scared to be mashed into a pulp. Or to have his eyes gouged out, and his elbows broken. To have his kneecaps split and his body burned away, and his limbs all hacked and mangled. Brave Sir Robin.*

Brave Sir Robin ran away, bravely ran away away. When danger reared its ugly head he bravely turned his tail and fled. Yes, brave Sir Robin turned about and gallantly he chickened out. Bravely taking to his feet he beat a very brave retreat – bravest of the brave, Sir Robin.

> Ipsa facilis ipsum `ducimus` omnis repudiandae `minima` nesciunt. Ipsa fuga itaque eius aliquid.
>
> Libero qui maiores ab ipsam temporibus molestiae ut blanditiis.
{: .note }

> Quasi aut beatae assumenda id officia `temporibus`. Qui ut aut natus at. Enim et sed `adipisci` maiores ad error nesciunt.
>
> - a list item
{: .seealso }

> Nunc congue sapien eget `gravida` tincidunt. Duis commodo, leo eget `porttitor` varius, tortor lorem ultricies arcu, non ornare quam justo id nisl.
{: .tip }

> - a list item
> - another list item
>
> Suspendisse tempor sed nisl a `posuere`. In nunc felis, iaculis a mi eget, `sagittis` pulvinar elit. Nulla facilisi. Duis bibendum sapien nec enim lobortis porta.
{: .warning }

> Fusce porttitor `malesuada` diam eget egestas. Aliquam vehicula `condimentum` scelerisque. Integer viverra interdum tellus, sit amet porta enim mattis a.
{: .todo }

> [RoboFont X.X]({{site.baseurl}}/downloads/)
{: .version-badge }

Maxime accusamus pariatur sed fugiat harum nisi qui. Libero qui maiores ab ipsam temporibus molestiae ut blanditiis. Consequatur ipsa consequuntur commodi sed earum aperiam deleniti.

### And now some code examples

Aut id ipsa quod. Ut explicabo `laborum` enim explicabo eligendi unde autem necessitatibus. Recusandae nisi `culpa` ex quasi fugiat nam ex.

{% showcode tutorials/setInfosDict.py %}

Maxime accusamus pariatur sed fugiat harum nisi qui. Libero qui maiores ab ipsam temporibus molestiae ut blanditiis. Consequatur ipsa consequuntur commodi sed earum aperiam deleniti.

```python
print('hello world')
```

```plaintext
>>> hello world
```

## Fonts

{% assign fonts = "RoboType-Roman RoboType-Italic RoboType-Bold RoboType-BoldItalic RoboType-NarrowBold RoboType-NarrowBoldItalic RoboType-Mono" | split:" " %}

{% for font in fonts %}
<div contenteditable spellcheck="false" style="font-family:'{{ font }}';line-height:1.4em;white-space:nowrap;overflow-x:scroll;">abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789 .,;:‘’“”</div>
{% endfor %}

{% for font in fonts %}
<div contenteditable spellcheck="false" style="font-family:'{{ font }}';font-size:5em;line-height:1.2em;white-space:nowrap;overflow-x:scroll;">abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789 .,;:‘’“”</div>
{% endfor %}

Sorry, Data. You're going to be an interesting companion, Mr. Data. Yesterday I did not know how to eat gagh. I'll alert the crew. Not if I weaken first. Yes, absolutely, I do indeed concur, wholeheartedly! Your shields were failing, sir. Smooth as an android's bottom, eh, Data? The unexpected is our normal routine. Francis Filter Bot

- - -

## Syntax examples

### Tree

Add `tree` to the front matter to include a page in a hierarchical order with subpages. All sub pages must be relative to the page.

```text
tree:
  - myPage
  - myOtherPage
  - subFolder/thirdPage
```

Use the `tree` directive to include a tree element on a page, with an optional `level` attribute to restrict nesting.

```text
{% raw %}{% tree page.url levels=1 %}{% endraw %}
```

It’s also possible to include a tree from another page:

```text
{% raw %}{% tree "/documentation" levels=3 %}{% endraw %}
```

### Internal links

Use the `internallink` directive to link to other pages in the documentation:

```text
{% raw %}{% internallink "workspace/inspector" %}{% endraw %}
```

### Images

All images are stored in the `/image` folder, in the root of the `robofont_com` repository.

To embed images on a page, use the `image` directive:

```text
{% raw %}{% image subFolder/myImage.png %}{% endraw %}
```

### Videos

All videso are stored in the `/videos` folder, in the root of the `robofont_com` repository.

To embed images on a page, use the `video` directive:

```text
{% raw %}{% video subFolder/myVideo.png %}{% endraw %}
```

{% video myVideo.mp4 %}

### Code

All .py files are stored in `/assets` folder, in the root of the `robofont_com` repository.

To embed images on a page, use the `showcode` directive:

```text
{% raw %}{% showcode /assets/myScript.py %}{% endraw %}
```

### Tags

Add `tags` to the front matter to include a list of tags for the page.

```text
tags:
  - scripting
  - interpolation
```

The tags of a page are rendered as a list of labels below the page title. Each tag links to a dedicated page listing all documents in which this tag appears.

To include one or more links to tags on any page, use the `tag` directive:

```text
{% raw %}{% include tag tags="glyphSet extensions" %}{% endraw %}
```

### Embedding video

[Vimeo](http://vimeo.com/)-hosted videos can be embedded on a page:

```text
{% raw %}{% include embed vimeo=34351633 %}{% endraw %}
```
