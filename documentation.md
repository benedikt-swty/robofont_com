---
layout: page
title: Documentation
menuOrder: 1
tree:
  - topics
  - tutorials
  - how-tos
  - reference
---

<div class='row'>
<div class='col'>
{% tree page.url levels=1 %}
</div>
<div class='col' markdown='1'>
- {% internallink "documentation/tags" %}
- {% internallink "documentation/glossary" %}
</div>
</div>

 
For questions and comments, use the [Forum].

[forum]: http://forum.robofont.com/category/12/documentation

- - -

This documentation is organized according to the [documentation system]:

<table>
  <tr>
    <td>
      <strong><a href='tutorials'>tutorials</a></strong><br/>
      <em>learning-oriented</em>
    </td>
    <td>
      <strong><a href='how-tos'>how tos</a></strong><br/>
      <em>problem-oriented</em>
    </td>
    <th>practical<br/>steps</th>
  </tr>
  <tr>
    <td>
      <strong><a href='topics'>topics</a></strong><br/>
      <em>understanding-oriented</em>
    </td>
    <td>
      <strong><a href='reference'>reference</a></strong><br/>
      <em>information-oriented</em>
    </td>
    <th>theoretical<br/>knowledge</th>
  </tr>
  <tr>
    <th width='35%'>useful when studying</th>
    <th width='35%'>useful when working</th>
    <td width='30%'></td>
  </tr>
</table>

> Use your left / right arrow keys to navigate to the previous / next pages in the documentation tree.
{: .tip }


[documentation system]: http://documentation.divio.com/
