require_relative "helpers"

module Jekyll
  class ShowcodeTag < Liquid::Tag

    include HelpersTools

    def initialize(tag_name, text, tokens)
      super
      parts = text.split(" ")
      @filename = parts[0]
      @type = parts[1] || "python"
      @path = File.join('assets', 'code', @filename)
    end

    def render(context)
      site = context.registers[:site]
      codePath = context.evaluate(@path)
      unless File.exist?(codePath)
        pinkWarning("Show code file not found: '#{codePath}' in #{context.registers[:page]["path"]}")
        ""
      else
          code = File.read(codePath)
          filename = File.basename (codePath)

          if @type.eql? "python"
            "```#{@type}
#{code}
```

<div class=\"downloadlink\">
  <a class=\"scriptReference roboFontLink\" href=\"#{site.baseurl}/#{@path}?open=1\">
    Open in RoboFont: #{filename}
  </a>
  <a class=\"scriptReference\" href=\"#{site.baseurl}/#{@path}\">
    Download: #{filename}
  </a>
</div>"

          else
            "```#{@type}
#{code}
```

<div class=\"downloadlink\">
  <a class=\"scriptReference\" href=\"#{site.baseurl}/#{@path}\">
    Download: #{filename}
  </a>
</div>"
        end
      end
    end
  end
end

Liquid::Template.register_tag('showcode', Jekyll::ShowcodeTag)