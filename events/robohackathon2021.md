---
layout: page
title: RoboHackathon 2021
excludeNavigation: true
---

{% image events/RoboHackathon.gif %}

The RoboHackathon is an online meetup for RoboFont developers. We released RoboFont 4.0 during the summer with a new drawing backend – {% internallink "/topics/merz" %} – and a new notification abstraction – {% internallink "/topics/subscriber" %}.  These new APIs can seriously improve users' experience but after a few months from the release, most {% internallink "/topics/mechanic-2/" %} extensions don't support the new features. That's an issue because extensions play a crucial role in a RoboFont workflow. For this reason, we came up with the idea of organizing an online hackathon, to create a space to discuss with developers best practices and strategies to update their tools.

We organized a {% internallink "how-tos/updating-scripts-from-RF3-to-RF4" text="table" %} containing every extension on Mechanic making use of Observers or `mojo.drawingTools`, so that you can keep track of the update status.

## Where and when

The RoboHackathon 2021 will be spread over two sessions of three hours each. Between the two sessions, there will be a 7 days gap to allow you to work on your code. The event will take place online on Wednesday, [October 6th](https://everytimezone.com/s/8053e9b8), and Wednesday, [October 13th](https://everytimezone.com/s/23c57810) at 19:00 CET (10 am West Coast, 1 pm East Coast). Before attending the event, we suggest you to browse the resources linked at the bottom of the page ("see also").

The event will take place in a Zoom conference room, [here](https://us02web.zoom.us/j/85121428732). We’ll use Slack to share code and stay in touch during and between the two sessions, [here](https://join.slack.com/t/robohackathon2021/shared_invite/zt-vhxj42l0-ttNzvl0CkK1FJmxU56Ctsg) you can find an invitation link.

## Program
* [October, 6th](https://everytimezone.com/s/8053e9b8)
    * 19 → 20: Keynotes from Frederik and Tal
    * 20 → 22: Update existing extensions with Frederik and Roberto

* [October, 13th](https://everytimezone.com/s/23c57810)
    * 19 → 20: Q&A
    * 20 → 22: Update existing extensions with Frederik and Roberto

The RoboHackathon will be hosted by RoboFont's Community Manager, Roberto Arista. If you need to contact him, you can use the RoboHackathon Slack workspace.

## RoboFont Hackathon public beta

Download the a public beta of the upcoming RoboFont 4.1: **[RoboFont 4.1 Beta](https://static.typemytype.com/robofont/versionHistory/RoboFont_4.1b_beta_2110061420.dmg)**

## Screencasts

{% include embed vimeo=629069073 %}

{% include embed vimeo=632159500 %}

> Be aware that both sessions will be recorded and shared publicly after the event!
{: .warning}

> - {% internallink "/topics/merz" text="Introduction to Merz" %}
> - {% internallink "/topics/subscriber" text="Introduction to Subscriber" %}
> - {% internallink "/topics/window-controller" text="Introduction to Window Controller" %}
> - {% internallink "/topics/upgrading-code-to-RoboFont4" %}
> - {% internallink "how-tos/updating-scripts-from-RF3-to-RF4" %}
{: .seealso}
