'''
make illustrations for how-to article about kerning groups 

uses 'IBM Plex Serif Regular' and 'RoboType Roman' as example fonts.

'''

# ---------
# functions
# ---------

def expandGroupsDict(groupsDict):
    groups = {}
    for fontName in groupsDict.keys():
        groups[fontName] = {}
        for side in groupsDict[fontName]:
            for chars in groupsDict[fontName][side].split():
                glyphs = list(chars)
                groupName = '%s.%s' % (chars[0], side)
                groups[fontName][groupName] = glyphs
    return groups

def drawGroups(side, groups, distance=140, padding=210, caption=True):
    save()
    translate(0, -padding)

    for group in groups.keys():
        if not side in group:
            continue

        alpha = 1.0 / len(groups[group])
        w = max([f[g].width for g in groups[group]])

        save()

        # draw glyphs
        for glyphName in groups[group]:
            g = f[glyphName]
            with savedState():
                if side == 'kern1':
                    translate(w - g.width, 0)
                # draw glyphs
                fill(*(c0 + (alpha,)))
                drawGlyph(g)

        # ruler
        with savedState():
            stroke(*c1)
            strokeWidth(10)
            x = 0 if side == 'kern2' else w
            y1 = f.info.descender - padding
            y2 = y1 + 1000 + padding*2
            line((x, y1), (x, y2))

        # caption
        with savedState():
            p = 80
            font('RoboType-Mono')
            fontSize(81)
            fill(*c1)
            txt = ' '.join(groups[group])
            a = 'left' if side == 'kern2' else 'right'    
            textBox(txt, (p, y1, w-p*2, 120), align=a)
            if caption:
                textBox(group, (p, y2-100, w-p*2, 100), align=a)
            else:
                arrow = '←' if a == 'left' else '→'
                font('Menlo')
                textBox(arrow, (p, y2-100, w-p*2, 100), align=a)

        restore()
        translate(w + distance, 0)

    restore()

# -----------
# groups data
# -----------

groupsDict = {
    'IBM Plex Serif Regular' : {
        'kern1' : 'pb jq dl hmn HI OQ',
        'kern2' : 'dq hkl mnr ceo BDEFHIKLMNPR CGOQ',
    },
    'RoboType Roman' : {
        'kern1' : 'bop hmnijldq kxvy HIJMN DOQ VWYXK',
        'kern2' : 'bijklmnprh cdeoq vy BDEFHIKLMNPR CGOQ',
    },
}

groups = expandGroupsDict(groupsDict)

# --------
# settings
# --------

imgSizes = {
    'IBM Plex Serif Regular' : (640, 420),
    'RoboType Roman' : (800, 420),
}

D = 50
P = 130
L = 320 + 1000 + P*2
C = False

c0 = 0, 0.4, 1
c1 = 1, 0, 0.9

saveImgs = True

# -----------
# make images
# -----------

for k in groups.keys():
    for f in AllFonts():
        fontName = '%s %s' % (f.info.familyName, f.info.styleName)
        if fontName == k:
            newPage(*imgSizes[fontName])
            translate(25, 80)
            scale(0.13)
            fontName = '%s %s' % (f.info.familyName, f.info.styleName)
            fontGroups = groups[fontName]

            drawGroups('kern2', fontGroups, distance=D, padding=P, caption=C)
            translate(0, L)
            drawGroups('kern1', fontGroups, distance=D, padding=P, caption=C)

if saveImgs:
    import os
    folder = '/Users/gferreira/Desktop'
    imgPath = os.path.join(folder, 'kerning.png', ) 
    saveImage(imgPath, multipage=True)
