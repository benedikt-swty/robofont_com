import os
import sys
import AppKit
import importlib
import pkg_resources  # part of setuptools

from mojo.compile import FDKVersion

# --------
# settings
# --------

saveYaml = True

# ------------
# extract data
# ------------

print("starting versionExtractor...\n")

bundle = AppKit.NSBundle.mainBundle()
info = bundle.infoDictionary()

# get FDK version
FDKVersion = FDKVersion.split("\n")[0]
FDKVersion = FDKVersion.replace("makeotf.lib version:", "").strip()

# embedded packages
moduleNames = set([
    "booleanOperations",
    "compositor",
    "cu2qu",
    "defcon",
    "defconAppKit",
    "extractor",
    "feaPyFoFum",
    # "fontCompiler", ### moved back into RF
    "fontmake",
    "fontMath",
    "fontParts",
    "fontPens",
    "fontTools",
    "glyphConstruction",
    "glyphNameFormatter",
    "markdown",
    "mutatorMath",
    "ufo2fdk",
    "ufo2svg",
    "ufonormalizer",
    "vanilla",
    "woffTools",
    "merz"
])

# collect version numbers
versionData = dict()
sysParts = info["LSMinimumSystemVersion"].split(".")
if len(sysParts) == 2:
    sysMajor, sysMinor = sysParts
elif len(sysParts) == 3:
    sysMajor, sysMinor, sysMicro = sysParts


versionData["minimumSystem"] = "%s.%s" % (sysMajor, sysMinor)
versionData["roboFont"] = info["CFBundleGetInfoString"]
versionData["afdko"] = FDKVersion
versionData["ufo"] = "3"  # manually??
versionData["python"] = "%s.%s.%s" % (sys.version_info.major, sys.version_info.minor, sys.version_info.micro)

fallback = "-"

for name in sorted(moduleNames):
    try:
        distribution = pkg_resources.get_distribution(name)
        version = distribution.version
    except pkg_resources.DistributionNotFound:
        try:
            module = importlib.import_module(name)
            print('\t\timporting %s...' % name)
            if hasattr(module, "__version__"):
                version = module.__version__
            elif hasattr(module, "version"):
                version = module.version
            else:
                version = fallback
        except:
            print('\t\tERROR: could not import %s' % name)
            pass

    if version in [None, "unknown"]:
        version = fallback
    if "unknown" in version:
        version = fallback
    version = version.split(".dev")[0]
    versionData[name] = version

# print imported data
print()
print("\textracted module versions:\n")
for key, value in sorted(versionData.items()):
    print("\t\t%s: %s" % (key, value))

# ------------------
# write data to yaml
# ------------------

if saveYaml:

    pyHelpersDir = os.getcwd()
    baseDir = os.path.dirname(pyHelpersDir)
    dataDir = os.path.join(baseDir, '_data')

    path = os.path.join(dataDir, "versions.yml")

    print()
    print("\tsaving to .yml file...\n")

    with open(path, "w") as f:
        f.write("# auto-generated with %s\n" % os.path.split(__file__)[-1])
        for key, value in versionData.items():
            f.write("%s: '%s'\n" % (key, value))

# done
print("...done.")
