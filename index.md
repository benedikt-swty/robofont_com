---
layout: default
title: RoboFont
hideTitle: true
excludeSearch: true
excludeNavigation: true
pageId: home-page
slideShows:
  intro:
    - image: reference/workspace/window-modes_single.png
    - image: tutorials/glyph-editor.png
    - image: reference/workspace/space-center.png
    - image: reference/workspace/scripting-window.png
    - image: reference/workspace/font-overview/features-editor.png
    #
    - image: extraDocs/carousel/workspace/font-overview-2.png
    - image: extraDocs/carousel/workspace/groups-editor-2.png
    - image: extraDocs/carousel/workspace/kern-center.png
    - image: extraDocs/carousel/workspace/font-info-2.png
  intro-extensions:
    - image: topics/Mechanic2.png
    - image: extraDocs/carousel/extensions/Ramsay-St.png
    - image: extraDocs/carousel/extensions/GlyphConstruction.png
    - image: extraDocs/carousel/extensions/OverlayUFOs.png
    - image: extraDocs/carousel/extensions/Outliner.png
    - image: extraDocs/carousel/extensions/Prepolator.png
    - image: extraDocs/carousel/extensions/word-o-mat.png
    - image: extraDocs/carousel/extensions/CornerTools.png
    - image: extraDocs/carousel/extensions/MetricsMachine.png
    - image: extraDocs/carousel/extensions/GlyphBrowser.png
    - image: extraDocs/carousel/extensions/SpeedPunk.png
    - image: extraDocs/carousel/extensions/ThemeManager.png
    - image: extraDocs/carousel/extensions/GlyphNanny.png
  autoPlay: true
  height: 550
---

{% comment %} ------ SLIDESHOW : WORKSPACE ------ {% endcomment %}

{% include slideshows items=page.slideShows.intro %}

{% comment %} ------ SPLASH : SENTENCE 1 ------ {% endcomment %}

<div class="center splash-subtitle">
The <span id='adjective'><script>var adjectives = [
  'missing',
  'heavy-duty',
  'poetic',
  'extensible',
  'avant-garde',
  'professional',
  'interstellar',
  'pythonic',
  'water-proof',
  'elegant',
  'industrial',
];
document.write(adjectives[Math.floor(Math.random() * adjectives.length)])</script></span> font editor for macOS.
</div>

{% comment %} ------ DOWNLOAD & BUY ------ {% endcomment %}

<div class="center">
  <a class="buy-button" href="https://static.typemytype.com/robofont/RoboFont.dmg">download</a>
  <a class="buy-button" href="{{ site.purchaselink }}">buy</a>
</div>

{% comment %} ------ COLUMNS : INTRO TEXT ------ {% endcomment %}

<div class="row item">
<div class="col">
Written from scratch in Python with scalability in mind.

A fully featured font editor with all the tools required for drawing typefaces.
</div>
<div class="col">
Provides full scripting access to objects and interface.

A platform for building your own tools and extensions, and much more…!
</div>
</div>

{% comment %} ------ SPLASH : ICONS ------ {% endcomment %}

**Start reading the [documentation]({{ site.baseurl }}/documentation/) or jump directly to one of the main sections:**

<div class="row item navigation-splash">
  <a class="documentation" href="{{ site.baseurl }}/documentation/topics">
    <span class="icon">D</span>Topics
  </a>
  <a class="documentation" href="{{ site.baseurl }}/documentation/tutorials">
    <span class="icon">W</span>Tutorials
  </a>
  <a class="documentation" href="{{ site.baseurl }}/documentation/how-tos">
    <span class="icon">T</span>How-Tos
  </a>
  <a class="documentation" href="{{ site.baseurl }}/documentation/reference">
    <span class="icon">A</span>Reference
  </a>
  <a class="extensions" href="{{ site.baseurl }}/documentation/topics/extensions/">
    <span class="icon">P</span>Extensions
  </a>
  <a class="extensions" href="http://robofontmechanic.com">
    <span class="icon">M</span>Mechanic
  </a>
  <a class="extensions" href="http://extensionstore.robofont.com">
    <span class="icon">S</span>Extension Store
  </a>
  <a class="education" href="http://education.robofont.com">
    <span class="icon">E</span>Education
  </a>
  <a class="forum" href="http://forum.robofont.com">
    <span class="icon">F</span>Forum
  </a>
</div>

{% comment %} ------ COLUMNS : TECHNICAL SPECS + EDUCATION ------ {% endcomment %}

<div class="row item">
<div class="col" markdown=1>
Technical specifications
------------------------

- requires macOS {{site.data.versions.minimumSystem}} or higher
- uses UFO3 as native font format
- supports Python {{site.data.versions.python}} out of the box

[read more…][Technical specification]
</div>
<div class="col" markdown=1>
Educational licenses
--------------------

Teachers can request a free 1-year trial license for their students, or subscribe to the Student License Service.

[read more…][Educational licensing]
</div>
</div>

{% comment %} ------ SPLASH : SENTENCE 2 ------ {% endcomment %}

<div class="center splash-subtitle">
A rock-solid core application +<br/> dozens of specialized extensions.
</div>

<br/>

{% comment %} ------ SLIDESHOW : EXTENSIONS ------ {% endcomment %}

{% include slideshows items=page.slideShows.intro-extensions %}

{% comment %} ------ COLUMNS : EXTENSIONS ------ {% endcomment %}

<div class="row item">
<div class="col" markdown=1>
Open-source extensions
----------------------

Dozens of open-source extensions by multiple developers are available on GitHub and via Mechanic.

[read more…][Extensions]
</div>

<div class="col" markdown=1>
Commercial extensions
---------------------

A growing collection of commercial extensions by certified developers are available in the Extension Store.

[read more…][Extension Store]
</div>

</div>

{% comment %} ------ SPLASH : SENTENCE 3 ------ {% endcomment %}

<div class="center splash-subtitle">
The tools you choose influence your creative process!
</div>

[Technical specification]: technical-specification
[Educational licensing]: http://education.robofont.com
[Extensions]: documentation/topics/extensions/
[Extension Store]: http://extensionstore.robofont.com

<br/>

{% comment %} ------ NEWS (FORUM > ANNOUNCEMENTS) ------ {% endcomment %}

<script src="{{site.baseurl}}/js/news.js"></script>
<div class="posts news">
    <h1><a href="http://forum.robofont.com/category/16/announcements/">Announcements:</a></h1>
    <div id="news-titles"></div>
</div>
