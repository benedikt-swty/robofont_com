---
excludeSearch: true
excludeNavigation: true
---

{% assign items = site.pages %}

{% for collection in site.collections %}
  {% for item in collection.docs %}
    {% assign items = items | push:item %}
  {% endfor %}
{% endfor %}

searchData = {
  {% for item in items %}
    {% unless item.draft-hidden %}
    {% unless item.excludeSearch %}
    {{ item.url | slugify | jsonify }}: {
        "title": {{ item.title | jsonify }},
        "url": {{ item.url | remove: 'index' | prepend: site.baseurl | jsonify }},
        "tags": {{ item.tags | join: ", " | jsonify }},
        {% if item.generatedPage %}
        "content": {{ item.documentation | append: item.objects | append: item.name | replace: "&#123;", " " | replace: "&#124;", " " | replace: "[", " " | replace: "]", " " | replace: "(", " " | replace: ")", " " | replace: ";", " " | replace: "=>", " " | replace: "&#x5c;", " " | replace: "`", " " | replace: '"', " " | replace: "\'", " " | strip_html | strip_newlines | jsonify }}
        {% else %}
        "content": {{ item.content | strip_html | strip_newlines | jsonify }}
        {% endif %}
    }{% unless forloop.last %},{% endunless %}
    {% endunless  %}
    {% endunless  %}
  {% endfor %}
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

$(document).ready(function () {
    // Set up search
    var index, store;
    index = lunr(function () {
      this.field('id')
      this.field('title', { boost: 10 })
      this.field('content', { boost: 8 })
      this.field('url')
      this.field('tags')
    });

    for (id in searchData) {
      data = searchData[id];
      data.id = id;
      index.add(data);
  };

  $("#search_box").keyup(function(){
      query = $("#search_box").val();
      results = index.search(query);
      display_search_results(results, query);
  });
  $("#s").keyup(function(){
      query = $("#s").val();
      results = index.search(query);
      display_search_results(results, query);
  });
  $("#site_search").submit(function(){
      event.preventDefault();
  });
  $("#search").submit(function(){
      event.preventDefault();
  });

  function display_search_results(results, query) {
      var search_results = $("#search_results");
      $("#search_term").html(": <span class='search-query'>" + query + "<span>");
      // Are there any results?
      search_results.empty();
      if (results.length) {
        search_results.empty();
        search_results.append("<p>Search finished, found " + results.length + " page(s) matching the search query.</p>")
        // Iterate over the results
        results.forEach(function(result) {
          var item = searchData[result.ref];
          // Build a snippet of HTML for this result
          var appendString = '<ul class="search"><li class="' + item.category + '"><a href="' + item.url + '?highlight=' + query + '">' + item.title + '</a></li></ul>';
          // Add it to the results
          search_results.append(appendString);
      });

      } else if (query == "") {
          search_results.empty();
      }

      else {
          search_results.html('<div>No results found</div>');
      }
  };

  var query = getUrlParameter("search")
    if ( query ) {
      var item = $("#search_box")
      item.removeAttr('placeholder')
      query = query.replace(/\+/g, ' ')
      item.val(query)
      item.keyup()
    }
})
