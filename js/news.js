function sortTimestamp(a, b) {
    return $(a).data("timestamp") < $(b).data("timestamp")
};

function readNews (url, search="") {
    $.getJSON(url + "?" + search, function(result){
        dest = $("#news-titles")
        $.each(result.topics, function(i, topic) {
            element = $("<h1/>")
            element.data("timestamp", topic.timestamp)
            element.append(
                $("<a/>", {href: "http://forum.robofont.com/topic/" + topic.slug }).append(
                    topic.title)
                )
            dest.append(element)
        })
        if (result.pagination.next.active) {
            readNews(url, result.pagination.next.qs)
        }
        $('#news-titles h1').sort(sortTimestamp).appendTo("#news-titles");
    })
}


$(document).ready(function () {
    blogapi = "https://forum.robofont.com/api/category/16/announcements/"
    readNews(blogapi)
})