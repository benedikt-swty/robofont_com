---
layout: page
title: Badges
menuOrder: 9
---

If you *really really* like RoboFont and you are a proud user you can put one of those badges on your site :)

<style>
    @font-face {
        font-family: "RoboFont-Badge";
        src: url("https://static.typemytype.com/robofont/RoboFont-Badge.woff");
    }
    .robofontbadge {
        font-family: "RoboFont-Badge";
    }
    .big {
        font-size: 40vw;
        line-height: 1em;
    }
    .bigger {
        font-size:75vw;
        line-height: 1em;
        color: DeepPink;
    }
    .bigger2 {
        font-size:75vw;
        line-height: 1em;
        color: orange;
    }

</style>
<div class="robofontbadge big">
    <div class="bigger">b</div>
    <div>ro</div>
    <div class="bigger2">f</div>
    <div>nt</div>
</div>

You can either [download the .woff file](http://robofont.com/downloads/RoboFont-Badge.woff) or add the following css in your `.css` file:

``` css
@font-face {
    font-family: "RoboFont-Badge";
    src: url("https://static.typemytype.com/robofont/RoboFont-Badge.woff");
}
.robofontbadge {
    font-family: "RoboFont-Badge";
}
```

Also possible to make a t-shirt from one of those badge.

# [Buy the t-shirt](http://society6.com/typemytype)

Made in RoboFont with a combination of the Outliner extension, ShadowMaker and Kalliculator. (the last two extensions are not available, yet...)

[![tshirt]({{ site.baseurl }}/images/tshirts/made-in-robofont-tank-tops.jpg)](http://society6.com/typemytype/made-in-robofont_t-shirt)
{: .align-left }
[![tshirt]({{ site.baseurl }}/images/tshirts/robofont-looney-tshirts.jpg)](https://society6.com/typemytype/robofont-looney_long-sleeve-tshirt)
{: .align-left }
[![tshirt]({{ site.baseurl }}/images/tshirts/robofont30-tank-tops.jpg)](https://society6.com/product/robofont30_vneck-tshirt)
{: .align-left }

