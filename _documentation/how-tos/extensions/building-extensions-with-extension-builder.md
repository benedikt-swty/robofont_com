---
layout: page
title: Using the Extension Builder
tags:
  - extensions
---

* Table of Contents
{:toc}

*This page explains how to use the {% internallink "workspace/extension-builder" %} to create and edit extension packages.*

Setting extension info
----------------------

To build an extension, you’ll first need to fill in the required information in the dialog.

> You can use the *Import* button to import data from an existing extension.
{: .tip }

{% image how-tos/extensions/extensionBuilder.png %}


Building the extension
----------------------

After all the data is set, use the *Build* button to build the extension package.

> The screenshot shows the settings for the {% internallink "boilerplate-extension" text='Boilerplate Extension' %}.
{: .note }

> - {% internallink "reference/extensions/extension-file-spec" %}
> - {% internallink "how-tos/extensions/building-extensions-with-script" %}
{: .seealso }
