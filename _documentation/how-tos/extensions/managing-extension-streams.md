---
layout: page
title: Managing extension streams
tags:
  - extensions
  - Mechanic
---

* Table of Contents
{:toc}


Overview
--------

An *extension stream* is a collection of {% internallink 'extensions/extension-item-format' text='extension items' %} about RoboFont extensions.

Multiple extension servers provide extension streams; the Mechanic 2 extension reads data from all linked streams, and builds a single list of available extensions.

Mechanic 2 comes with two pre-configured extension streams:

<table>
  <tr>
    <th width='35%'>stream</th>
    <th width='65%'>contents</th>
  </tr>
  <tr>
    <td><a href='http://robofontmechanic.com/'>Mechanic</a></td>
    <td>free & open-source extensions</td>
  </tr>
  <tr>
    <td><a href='http://extensionstore.robofont.com/'>Extension Store</a></td>
    <td>paid extensions</td>
  </tr>
</table>

Other streams and individual extensions can be added using the options in the *Settings* sheet – click on the gears icon (top right of the Mechanic 2 window) to open it.

> Support for multiple extension streams opens some interesting possibilities, such as curated lists of extensions or custom commercial stores…
{: .tip }


Adding extension streams
------------------------

In the *Settings* sheet, use the +/- buttons in the section *extensions json url stream* to add/remove extension streams from the list.

{% image how-tos/extensions/managing-extension-streams_extension-streams.png %}

The Mechanic 2 extension stream is available from this URL:

> [http://robofontmechanic.com/api/v2/registry.json](http://robofontmechanic.com/api/v2/registry.json)
{: .asCode }

Using a local server is also possible:

> [localhost:8888/mechanic-2-server/api/extensions.json](localhost:8888/mechanic-2-server/api/extensions.json)
{: .asCode }


Adding single extension items
-----------------------------

Single extension items can be added using `.mechanic` files – see {% internallink 'extensions/extension-item-format' %} for more information.

{% image how-tos/extensions/managing-extension-streams_single-items.png %}

In the *Settings* sheet, use the +/- buttons in the section *single extension items* to add/remove extensions from the list.



Check for updates on startup
----------------------------

{% image how-tos/extensions/managing-extension-streams_check-for-updates.png %}

The *Settings* sheet also has an option to let you choose if Mechanic should check for updates when RoboFont starts up.
