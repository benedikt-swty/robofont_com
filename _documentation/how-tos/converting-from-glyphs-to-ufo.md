---
layout: page
title: Converting from Glyphs to UFO
tags:
  - UFO
---

* Table of Contents
{:toc}


[Glyphs] uses its own `.glyphs` file format to store font sources, and supports UFO as a data interchange format: UFOs can be opened and saved as a native format, and there’s a built-in UFO export function.

The Glyphs documentation includes a tutorial on [Working with UFOs][Working with UFO]. This page provides additional information for RoboFont users.


Notes about exporting UFOs from Glyphs
--------------------------------------

1. When exporting UFOs using the *Export to UFO* menu, only the current front font is exported – active instances in files with multiple sources are not included. Use the [script](#export-all-instances-to-ufo) below if you need to export UFOs from all instances.

2. Fonts are exported to [UFO2] format; [UFO3] is not supported yet.

3. All glyphs are exported, including those marked to not export. You‘ll have to take care of removing them later.

4. The following private keys are added to the UFO’s font lib:

   - `com.schriftgestaltung.font.license`
   - `com.schriftgestaltung.font.licenseURL`
   - `com.schriftgestaltung.font.panose`
   - `com.schriftgestaltung.disablesAutomaticAlignment`
   - `com.schriftgestaltung.glyphOrder`
   - `com.schriftgestaltung.useNiceNames`
   - `com.schriftgestaltung.fontMasterID`

5. Mark colors are lost when opening exported UFOs in RoboFont 1; they work fine in RoboFont 3.


Exporting UFOs with code
------------------------

### Using glyphsLib

GoogleFonts developed the [glyphsLib](https://github.com/googlefonts/glyphsLib) Python module that provides a bridge from Glyphs source files to UFOs and Designspace files.

### Export all open fonts to UFO

This script exports the front source of all open fonts to UFO.

{% showcode how-tos/exportFontsFromGlyphsToUFO.py %}

### Export all instances to UFO

This script exports all instances in the current font to UFO.

{% showcode how-tos/exportInstancesFromGlyphsToUFO.py %}


Opening UFOs in Glyphs
----------------------

Some data might be lost by editing a UFO file with Glyphs. The list of issues can change over time, so the conditional is mandatory. If you know of any other piece of data lost during the process, please let us know. Here is a list of the known issues:

- The template glyph order is lost
- Glyphs convert all non-kerning groups to feature class definition

---

Based on original contribution by [Joancarles Casasín].

[Glyphs]: http://glyphsapp.com
[Working with UFO]: http://glyphsapp.com/tutorials/working-with-ufo
[UFO2]: http://unifiedfontobject.org/versions/ufo2/
[UFO3]: http://unifiedfontobject.org/versions/ufo3/
[Joancarles Casasín]: http://casasin.com/
