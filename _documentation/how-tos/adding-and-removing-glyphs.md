---
layout: page
title: Adding and removing glyphs
tags:
  - character set
---

* Table of Contents
{:toc}


Adding glyphs
-------------

### Using the Add Glyphs sheet

Use the {% internallink 'workspace/font-overview/add-glyphs-sheet' %} to create new glyphs manually from a list of glyph names. A basic syntax for building glyphs from components is supported.

{% image how-tos/adding-and-removing-glyphs_add-glyphs-sheet.png %}

### Using the Glyph Construction extension

The [Glyph Construction] extension provides a more powerful language for describing how glyph shapes are built. It is specially useful for creating accented glyphs.

{% image how-tos/building-accented-glyphs_glyph-builder_3.png %}

> - {% internallink 'how-tos/building-accented-glyphs-with-glyph-construction' %}
{: .seealso }

[Glyph Construction]: http://github.com/typemytype/glyphconstruction


Removing glyphs
---------------

1. select the glyphs to be removed in the {% internallink 'workspace/font-overview' %}
2. press the backspace key

> When a glyph is deleted from the font, it is also automatically removed from `font.glyphOrder`.
{: .note }

Depending on your settings in {% internallink "workspace/preferences-window/font-overview" %}, deleted glyphs will also be removed from groups and/or kerning pairs.

{% image how-tos/preferences_character-set_delete-glyph.png %}

> - This setting is applied only when deleting glyphs manually in the Font Overview.
> - The UFO3 specification allows groups, kerning and components to reference glyphs which are not in the font.
{: .note}

>Deleted glyphs are **not** removed automatically from OpenType features. If a deleted glyph appears in the features, you’ll need to remove it manually.
>
> If the OpenType compiler encounters a glyph which is not in the font, it will raise an error and the font will not be generated.
{: .warning }


Removing template glyphs
------------------------

Use ⌥ + backspace in the Font Overview to remove {% internallink "workspace/font-overview#template-glyphs" text='template glyphs' %} from the font.


- - -

> - {% internallink 'adding-and-removing-glyphs-with-code' %}
{: .seealso }

