---
layout: page
title: Creating glyph substitution rules
tags:
  - variable fonts
  - interpolation
  - designspace
---

* Table of Contents
{:toc}


Understanding glyph substitution rules
--------------------------------------

Glyph substitution rules work like trains changing tracks: the glyph mapped to the the character `I` (train) changes from glyph `I` (track 1) to glyph `I.narrow` (track 2) at a certain point along the path.

{% image how-tos/creating-glyph-substitution-rules_tracks.png %}

{% image how-tos/creating-glyph-substitution-rules_tracks-glyph.png %}

When the glyph substitution rule is applied, the user experiences this axis as a continuous range of values with one discontinuous ‘jump’:

{% image how-tos/creating-glyph-substitution-rules_substitution-axis.png %}

Looking at the entire designspace, we can see how the rule affects all weights:

{% image how-tos/creating-glyph-substitution-rules_substitution-designspace.png %}


Defining a glyph substitution rule
----------------------------------

A glyph substitution rule is made of two parts:

1. a pair of glyphs (substitution)
2. a set of conditions under which this substitution is active

Each condition is made of a pair of min/max values, and the name of the axis to which these values apply.

This is the rule for folding the serifs of the `I` in MutatorSans, in plain language:

substitution
: *substitute glyph `I` by glyph `I.narrow`*

condition
: *whenever the `width` parameter is less than `328`*

Here is the same substitution rule expressed in `.designspace` XML:

```xml
<rules>
    <rule name="fold_I_serifs">
        <conditionset>
            <condition maximum="328" minimum="0" name="width" />
        </conditionset>
        <sub name="I" with="I.narrow" />
    </rule>
</rules>
```


Different implementations of glyph substitution rules
-----------------------------------------------------

Variable fonts
: In variable fonts, the substitution happens *at runtime*: there are no changes in the font, only in the *sequence of glyphnames* that is rendered. The partial ‘tracks’ are combined into one single axis using conditional GSUB and GPOS.

UFO instances
: When making instances as UFOs using [MutatorMath] or [Superpolator], however, it is necessary to actually *swap the glyphs in the fonts* – and also update all references to the swapped glyphs in components and kerning data.


Creating rules with DesignSpaceEditor
-------------------------------------

[DesignSpaceEditor] offers a simple interface for creating and editing designspace data (no visual preview included).

Use the buttons in the *Rules* section to define new rules, substitutions and conditions.

{% image how-tos/creating-glyph-substitution-rules_design-space-editor.png %}

You can visualize the glyph substitution rules using [Skateboard](https://superpolator.com/skateboard.html). Navigate the design space and check the outcome in the Skateboard Text Window.

{% video how-tos/visualize-substitution-rules-skateboard.mp4 %}

> - [Rules (DesignSpaceEditor documentation)](http://github.com/LettError/designSpaceDocument#rules)
{: .seealso }


Creating rules with Superpolator
--------------------------------

[Superpolator] offers a complete UI for editing and previewing glyph substitution rules.

Use the *Rules* sheet to add rules to the current designspace. It offers an intuitive UI for composing glyph substitution rules.

{% image how-tos/creating-glyph-substitution-rules_superpolator-3.png %}

Glyph substitutions are displayed in the preview area. Notice also the dotted line across the designspace map – it indicates where the substitution takes place.

{% image how-tos/creating-glyph-substitution-rules_superpolator.png %}

> The current implementation of glyph substitution rules in Superpolator 3 is not yet compatible with the `.designspace` format and variable fonts.
{: .note }

> - [Rules (Superpolator Documentation)](http://new.superpolator.com/documentation/rules/)
{: .seealso }


Creating rules with designSpaceLib
----------------------------------

Glyph substitution rules can also be created with code using the [designSpaceLib]. Check this {% internallink "/tutorials/creating-designspace-files#creating-designspace-files-with-designspacelib" text="section" %} of {% internallink "/tutorials/creating-designspace-files" %}.

[MutatorMath]: http://github.com/LettError/mutatorMath
[Superpolator]: http://superpolator.com/
[DesignSpaceEditor]: http://github.com/LettError/designSpaceRoboFontExtension
[designSpaceLib]: http://github.com/fonttools/fonttools/blob/master/Doc/source/designspaceLib/scripting.rst
