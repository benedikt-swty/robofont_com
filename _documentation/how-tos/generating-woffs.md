---
layout: page
title: Generating WOFFs
tags:
  - scripting
  - WOFF
---

* Table of Contents
{:toc}


Using the Batch extension
-------------------------

OpenType fonts can be converted to WOFF and WOFF2 using the [Batch] extension, which provides an easy to use drag-and-drop interface.

{% image how-tos/batch-webfont-formats.png %}


Using a script
--------------

If you need to generate WOFF or WOFF2 files yourself with a script, here’s how you can do it using {% glossary FontTools %}:

### WOFF

```python
from fontTools.ttLib import TTFont

'''Generate WOFF from TTF or OTF font.'''

srcPath  = 'myFolder/myFont.ttf'
woffPath = srcPath.replace('.ttf', '.woff')

with TTFont(srcPath) as font:
    font.flavor = 'woff'
    font.save(woffPath)
```

### WOFF2

```python
from fontTools.ttLib import TTFont

'''Generate WOFF2 from TTF or OTF font.'''

srcPath  = 'myFolder/myFont.ttf'
woff2Path = srcPath.replace('.ttf', '.woff2')

with TTFont(srcPath) as font:
    font.flavor = 'woff2'
    font.save(woff2Path)
```

[Batch]: http://github.com/typemytype/batchRoboFontExtension/
