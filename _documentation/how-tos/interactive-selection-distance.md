---
layout: page
title: Displaying distances between selected points in the glyph editor
tags:
  - subscriber
  - merz
---

This example shows how to display distances between selected points in the glyph editor using {% internallink 'topics/subscriber' text="`Subscriber`" %} and {% internallink 'topics/merz' text="`Merz`" %}.

{% showcode how-tos/pointDistance.py %}