---
layout: page
title: Subscribing to current font events
tags:
  - subscriber
  - drawing
  - scripting
---

* Table of Contents
{:toc}


This example shows a floating window that displays information about the current font. The window knows when the user has switched the current font and updates accordingly.
The displayed information is updated when the data within the current font is changed.

{% image how-tos/mojo/currentFontWithUI.png %}

The tool uses the Subscriber module to subscribe to font kerning, info, features changed and to all outline changes. It also draws a small graph using `merz`.

{% showcode how-tos/mojo/currentFontWithUI.py %}