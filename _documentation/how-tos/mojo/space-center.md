---
layout: page
title: Scripting the Space Center
tags:
  - mojo
  - spacing
---

* Table of Contents
{:toc}

## Setting Space Center display options with a script

This example shows how to set the Space Center’s input text and display options.

{% showcode how-tos/mojo/setSpaceCenterDisplayOptions.py %}

## Adding a custom button to the Space Center

The script below shows how to add a custom button to the Space Center interface.

{% image how-tos/mojo/customButtonSpaceCenter.png %}

{% showcode how-tos/mojo/customButtonSpaceCenter.py %}

> Adapted from this [forum thread](https://forum.robofont.com/topic/815/how-to-add-custom-button-to-the-spacecenter-s-display-menu)


## Synchronizing multiple Space Centers

The following script shows a simple tool to synchronize input and display options across all open Space Centers. Once initialized, the tool will add a checkbox to the bottom right of all Space Center windows. Select/deselect the checkbox to turn synchronization on/off.

{% showcode how-tos/mojo/synchronizeSpaceCenters.py %}

> Based on [this script] by Jackson Cavanaugh (Okay Type).
{: .note }

[this script]: http://gist.github.com/okay-type/bcd2d9ea18b16b58d757cc1e88c9cb84

## Exporting multipage PDFs from the Space Center

The ‘proof of concept’ script below takes the current Space Center’s content and settings, and uses them to create a multipage PDF with DrawBot.

{% image how-tos/mojo/exportSpaceCenterToMultipagePDF-1.png %}

{% image how-tos/mojo/exportSpaceCenterToMultipagePDF-2.png %}

1. open a font
2. open the Space Center, type some text
3. run the script below in the [DrawBot extension](http://github.com/typemytype/drawBotRoboFontExtension)

{% showcode how-tos/mojo/exportSpaceCenterToMultipagePDF.py %}

> Written in response to a [question in the Forum].
{: .note }

[question in the Forum]: http://forum.robofont.com/topic/658/is-it-possible-to-export-to-from-space-center-to-a-multi-page-or-tall-page-pdf/7

> Have a look at the {% internallink "/reference/api/mojo/mojo-ui#mojo.UI.SpaceCenter" text="SpaceCenter object reference" %} for the complete list of attributes and methods.
{: .seealso }
