---
layout: page
title: Scripting Smart Sets
tags:
  - mojo
  - smart sets
---

* Table of Contents
{:toc}

> [RoboFont 3.4]({{site.baseurl}}/downloads/)
{: .version-badge }

The scripts below demonstrate the new {% internallink 'reference/api/mojo/mojo-smartset' %} API.


Getting Smart Sets
------------------

```python
from mojo.smartSet import getSmartSets

# get default sets
defaultSets = getSmartSets()
print(defaultSets)

# get font sets
f = CurrentFont()
fontSets = getSmartSets(f)
print(fontSets)
```


Setting Smart Sets
------------------

> This will overwrite all existing Smart Sets!
{: .warning }

```python
from mojo.smartSet import SmartSet, setSmartSets, updateAllSmartSets

# create new smart sets
items = [
    SmartSet(smartSetName="foo", glyphNames=["a", "b"]),
    SmartSet(smartSetName="bar", query="Width < 200")
]

# set default smarts sets
setSmartSets(items)

# set font sets
f = CurrentFont()
setSmartSets(items, font=f)

# update the Smart Sets panel
updateAllSmartSets()
```


Adding Smart Sets
-----------------

```python
from mojo.smartSet import SmartSet, addSmartSet, updateAllSmartSets

# create new smart sets
items = [
    SmartSet(smartSetName="vowels", glyphNames=["a", "e", "i", "o", "u"]),
    SmartSet(smartSetName="empty", query="Empty == 'True'")
]

# add new sets to default sets
for item in items:
  addSmartSet(item)

# add new sets to font sets
f = CurrentFont()
for item in items:
    addSmartSet(item, font=f)

# update the Smart Sets panel
updateAllSmartSets()
```

> - {% internallink 'topics/smartsets' %}
> - {% internallink 'workspace/font-overview/smart-sets-panel' %}
{: .seealso }


Query-based Smart Sets
----------------------

Queries are expressed using Apple’s [Predicate Format String Syntax].

### Language tokens

These are the main building blocks of the predicate language:

| construct             | examples                                            |
| --------------------- | --------------------------------------------------- |
| literals              | `False` `True` `'a'` `42` `{'a', 'b'}`              |
| compound expressions  | `and` `or` `not`                                    |
| aggregate operations  | `any` `some` `all` `none` `in`                      |
| basic comparisons     | `=` `>=` `<=` `>` `<` `!=` `between`                |
| string comparisons    | `contains` `beginswith` `endswith` `like` `matches` |

> The `matches` comparison requires a regular expression. (see the example below)
{: .note }

### Glyph attributes and supported comparisons

These are the glyph attribute names, their data types and supported comparisons.

<table>
  <tr>
    <th width='30%'>type</th>
    <th width='30%'>attributes</th>
    <th width='40%'>comparisons</th>
  </tr>
  <tr>
    <td><code>str</code></td>
    <td>
      <code>Name</code><br/>
      <code>ComponentsNames</code><br/>
      <code>AnchorNames</code>
    </td>
    <td>
      <code>in</code>
      <code>contains</code>
      <code>beginswith</code>
      <code>endswith</code>
      <code>like</code>
      <code>matches</code>
    </td>
  </tr>
  <tr>
    <td><code>int</code> or <code>float</code></td>
    <td>
      <code>Width</code><br/>
      <code>LeftMargin</code><br/>
      <code>RightMargin</code><br/>
      <code>Unicode</code><br/>
      <code>Contours</code><br/>
      <code>Components</code><br/>
      <code>Anchors</code>
    </td>
    <td>
      <code><</code>
      <code>=</code>
      <code>></code>
      <code>!=</code>
      <code>between</code>
      <code>in</code>
    </td>
  </tr>
  <tr>
    <td><code>str</code></td>
    <td>
      <code>MarkColor</code>
    </td>
    <td>
      <code>=</code>
      <code>!=</code>
      <code>contains</code>
      <code>matches</code>
    </td>
  </tr>
  <tr>
    <td><code>bool</code></td>
    <td>
      <code>Empty</code><br/>
      <code>GlyphChanged</code><br/>
      <code>Template</code><br/>
      <code>SkipExport</code>
    </td>
    <td>
      <code>True</code> <code>False</code>
    </td>
  </tr>
  <tr>
    <td><code>str</code></td>
    <td>
      <code>Note</code>
    </td>
    <td>
      <code>contains</code>
    </td>
  </tr>
</table>

### Example queries

```text
"Name == 'a'"
"Name in {'a', 'b'}"
"Empty == 'True'"
"Name contains 'a' AND Width < 300"
"Name matches '[A-z]'"
"Width in {120, 240, 360}")
```

> - {% internallink 'workspace/font-overview/search-glyphs-panel' %}
{: .seealso }

[Predicate Format String Syntax]: http://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/Predicates/Articles/pSyntax.html#//apple_ref/doc/uid/TP40001795-CJBDBHCB
