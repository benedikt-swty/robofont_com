---
layout: page
title: Subscribing to RoboFont application events
tags:
  - subscriber
  - drawing
  - scripting
---

* Table of Contents
{:toc}

This example shows a floating window that displays a list of all open fonts. The window knows when the user has changed the open fonts (opened, closed, created a new one) and updates accordingly.

{% image how-tos/mojo/roboFontWithUI.png %}

The tool subscribes to font info and features. It uses the `merz` module to draw a little graph in the "glyph" panel.

{% showcode how-tos/mojo/roboFontWithUI.py %}
