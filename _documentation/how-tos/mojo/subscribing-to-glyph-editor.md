---
layout: page
title: Subscribing to glyph editor events
tags:
  - subscriber
  - drawing
  - scripting
---

* Table of Contents
{:toc}

This example shows how to visualize data in the glyph editor. The visualization is done within the glyph editing space with the `merz` library. The visualization is done atop the glyph editor with the vanilla library. The visualization is updated automatically when the glyph in the editor is switched and when the data within the glyph is changed. Different timings (events coalescing) are used to demonstrate optimization possibilities.

{% image how-tos/mojo/glyphEditorWithUI.png %}

The tool subscribes to the current glyph contour, components and anchor changes and count them while drawing some info with `merz`.

{% showcode how-tos/mojo/glyphEditorWithUI.py %}
