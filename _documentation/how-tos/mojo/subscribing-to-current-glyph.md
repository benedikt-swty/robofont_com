---
layout: page
title: Subscribing to current glyph events
tags:
  - subscriber
  - drawing
  - scripting
---

* Table of Contents
{:toc}


This example shows a floating window that displays information about the current glyph. The window knows when the user has switched the current glyph and updates accordingly. The displayed information is updated when the data within the current glyph is changed.

{% image how-tos/mojo/currentGlyphWithUI.png %}

The tool uses the `Subscriber` module to subscribe to current glyph outlines, anchors, components, and guidelines. It uses `merz` to render a visualization into a vanilla `FloatingWindow` based on the glyph data.

{% showcode how-tos/mojo/currentGlyphWithUI.py %}
