---
layout: page
title: Updating scripts from RF3 to RF4
tags:
  - merz
  - subscriber
  - mojo
---

* Table of Contents
{:toc}

RoboFont 4.0 introduces several new APIs, like {% internallink "topics/merz" text="Merz" %} and {% internallink "topics/subscriber" text="Subscriber" %}. You are probably excited about the new opportunities offered by these modules, but how should you approach the update of existing tools? We got you covered. Every example shown in the RoboFont documentation has been updated to Merz and Subscriber. And the RoboFont team is also helping developers updating their extensions.

Here you can find a table of all the [Mechanic2](https://robofontmechanic.com) extensions using observers and drawingTools. This means they need to be updated in order to perform optimally on RoboFont. The table also keeps track of their update status.

<!-- START MECHANIC TABLE -->
<table>
  <thead>
    <tr>
      <th>extension</th>
      <th>developer</th>
      <th>observers</th>
      <th>drawingTools</th>
      <th>updated</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td><a href="https://github.com/asaumierdemers/AddOverlap/">Add Overlap</a></td>
        <td><a href="http://asaumierdemers.com">Alexandre Saumier Demers</a></td>
        <td>yes</td>
        <td>no</td>
        <td>yes</td>
    </tr>
    <tr>
        <td><a href="https://github.com/adobe-type-tools/adjust-anchors-rf-ext/">Adjust Anchors</a></td>
        <td><a href="https://github.com/adobe-type-tools">Adobe Type Tools</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/jenskutilek/AnchorOverlayTool/">Anchor Overlay Tool</a></td>
        <td><a href="http://www.kutilek.de">Jens Kutilek</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/LettError/angleRatioTool/">AngleRatioTool</a></td>
        <td><a href="http://letterror.com">LettError</a></td>
        <td>no</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typedev/autoSaviour-RF/">AutoSaviour</a></td>
        <td><a href="https://github.com/typedev">Alexander Lubovenko</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/BlackFoundry/DesignRecorder">Black DesignRecorder</a></td>
        <td><a href="http://black-foundry.com">BlackFoundry</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/andyclymer/BlueZoneEditor-roboFontExt/">Blue Zone Editor</a></td>
        <td><a href="http://www.andyclymer.com">Andy Clymer</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/asaumierdemers/BroadNibBackground/">Broad Nib Background</a></td>
        <td><a href="http://asaumierdemers.com">Alexandre Saumier Demers</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/jtanadi/CheckParallelTool/">Check Parallel Tool</a></td>
        <td><a href="http://jesentanadi.com">Jesen Tanadi</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/thomgb/ContoursLock/">ContoursLock</a></td>
        <td><a href="http://www.geenbitter.nl">Thom Janssen</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/jenskutilek/Curve-Equalizer">Curve Equalizer</a></td>
        <td><a href="http://www.kutilek.de">Jens Kutilek</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/cjdunn/delorean">Delorean: Interpolation Preview</a></td>
        <td><a href="https://cjtype.com">CJ Dunn</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
    </tr>
    <tr>
        <td><a href="https://github.com/LettError/designSpaceRoboFontExtension/">DesignSpaceEditor</a></td>
        <td><a href="http://letterror.com">LettError</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/drawBotRoboFontExtension/">DrawBot</a></td>
        <td><a href="http://drawbot.com">DrawBot</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/ryanbugden/Edit-Font-Dimensions/">Edit Font Dimensions</a></td>
        <td><a href="https://ryanbugden.com">Ryan Bugden</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/LettError/editThatNextMasterRoboFontExtension/">EditThatNextMaster</a></td>
        <td><a href="http://letterror.com">LettError</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/roboDocs/eventObserverExtension/">EventObserver</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/ryanbugden/Eyeliner/">Eyeliner</a></td>
        <td><a href="https://ryanbugden.com">Ryan Bugden</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/featurePreviewRoboFontExtension/">Feature Preview</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/glifViewerRoboFontExtension/">Glif Viewer</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typesupply/glyph-nanny">Glyph Nanny</a></td>
        <td><a href="http://tools.typesupply.com">Tal Leming</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/GlyphConstruction/">GlyphConstruction</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/thomgb/glyphmorfExtension/">GlyphMorf</a></td>
        <td><a href="http://hallotype.nl/glyph-morf">Thom Janssen</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/rafalbuchner/glyph-palette/">GlyphPalette</a></td>
        <td><a href="http://www.rafalbuchner.com">Rafał Buchner</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/luke-snider/GlyphWalker/">GlyphWalker</a></td>
        <td><a href="http://www.revolvertype.com">Lukas Schneider</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/goldnerRoboFontExtension/">Goldener</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/roboDocs/GroundControl/">Ground Control</a></td>
        <td><a href="https://github.com/loicsander">Loïc Sander</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typesupply/guidetool/">Guide Tool</a></td>
        <td><a href="https://github.com/typesupply">Tal Leming</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/mrecord/InspectorMini/">InspectorMini</a></td>
        <td><a href="http://commercialtype.com">Mark Record</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/roboDocs/interpolationMatrix/">Interpolation Matrix</a></td>
        <td><a href="https://github.com/loicsander">Loïc Sander</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/andyclymer/InterpolationSlider-RoboFontExt/">Interpolation Slider</a></td>
        <td><a href="http://www.andyclymer.com">Andy Clymer</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/FontBureau/fbOpenTools/tree/master/ItalicBowtie">Italic Bowtie</a></td>
        <td><a href="http://fontbureau.typenetwork.com">FontBureau</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
    </tr>
    <tr>
        <td><a href="https://github.com/sansplomb/ItalicGuide/">ItalicGuide</a></td>
        <td><a href="http://black-foundry.com">Jérémie Hornus</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/adobe-type-tools/kernalytics-rf-ext/">Kern-A-Lytics</a></td>
        <td><a href="https://github.com/adobe-type-tools">Adobe Type Tools</a></td>
        <td>no</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typedev/KernTool3/">KernTool3</a></td>
        <td><a href="https://github.com/typedev">Alexander Lubovenko</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/kerningCheckerRoboFontExtension/">KerningChecker</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/layerPreviewRoboFontExtension/">Layer Preview</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/LettError/LightMeter/">LightMeter</a></td>
        <td><a href="http://letterror.com">LettError</a></td>
        <td>no</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/loggerRoboFontExtension/">Logger</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/cjdunn/MM2SpaceCenter/">MM2SpaceCenter</a></td>
        <td><a href="http://cjtype.com">CJ Dunn</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https:/gitlab.com/typoman/robofont-mark-tool/">Mark Positioning</a></td>
        <td><a href="http://bahman.design">Bahman Eslami</a></td>
        <td>no</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/okay-type/Markymark-roboFontExt/">Marky Mark</a></td>
        <td><a href="http://www.okaytype.com">Jackson Showalter-Cavanaugh</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/robofont-mechanic/mechanic-2/">Mechanic</a></td>
        <td><a href="http://robofontmechanic.com">RoboFont Mechanic</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/outlinerRoboFontExtension/">Outliner</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>no</td>
        <td>yes</td>
    </tr>
    <tr>
        <td><a href="https://github.com/thomgb/PDFButtonSpace/">PDFButtonSpace</a></td>
        <td><a href="http://www.hallotype.nl">Thom Janssen</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https:/gitlab.com/typoman/robofont-special-clipboard/">Paste Special</a></td>
        <td><a href="http://bahman.design">Bahman Eslami</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/roboDocs/PenBallWizard/">PenBallWizard</a></td>
        <td><a href="https://github.com/loicsander">Loïc Sander</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/jackjennings/Plum/">Plum</a></td>
        <td><a href="ttp://ja.ckjennin.gs">Jack Jennings</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typesupply/popuptools/">Pop Up Tools</a></td>
        <td><a href="https://github.com/typesupply">Tal Leming</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/BlackFoundry/Properties/">Properties</a></td>
        <td><a href="http://black-foundry.com">Jérémie Hornus</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/sansplomb/QuadraticConverter/">QuadraticConverter</a></td>
        <td><a href="http://www.typosansplomb.com/">Jérémie Hornus</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/ramsayStreetRoboFontExtension/">Ramsay St.</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/jenskutilek/RedArrow/">Red Arrow</a></td>
        <td><a href="http://www.kutilek.de">Jens Kutilek</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/jenskutilek/RoboChrome/">RoboChrome</a></td>
        <td><a href="https://github.com/jenskutilek">Jens Kutilek</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/projectRoboFontExtension/">RoboFontProject</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/productiontype/RoboLasso/">RoboLasso</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>no</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typesupply/roborepl/">RoboREPL</a></td>
        <td><a href="http://typesupply.com">Tal Leming</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/jackjennings/RoboToDo/">RoboToDo</a></td>
        <td><a href="http://ja.ckjennin.gs">Jack Jennings</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/frankrolf/Rotator/">Rotator</a></td>
        <td><a href="http://www.frgr.de">Frank Grießhammer</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/jansindl3r/Scale-absolutely/">Scale, absolutely!</a></td>
        <td><a href="https://www.futurefonts.xyz/jan-sindler">Jan Šindler</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/shapeToolRoboFontExtension">Shape Tool</a></td>
        <td><a href="http://typemytype.com/">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
    </tr>
    <tr>
        <td><a href="https://github.com/LettError/showSparks/">ShowSparks</a></td>
        <td><a href="http://letterror.com">LettError</a></td>
        <td>no</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/ryanbugden/Sidebear/">Sidebear</a></td>
        <td><a href="https://ryanbugden.com">Ryan Bugden</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https:/gitlab.com/typoman/robofont-kerning-tool/">Simple Kerning</a></td>
        <td><a href="http://bahman.design">Bahman Eslami</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/slanterRoboFontExtension/">Slanter</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/yanone/speedpunk/">Speed Punk</a></td>
        <td><a href="https://yanone.de">Yanone</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/LettError/SuperpolatorRemote/">SuperpolatorRemote</a></td>
        <td><a href="http://letterror.com">LettError</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/LettError/symmetricalRoundShapeDrawingTool/">SymmetricalRoundShapeDrawingTool</a></td>
        <td><a href="http://letterror.com">LettError</a></td>
        <td>no</td>
        <td>yes</td>
        <td>yes</td>
    </tr>
    <tr>
        <td><a href="https://github.com/connordavenport/Theme-Manager/">Theme Manager</a></td>
        <td><a href="http://www.connordavenport.com">Connor Davenport and Andy Clymer</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/RafalBuchner/tool-manager/">ToolManager</a></td>
        <td><a href="http://www.rafalbuchner.com">Rafał Buchner</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/typemytype/tracerRoboFontExtension/">Tracer</a></td>
        <td><a href="http://typemytype.com">Frederik Berlaen</a></td>
        <td>no</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/Typefounding/underlineStrikethrough/">Underline Strikethrough</a></td>
        <td><a href="http://www.typefounding.com">Ben Kiel</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/jenskutilek/RFUnicodeInfo/">Unicode Info</a></td>
        <td><a href="http://www.kutilek.de/">Jens Kutilek</a></td>
        <td>yes</td>
        <td>no</td>
        <td>yes</td>
    </tr>
    <tr>
        <td><a href="https://github.com/asaumierdemers/WurstSchreiber/">WurstSchreiber</a></td>
        <td><a href="http://asaumierdemers.com">Alexandre Saumier Demers</a></td>
        <td>yes</td>
        <td>yes</td>
        <td>yes</td>
    </tr>
    <tr>
        <td><a href="https://github.com/LettError/zoneChecker/">ZoneChecker</a></td>
        <td><a href="http://letterror.com">LettError</a></td>
        <td>no</td>
        <td>yes</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/frankrolf/showDist/">showDist</a></td>
        <td><a href="http://www.frgr.de">Frank Grießhammer</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
    <tr>
        <td><a href="https://github.com/ninastoessinger/word-o-mat/">word-o-mat</a></td>
        <td><a href="http://www.typologic.nl">Nina Stössinger</a></td>
        <td>yes</td>
        <td>no</td>
        <td>no</td>
    </tr>
</tbody>
</table>

<!-- END MECHANIC TABLE -->

Here you can find a list of scripts before and after the update. Usually, examples are the best way to wrap your head around new concepts.

<!-- START COMPARISON TABLES -->
<table>
  <thead>
    <tr>
      <th width="70%">scripts</th>
      <th width="15%">RoboFont 3</th>
      <th width="15%">RoboFont 4</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td>synchronizeSpaceCenters.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/mojo/synchronizeSpaceCenters.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/mojo/synchronizeSpaceCenters.py">NEW</a></td>
    </tr>
    <tr>
        <td>listLayersTool.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/listLayersTool.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/listLayersTool.py">NEW</a></td>
    </tr>
    <tr>
        <td>multiFontGlyphPreview.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/multiFontGlyphPreview.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/multiFontGlyphPreview.py">NEW</a></td>
    </tr>
    <tr>
        <td>openComponentInNewWindow.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/openComponentInNewWindow.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/openComponentInNewWindow.py">NEW</a></td>
    </tr>
    <tr>
        <td>pointDistance.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/pointDistance.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/pointDistance.py">NEW</a></td>
    </tr>
    <tr>
        <td>stencilPreview.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/stencilPreview.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/stencilPreview.py">NEW</a></td>
    </tr>
    <tr>
        <td>simpleFontWindow.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/how-tos/simpleFontWindow.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/how-tos/simpleFontWindow.py">NEW</a></td>
    </tr>
    <tr>
        <td>drawReferenceGlyph.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/drawReferenceGlyph.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/observers/drawReferenceGlyph.py">NEW</a></td>
    </tr>
    <tr>
        <td>addToolbarItem.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/mojo/addToolbarItem.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/a098bd67eb6076c57ded45599888554180c54616/assets/code/building-tools/mojo/addToolbarItem.py">NEW</a></td>
    </tr>
    <tr>
        <td>earMarkGlyph.py</td>
        <td><a href="https://github.com/okay-type/Markymark-roboFontExt/blob/67f208797579d940304b1d49670fcee7320a27e8/MarkyMark.roboFontExt/lib/mark-in-glyphview.py">OLD</a></td>
        <td><a href="https://gist.github.com/okay-type/9111826ae6571d56905538c98d5de369">NEW</a></td>
    </tr>
    <tr>
        <td>GlyphViewMetricsUI.py</td>
        <td><a href="https://github.com/okay-type/GlyphviewMetricsHUD-robofontExt/blob/d8c691909785fad5a61f09ecf0fc39a28e5a8a42/GlyphviewMetricsUI.roboFontExt/lib/glyphview-metrics-ui.py">OLD</a></td>
        <td><a href="https://gist.github.com/okay-type/5b82a346996015f26e0e4f4d1a3cec85">NEW</a></td>
    </tr>
    <tr>
        <td>showBody.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/showBody.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/7e5356a0607eff8c04201768e86994a3490b550e/assets/code/building-tools/observers/showBody.py">NEW</a></td>
    </tr>
    <tr>
        <td>addGlyphEditorSubviewExample.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/addGlyphEditorSubviewExample.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/6e120385698eca5e1798e43f3a08e8ea1f717015/assets/code/building-tools/observers/addGlyphEditorSubviewExample.py">NEW</a></td>
    </tr>
    <tr>
        <td>customToolActivator.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/interactive/customToolActivator.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/6e120385698eca5e1798e43f3a08e8ea1f717015/assets/code/building-tools/interactive/customToolActivator.py">NEW</a></td>
    </tr>
    <tr>
        <td>customGlyphWindowDisplayMenu.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/customGlyphWindowDisplayMenu.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/6e120385698eca5e1798e43f3a08e8ea1f717015/assets/code/building-tools/observers/customGlyphWindowDisplayMenu.py">NEW</a></td>
    </tr>
    <tr>
        <td>customInspectorPanel.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/observers/customInspectorPanel.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/6e120385698eca5e1798e43f3a08e8ea1f717015/assets/code/building-tools/observers/customInspectorPanel.py">NEW</a></td>
    </tr>
    <tr>
        <td>simpleExampleTool.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/interactive/simpleExampleTool.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/4508da4bb38c70c5921445aca065df382ded0323/assets/code/building-tools/interactive/simpleExampleTool.py">NEW</a></td>
    </tr>
    <tr>
        <td>polygonSelection.py</td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/2984490d9ad37684e2b6a6ffd8f5a7431264801a/assets/code/building-tools/interactive/polygonSelection.py">OLD</a></td>
        <td><a href="https://gitlab.com/typemytype/robofont_com/-/blob/e650e018db71d8a9105a75d4f45a44f9e8ee1eb4/assets/code/building-tools/interactive/polygonSelection.py">NEW</a></td>
    </tr>
</tbody>
</table>

<!-- END COMPARISON TABLES -->

If you updated a publicly available tool and you want to show it here, you can {% internallink "tutorials/editing-the-docs" text="edit the documentation" %}, or let us know on the [RoboFont forum](https://forum.robofont.com).

> - {% internallink "topics/subscriber" text="Subscriber introduction article" %}
> - {% internallink "reference/api/mojo/mojo-subscriber" text="Subscriber reference" %}
> - {% internallink "topics/merz" text="Merz introduction article" %}
> - [Merz reference](https://typesupply.github.io/merz/)
{: .seealso }