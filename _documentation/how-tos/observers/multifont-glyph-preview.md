---
layout: page
title: Multifont glyph preview
---

{% image how-tos/observers/multifont-glyph-preview.png %}

{% showcode how-tos/observers/multiFontGlyphPreview.py %}
