---
layout: page
title: Custom glyph preview
tags:
  - observers
---

This example shows how to draw something in the canvas when previewing a glyph.

{% showcode how-tos/observers/bubblesPreview.py %}
