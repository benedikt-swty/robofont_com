---
layout: page
title: Show glyph box
tags:
  - observers
---

Another simple example tool which subscribes to the `drawBackground` event.

Instead of showing the glyph space as an infinitely long vertical stripe, we want it to be displayed as a box, with a fixed height. This box starts at the font’s descender height, and is as high as its Em square.

{% showcode how-tos/observers/showBody.py %}
