---
layout: page
title: Open component in new window
tags:
  - observers
---

This example shows how to use an observer to perform an action when a key is pressed. If a component is selected in the {% internallink 'workspace/glyph-editor' %}, pressing `Shift C` will open a new glyph window with the selected component’s base glyph.

{% showcode how-tos/observers/openComponentInNewWindow.py %}
