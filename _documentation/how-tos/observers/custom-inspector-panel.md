---
layout: page
title: Custom Inspector section
tags:
  - observers
---

This example shows how to add a custom section to the {% internallink 'workspace/inspector' %} panel.

{% image how-tos/observers/custom-inspector-panel.png %}

{% showcode how-tos/observers/customInspectorPanel.py %}

> - {% internallink 'how-tos/mojo/accordion-window' %}
{: .seealso }