---
layout: page
title: Stencil preview
tags:
  - observers
---

This example shows a simple stencil preview tool. The result is produced by subtracting the background layer from the foreground using {% internallink 'topics/boolean-glyphmath' %}.

{% image how-tos/observers/stencilPreview_0.png %}

{% image how-tos/observers/stencilPreview_1.png %}

{% showcode how-tos/observers/stencilPreview.py %}
