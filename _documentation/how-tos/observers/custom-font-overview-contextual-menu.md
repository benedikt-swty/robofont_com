---
layout: page
title: Custom contextual menu
tags:
  - mojo
---

This example shows how to create a custom contextual menu in the {% internallink 'workspace/font-overview' %}.

{% image how-tos/observers/customFontOverviewContextualMenu.png %}

> To activate the contextual menu, right-click anywhere in the font window.
{: .note }

{% showcode how-tos/observers/customFontOverviewContextualMenu.py %}

> The same approach can be used to add custom items to other contextual menus in RoboFont. Use the following keys to access the menus for glyphs, guides and images:
>
> - `glyphAdditionContextualMenuItems`
> - `guideAdditionContextualMenuItems`
> - `imageAdditionContextualMenuItems`
{: .tip }
