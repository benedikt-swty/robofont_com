---
layout: page
title: Custom menu in the Glyph Editor
tags:
  - mojo
---

This example shows how to add a custom pop-up menu next to the *Display options* menu in the bottom bar of the Glyph Window.

{% image how-tos/observers/customGlyphWindowDisplayMenu.png %}

The items in the custom menu can be toggled on/off:

{% image how-tos/observers/customGlyphWindowDisplayMenu-2.png %}

{% showcode how-tos/observers/customGlyphWindowDisplayMenu.py %}

> - [Adding a custom “Display…” menu (RoboFont Forum)](http://forum.robofont.com/topic/579/)
{: .seealso }
