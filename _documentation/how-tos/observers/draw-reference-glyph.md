---
layout: page
title: Draw reference glyph
tags:
  - observers
---

This example shows a very simple tool which displays the current glyph in a system font next to the glyph shape in the Glyph Editor using `Subscriber`.

{% image how-tos/observers/draw-reference-glyph.png %}

This is how the code works:

1. the tool subclasses Subscriber

2. the tool overrides three Subscriber methods:
    1. with `build()` accesses the glyphEditor object and its `merz` container
    2. with `started()` creates a [text line sublayer](https://typesupply.github.io/merz/objects/textLine.html) into the `merz` container
    3. with `destroy()` cleans the sublayers from the container once unregistered

3. the tools uses the Subscriber `glyphEditorDidSetGlyph` callback to set the text attribute of the text line sublayer

4. note that the tool is initiated using a `registerGlyphEditorSubscriber`, so the tool will catch only events related to the glyph editor in use

{% showcode how-tos/observers/drawReferenceGlyph.py %}
