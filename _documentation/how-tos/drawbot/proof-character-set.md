---
layout: page
title: Proof character set
tags:
  - scripting
  - drawBot
---

This example shows how to create a simple character set proofer using DrawBot.

{% image how-tos/drawbot/proofCharacterSet.png %}

All glyphs in the font are displayed side-by-side following the order defined in `font.glyphOrder`. Some basic parameters such as page margin and size/spacing of the glyph boxes can be adjusted using the variables at the top of the script.

{% showcode how-tos/drawbot/proofCharacterSet.py %}

> This example could be extended to show the font name and additional information for each glyph, such as the glyph name, unicode, left and right margins, width, guidelines, etc. Give it a try!
{: .tip }
