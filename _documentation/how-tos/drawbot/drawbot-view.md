---
layout: page
title: Using DrawBot with vanilla
tags:
  - scripting
  - drawBot
---

DrawBot can be used together with {% internallink 'topics/vanilla' %} too!

Use the `drawView` to integrate a DrawBot document into your dialogs and tools, or to add an interface around your DrawBot scripts.

{% image how-tos/drawbot/drawbot-vanilla.png %}

{% showcode how-tos/drawbot/drawBotVanillaExample.py %}

> - {% internallink 'tutorials/canvas' %}
{: .seealso }