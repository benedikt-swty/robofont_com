---
layout: page
title: Converting from VFB to UFO
tags:
  - UFO
---

* Table of Contents
{:toc}

There are two main ways to convert between UFO and VFB formats:

1. using Tal Leming’s *UFO Central* script
2. using FontLab’s *vfb2ufo* command-line tool

## Using UFO Central

UFO Central is a tool with many years of service to RoboFab+FontLab users in pre-RoboFont days. It can export VFBs to UFOs and import UFOs into VFBs.

### Installation

- download the [`UFOCentral.py`][UFO Central] script from GitHub
- save it inside your `Library/Application Support/FontLab/Studio 5/Macros` folder
- restart FontLab
- UFO Central will appear in the list of scripts in the *Macro* toolbar

{% image how-tos/converting-from-vfb-to-ufo_UFOCentral-macro.png caption="FontLab’s Macro toolbar" %}

[UFO Central]: http://github.com/typesupply/fontlab-scripts/blob/master/UFOCentral.py

### Usage

Select *UFO Central* from the drop-down list in the *Macro* toolbar, and press the *Run* button. A window with several options will appear:

{% image how-tos/converting-from-vfb-to-ufo_UFOCentral.png caption="UFO Central dialog" %}

The interface is simple and self-explaining. Use the *Help* button to get more information about each option.

### Advantages / disadvantages

<table>
  <tr>
    <th width='50%'>advantages</th>
    <th width='50%'>disadvantages</th>
  </tr>
  <tr>
    <td>
      <ul>
        <li>works consistently and predictably</li>
        <li>has a user-friendly interface</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>does not support hinting data (PS or TT)</li>
        <li>does not export FL kerning classes</li>
        <li>does not support some UFO2 features</li>
      </ul>
    </td>
  </tr>
</table>

## Using vfb2ufo

`vfb2ufo` is a command-line conversion utility provided by FontLab. It is able to convert both ways (from VFB to UFO, and from UFO to VFB), and is available for macOS and Windows users.

### Installation

The code can be downloaded for free [from the FontLab website][vfb2ufo], where installation and usage instructions are also provided.

[vfb2ufo]: http://blog.fontlab.com/font-utility/vfb2ufo/

### Usage

After you have installed `vfb2ufo`, you can run it in Terminal:

```plaintext
vfb2ufo myFolder/myFont.vfb anotherFolder/myOuputFont.ufo
```

The destination path is optional – if it is omitted, the output files will be saved with the same name and in the same folder as the source files.

For the complete list of options, see the included documentation:

```plaintext
vfb2ufo -h
```

### Calling vfb2ufo with Python

Even though `vfb2ufo` is a C program, you can call it with Python using the `subprocess` module:

{% showcode how-tos/vfb2ufo.py %}

Here is another example, showing how to convert a whole folder of VFBs into UFOs:

{% showcode how-tos/vfb2ufo_batch.py %}

### Advantages / disadvantages

<table>
  <tr>
    <th width='50%'>advantages</th>
    <th width='50%'>disadvantages</th>
  </tr>
  <tr>
    <td>
      <ul>
        <li>very fast</li>
        <li>does not require FontLab Studio</li>
        <li>supports hinting data (PS and TT)</li>
        <li>supports Multiple Master VFBs</li>
      </ul>
    </td>
    <td>
      <ul>
        <li>contains a few know bugs*</li>
        <li>relatively recent project</li>
      </ul>
    </td>
  </tr>
</table>

\* Some known bugs in vfb2ufo:

- glyphs with overlapping components are decomposed
- unnamed guidelines get corrupted
- ghost hints are reversed
- OT classes are not written into features (remain as groups)

> - [Free vfb2ufo font converter (FontLab Blog)][vfb2ufo]
> - [vfb2ufo (FontLab Forum)](http://forum.fontlab.com/fontlab-vi-ufo-support-(also-vfb2ufo)/vfb2ufo/)
> - [vfb2ufo (RoboFab mailing list)](http://groups.google.com/forum/#!topic/ufo-spec/q0LKP7eOzr4)
> - [One thing I don’t like in vfb2ufo (TypeDrawers)](http://typedrawers.com/discussion/1859/one-thing-i-dont-like-in-vfb2ufo)
{: .seealso }
