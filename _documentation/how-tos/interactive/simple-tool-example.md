---
layout: page
title: Simple example tool
---

* Table of Contents
{:toc}

The script below demonstrates how interactive tools work. The tool doesn’t do anything useful – it just shows how to react to mouse and keyboard events, and prints out some information for each event that is triggered.

{% showcode how-tos/interactive/simpleExampleTool.py %}


Installing a tool
-----------------

Tools can be installed into the {% internallink 'workspace/glyph-editor' %}’s toolbar with `installTool`:

```python
from mojo.events import installTool
installTool(MyTool())
```

To make your tool available when RoboFont restarts, create an installation script and save it as a [start-up script].

[start-up script]: ../../../reference/workspace/preferences-window/extensions#start-up-scripts


Testing a tool during development
---------------------------------

Below is a simple helper to use while working on your own interactive tools. It provides a dialog to install/uninstall a custom tool during development, without having to restart RoboFont.

{% showcode how-tos/interactive/customToolActivator.py %}
