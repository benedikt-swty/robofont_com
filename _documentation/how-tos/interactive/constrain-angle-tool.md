---
layout: page
title: Constrain angle tool
tags:
  - italics
---

This example shows a custom [EditingTool](../../../../../documentation/reference/api/mojo/mojo-events#mojo.events.EditingTool) which uses the font’s italic angle to constrain point movements – a useful tool when working on slanted designs.

The tool subclasses `EditingTool._mouseDragged` and *disables* its Shift constraint, before adding its own Shift behaviour.

Off-curve points are constrained in relation to their on-curve point, not the last mouse down point.

{% showcode how-tos/interactive/constrainAngleTool.py %}

> - [Constrain to angles other than 0, 90, 45° (RoboFont Forum)](http://forum.robofont.com/topic/802/constrain-to-angles-other-than-0-90-45/4)
{: .seealso }
