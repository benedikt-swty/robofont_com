---
layout: page
title: Polygonal selection tool
---

This example shows a polygonal selection tool based on the [EditingTool].

{% showcode how-tos/interactive/polygonSelection.py %}

[EditingTool]: ../../../reference/api/mojo/mojo-events/#mojo.events.EditingTool
