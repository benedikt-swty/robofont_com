---
layout: page
title: Tutorials
tree:
    - making-a-font
    - making-a-simple-font-family
    - making-a-complex-font-family
    - making-a-multilingual-font
    - making-italic-fonts
    - building-accented-glyphs
    - setting-font-infos
    - setting-font-names
    - drawing-glyphs
    - drawing-with-quadratic-curves
    - checking-contour-quality
    - spacing-intro
    - kerning
    - defining-character-sets
    - sorting-glyphs
    - using-production-names
    - using-mark-colors
    - marking-glyphs
    - working-with-large-fonts
    - generating-fonts
    - using-test-install
    - postscript-hinting
    - truetype-hinting
    - creating-designspace-files
    - creating-variable-fonts
    - testing-variable-fonts
    - python
    - dealing-with-errors
    - using-external-code-editors
    - using-robofont-from-command-line
    - using-git
    - making-proofs-with-drawbot
    - setting-up-a-startup-script
    - upgrading-from-py2-to-py3
    - get-font
    - get-glyph
    - scripts-font
    - scripts-glyph
    - scripts-interpolation
    - using-glyphmath
    - using-pens
    - creating-window-with-action-buttons
    - creating-font-library-browser
    - canvas
    - building-a-custom-key-observer
    - from-tool-to-extension
    - packaging-extensions
    - asking-for-help
    - submitting-bug-reports
    - editing-the-docs
    - writing-how-tos
---

*practical steps · learning-oriented · useful when studying*

<div class='row'>
<div class='col' markdown='1'>
### Making fonts

- {% internallink 'tutorials/making-a-font' %} <span class='green'>✔︎</span>
<!-- - {% internallink 'tutorials/making-a-simple-font-family' %} <span class='red'>✘</span> -->
<!-- - {% internallink 'tutorials/making-a-complex-font-family' %} <span class='red'>✘</span> -->
<!-- - {% internallink 'tutorials/making-a-multilingual-font' %} <span class='red'>✘</span> -->
- {% internallink 'tutorials/making-italic-fonts' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/building-accented-glyphs' %} <span class='green'>✔︎</span>

### Setting font info

- {% internallink 'tutorials/setting-font-infos' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/setting-font-names' %} <span class='yellow'>✔︎</span>

### Drawing & spacing

- {% internallink 'tutorials/drawing-glyphs' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/drawing-with-quadratic-curves' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/checking-contour-quality' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/spacing-intro' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/kerning' %} <span class='green'>✔︎</span>

### Character set

- {% internallink 'tutorials/defining-character-sets' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/sorting-glyphs' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/using-production-names' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/using-mark-colors' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/marking-glyphs' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/working-with-large-fonts' %} <span class='green'>✔︎</span>

### Font generation

- {% internallink 'tutorials/generating-fonts' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/using-test-install' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/postscript-hinting' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/truetype-hinting' %} <span class='green'>✔︎</span>

### Variable fonts

- {% internallink 'tutorials/creating-designspace-files' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/creating-variable-fonts' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/testing-variable-fonts' %} <span class='green'>✔︎</span>

</div>
<div class='col' markdown='1'>

### Building Tools

- {% internallink 'tutorials/python' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/dealing-with-errors' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/using-external-code-editors' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/using-robofont-from-command-line' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/using-git' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/making-proofs-with-drawbot' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/setting-up-a-startup-script' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/upgrading-from-py2-to-py3' %} <span class='green'>✔︎</span>

### FontParts

- {% internallink 'tutorials/get-font' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/get-glyph' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/scripts-font' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/scripts-glyph' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/scripts-interpolation' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/using-glyphmath' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/using-pens' %} <span class='green'>✔︎</span>

### vanilla

- {% internallink 'tutorials/creating-window-with-action-buttons' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/creating-font-library-browser' %} <span class='green'>✔︎</span>

### mojo

- {% internallink 'tutorials/canvas' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/building-a-custom-key-observer' %} <span class='green'>✔︎</span>

### Extensions

- {% internallink 'tutorials/creating-extensions' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/from-tool-to-extension' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/packaging-extensions' %} <span class='green'>✔︎</span>

### Help & support

- {% internallink 'tutorials/asking-for-help' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/submitting-bug-reports' %} <span class='green'>✔︎</span>

### Documentation

- {% internallink 'tutorials/editing-the-docs' %} <span class='green'>✔︎</span>
- {% internallink 'tutorials/writing-how-tos' %} <span class='green'>✔︎</span>
</div>
</div>
