---
layout: page
title: Topics
levels: 1
tree:
    - welcome-to-robofont
    - robofont-design-principles
    - robofont-features-overview
    - robofont-pre-history
    - robofont-history
    - the-ufo-format
    - workspace-overview
    - drawing-environment
    - scripting-environment
    - accented-glyphs
    - production-names
    - smartsets
    - interpolation
    - preparing-for-interpolation
    - kerning
    - variable-fonts
    - robofont-3-changes
    - recommendations-upgrading-RF3
    - robofab-fontparts
    - why-scripting
    - fontparts
    - vanilla
    - merz
    - subscriber
    - window-controller
    - drawbot
    - understanding-contours
    - glyphmath
    - pens
    - transformations
    - boolean-glyphmath
    - interactive-tools
    - the-observer-pattern
    - observers
    - extensions
    - mechanic-2
    - upgrading-code-to-RoboFont4
    - boilerplate-extension
---

*theoretical knowledge · understanding-oriented · useful when studying*

<div class='row'>
<div class='col' markdown='1'>

### Introduction

- {% internallink 'topics/welcome-to-robofont' %} <span class='green'>✔︎</span>
- {% internallink 'topics/robofont-design-principles' %} <span class='green'>✔︎</span>
- {% internallink 'topics/robofont-features-overview' %} <span class='green'>✔︎</span>
- {% internallink 'topics/robofont-pre-history' %} <span class='green'>✔︎</span>
- {% internallink 'topics/robofont-history' %} <span class='yellow'>✔︎</span>
- {% internallink 'topics/the-ufo-format' %} <span class='green'>✔︎</span>

### Workspace

- {% internallink 'topics/workspace-overview' %} <span class='green'>✔︎</span>
- {% internallink 'topics/drawing-environment' %} <span class='green'>✔︎</span>
- {% internallink 'topics/scripting-environment' %} <span class='green'>✔︎</span>

### Making fonts

- {% internallink 'topics/accented-glyphs' %} <span class='green'>✔︎</span>
- {% internallink 'topics/production-names' %} <span class='green'>✔︎</span>
- {% internallink 'topics/smartsets' %} <span class='green'>✔︎</span>
- {% internallink 'topics/interpolation' %} <span class='green'>✔︎</span>
- {% internallink 'topics/preparing-for-interpolation' %} <span class='green'>✔︎</span>
- {% internallink 'topics/kerning' %} <span class='green'>✔︎</span>
- {% internallink 'topics/variable-fonts' %} <span class='green'>✔︎</span>

### Upgrading

- {% internallink 'topics/robofont-3-changes' %} <span class='green'>✔︎</span>
- {% internallink 'topics/recommendations-upgrading-RF3' %} <span class='green'>✔︎</span>
- {% internallink 'topics/upgrading-code-to-RoboFont4' %} <span class='green'>✔︎</span>
- {% internallink 'topics/robofab-fontparts' %} <span class='green'>✔︎</span>

</div>
<div class='col' markdown='1'>

### Scripting

- {% internallink 'topics/why-scripting' %} <span class='green'>✔︎</span>
- {% internallink 'topics/fontparts' %} <span class='green'>✔︎</span>
- {% internallink 'topics/vanilla' %} <span class='green'>✔︎</span>
- {% internallink 'topics/merz' %} <span class='green'>✔︎</span>
- {% internallink 'topics/subscriber' %} <span class='green'>✔︎</span>
- {% internallink 'topics/drawbot' %} <span class='green'>✔︎</span>
- {% internallink 'topics/understanding-contours' %} <span class='green'>✔︎</span>
- {% internallink 'topics/glyphmath' %} <span class='green'>✔︎</span>
- {% internallink 'topics/pens' %} <span class='green'>✔︎</span>
- {% internallink 'topics/transformations' %} <span class='green'>✔︎</span>
- {% internallink 'topics/boolean-glyphmath' %} <span class='green'>✔︎</span>
- {% internallink 'topics/interactive-tools' %} <span class='green'>✔︎</span>
- {% internallink 'topics/the-observer-pattern' %} <span class='green'>✔︎</span>
- {% internallink 'topics/observers' %} <span class='green'>✔︎</span>
- {% internallink 'topics/window-controller' %} <span class='green'>✔︎</span>

### Extensions

- {% internallink 'topics/extensions' %} <span class='green'>✔︎</span>
- {% internallink 'topics/mechanic-2' %} <span class='green'>✔︎</span>
- {% internallink 'topics/boilerplate-extension' %} <span class='green'>✔︎</span>
- {% internallink 'topics/building-extensions' %}  <span class='green'>✔︎</span>
- {% internallink 'topics/publishing-extensions' %}  <span class='green'>✔︎</span>
- <a href="https://extensionstore.robofont.com/certified-developers/">Publish extensions in the Extension Store</a>  <span class='green'>✔︎</span>

</div>
</div>
