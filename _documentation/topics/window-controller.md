---
layout: page
title: Window Controller
tags:
  - scripting
  - Subscriber
---

* Table of Contents
{:toc}

{% image topics/window.png %}

*WindowController is basic vanilla window controller that makes common functionalities easily available*

`WindowController` is a new class shipped with the {% internallink "reference/api/mojo/mojo-subscriber" text="mojo.subscriber" %} module in RoboFont 4.0. It replaces the [`defconAppKit` `BaseWindow`](https://github.com/robotools/defconAppKit/blob/1fa2aa27e9107ffd133c0b379d4fd17fb0aefcfe/Lib/defconAppKit/windows/baseWindow.py#L6) class. Similarly to the `BaseWindow`, `WindowController` is a controller of a [`vanilla`](https://vanilla.robotools.dev) window that adds several common functionalities to a tool that uses a [`vanilla`](https://vanilla.robotools.dev) [`Window`](https://vanilla.robotools.dev/en/latest/objects/Window.html) or [`FloatingWindow`](https://vanilla.robotools.dev/en/latest/objects/FloatingWindow.html). Actions like showing a message, opening a file, or showing a progress bar are immediately available as methods by subclassing `WindowController` in your tool. Check the `WindowController` {% internallink "reference/api/mojo/mojo-subscriber#mojo.subscriber.WindowController" text="reference" %} for a detailed list of methods. 

`Subscriber` and `WindowController` are designed to be compatible in multiple inheritance, so you can benefit from both classes at the same time.

{% showcode topics/baseWindow.py %}

By default, you should store your vanilla window into a `self.w` attribute. If you want to use another name, just overwrite the {% internallink "reference/api/mojo/mojo-subscriber#mojo.subscriber.getWindow" text="`getWindow(self)`" %} method and return your vanilla window object.

{% showcode topics/differentlyNamedWindow.py %}

`WindowController` also allows for a `windowWillClose()` callback that is triggered once the window is closed. That's useful for cleaning `merz` sublayers or cleaning any reference to other objects. The following example opens a new font once initiated and closes it along with the window.

{% showcode topics/windowWillClose.py %}

If your tool inherits from the Subscriber class, you can override the {% internallink "reference/api/mojo/mojo-subscriber#mojo.subscriber.destroy" text="`destroy(self)`" %} method to achieve the same effect

{% showcode topics/subscriberDestroy.py %}

> - Intro to {% internallink "topics/subscriber" text="Subscriber" %}
{: .seealso }
