---
layout: page
title: Merz
tags:
  - merz
  - drawing
---

* Table of Contents
{:toc}

*Merz is a lightweight wrapper around Apple’s [CoreAnimation](https://developer.apple.com/documentation/quartzcore) framework with a Pythonic API that allows for ergonomic, declarative and performant object drawing within RoboFont.* 

Written and contributed by [Tal Leming](https://www.typesupply.com). You can find the detailed documentation [here](https://typesupply.github.io/merz/).

A new way of visualizing data on your screens
----------

Merz is one of the major new features in RoboFont 4.0. Most users will not notice much difference from RoboFont 3.0, but it represents a radical change in how the application visualizes data on the screen. Rendering speed will enormously benefit from this shift.

RoboFont’s extension ecosystem plays an important role in the overall user experience, so the RoboFont team will try to make it as easy as possible for tool developers the switch from `mojo.drawingTools` to Merz.

Merz represents a substantial conceptual shift from `mojo.drawingTools` – and Drawbot. The `mojo.drawingTools` modules follow a strict procedural painter’s model: each drawing operation applies a layer of paint to an output canvas. The paint already applied cannot be modified, only new layers of paint can obscure or change the appearance of the previous layers.

Differently, Merz adopts an object-oriented approach. It isn’t even correct to define Merz – and the Core Animation framework on which Merz relies upon – a drawing system itself. Merz is an infrastructure for compositing and manipulating data organized in layer objects. Or, in other words, Merz allows for creating visual layers of data.

The previous approach
----------

Let’s compare two scripts returning the same output on the screen. The first one uses a drawbot-style approach. You can run this code in the [Drawbot extension](https://github.com/typemytype/drawBotRoboFontExtension) in RoboFont. 

{% showcode topics/merz_vs_drawbot_01.py %}

Look at the sequence of actions: the script accesses the glyph, creates a canvas, adjusts the canvas reference point, draws the glyph, set a line stroke, draws the baseline. It mimics the painter's gestures.

This drawing approach will keep working on RoboFont 4.0, but it is deprecated. It is considerably slower than Merz, and RoboFont 4.0 sets a resolution limit to `mojo.drawingTools` in order to preserve the user experience from rendering delays. The following screenshot is made using the [WurstSchreiber](https://github.com/asaumierdemers/WurstSchreiber) extension – still using `mojo.drawingTools` – at 100%. So far, so good. 

{% image topics/wurst100.png %}

But this is what happens if the users zoom in at 500%. The resolution limit kicks in, and contours become blurry.

{% image topics/wurst500.png %}

Not ideal, but a compromise. For this reason, the RoboFont team encourages extension developers to update their tools to Merz. Most extensions will become faster, and the object-oriented approach of Merz will make *drawing* code better adhere to the tool object structure.

The new approach
----------

The following example uses Merz. You can run this code in the [Merz Playground extension](https://github.com/typesupply/merzplayground).

{% showcode topics/merz_vs_drawbot_02.py %}

Visual elements are initiated with their own properties. They are grouped into a hierarchy, the line and the glyph live into a group layer which is stored inside the view container.

In fact, Merz layers are organized into a tree hierarchy. Layers can be nested in sublayers and grouped into containers. At the top of the hierarchy, there is a view. Above views, you’ll find a window.

{% image topics/merz_diagram.png %}

The [Merz Playground extension](https://github.com/typesupply/merzplayground) provides a view “free of charge”, which will contain the layers created in the code. Outside the playground, you can use Merz to render layers in vanilla views or in RoboFont’s glyph editor. Here’s a variation of the previous example adapted to render into a vanilla `FloatingWindow`. You can run the following code from RoboFont's scripting window:

{% showcode topics/merz_vanilla.py %}

Great, right? To improve the script, we should tie the rendering to user events. Each time the CurrentGlyph changes, our `GlyphVisualizer` tool should update. Here comes [Subscriber]({% link _documentation/topics/subscriber.md %}), the other major new feature of RoboFont 4.0. Combined with [Subscriber]({% link _documentation/topics/subscriber.md %}), Merz really shines. See how simple it is to listen to changes in the glyph data and update their representation on screen:

{% showcode topics/merz_vanilla_subscriber_01.py %}

Merz layers have a geometry – a combination of position, size, and affine transformation attributes – and several graphical properties as color, bezier path, text, font, and so on. You can find the details on the [Merz documentation](https://typesupply.github.io/merz/). Every attribute can be set when a layer is created and most properties can be changed after the layer creation. These layer attributes affect the rendering on the screen effortlessly. See how simple it is to implement a 🌞/🌛 mode switch for our previous example:

{% showcode topics/merz_vanilla_subscriber_02.py %}

Note that you can pass a bunch of views to a `vanilla.VerticalStackView` and let the views distribute themselves in the `FloatingWindow`.

> Check these extensive examples of combined usage of `Subscriber` and `Merz`
> + {% internallink 'how-tos/mojo/subscribing-to-current-font' %}
> + {% internallink 'how-tos/mojo/subscribing-to-current-glyph' %}
> + {% internallink 'how-tos/mojo/subscribing-to-glyph-editor' %}
> + {% internallink 'how-tos/mojo/subscribing-to-robofont-application' %}
{: .tip}
