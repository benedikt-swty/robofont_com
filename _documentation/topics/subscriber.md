---
layout: page
title: Subscriber
tags:
  - subscriber
  - drawing
  - scripting
---

* Table of Contents
{:toc}

*Subscriber is an event manager for scripters, making the bridge of subscribing and unsubsribing all sort of events super easy.*

Written and contributed by [Tal Leming](http://www.typesupply.com/).

## Overview

When scripting in RoboFont, it is helpful to get notified about events. A user clicks on the mouse, a font opens, the sidebearings in a glyph change value, and so on. If the tool you are working on requires reacting to the user’s actions, you need to tie some code to user events. 

That’s a different approach compared to a procedural script, where a set of actions is lined up sequentially. For example:

+ open a font
+ remove outlines
+ generate a binary font
+ close the font

In this case, there is not need to tie any code to user’s events.

Think of RoboFont as a mailman, handling events and keeping updated whoever requested notifications about these events.

> Dear Sir/Madam,
>
> I just opened “someFont.ufo”. This is the path, here you can access the content. Please let me know if I need to do something with it. I’ll keep you posted with updates.
>
> All the best,<br/>
> RoboFont Notification Manager

Notifications are great for two reasons:

+ they allow a tool to listen to specific events, not every event
+ they allow a tool to trigger custom code when a notification is sent, through a callback function

Wait, what? A callback function? Is that a new type of function? No, just a regular function. Usually a method part of a class. If you have ever used [vanilla](https://vanilla.robotools.dev) to build some user interface elements, you have already used callbacks. Look at this snippet from the vanilla [Button](https://vanilla.robotools.dev/en/latest/objects/Button.html) object reference page

{% showcode topics/vanillaButtonDemo.py %}

You can run this code in the RoboFont scripting window. Check the print statements in the output window. Take yourself a moment to edit the `buttonCallback` function and check the results.

Let’s look at another example. Each time we change the spacing of the letter ‘a’, all accented letters using ‘a’ as a base glyph could/should be updated automatically. In the old way of subscribing to notifications (before RoboFont 4.0) this could be obtained using [defcon](https://defcon.robotools.dev/en/latest/). The new subscriber modules makes it so easy:

{% showcode topics/subscriber_intro.py %}

Over the years RoboFont made it easy to subscribe to lots and lots of events, but users were not incentivised to unsubscribe once they stopped listening. For example, if you don’t need to listen anymore to user’s clicks in the glyph view, you are supposed to inform RoboFont. You should explicitly unsubscribe from the notification. For different reasons, the unsubscription often does not happen and RoboFont keeps dispatching useless notifications around slowing down the application.

So, it was time to reconsider the way notifications are handled. For this reason, Tal and Frederik worked hard to ship Subscriber with RoboFont 4.0, a layer on top of the general notification system, to make life easier for all the scripters out there!

## Three different approaches

Subscriber considers three general approaches for subscribing to events. More setups are available to advanced developer, but the following ones cover all the use cases in the RoboFont extension ecosystems. There a couple of aspects to consider in order to choose the right approach:

+ you should aim to trigger as few events as possible to solve the task at hand. Some callbacks react very frequently to user events and they could significantly slower down the application. The {% internallink "reference/api/mojo/mojo-subscriber" text="Subscriber reference" %} has a very detailed list of events you could subscribe to. If you need to watch the current glyph metrics, you could use the `currentGlyphDidChange` callback but that would be a waste. The callback will react also when other glyph attributes change. The `currentGlyphMetricsDidChange` is more specific and less wasteful. Consider that the tool you are working on will probably run alongside many others. Almost every extension listens to events, so you should try to achieve your task with just the right amount of notifications. It will make the user interaction more responsive.
+ you should think about the *geometry* of the notification subscriptions. Does the tool look to several glyphs across several fonts? Does it listen to multiple glyph editors at once? Or, does it just look at the current glyph? These considerations are very important when choosing how to register your subscriber object.

Let's look at the possible *geometries* with some examples.

### One object to one subscriber

You want to listen to changes to the current glyph and draw directly in the glyph editor. Tools with functionalities similar to [SpeedPunk](https://github.com/yanone/speedpunk) or [WurstSchreiber](https://github.com/asaumierdemers/WurstSchreiber). The following example draws two colored neighbours near the current glyph. Note that the tool is registered using the `registerGlyphEditorSubscriber()` function, and not regularly initiated with the default Neighbours() constructor. 

{% showcode topics/subscriberGlyphEditor.py %}

### Multiple objects to one subscriber

This approach is useful when a single subscriber object receives notifications from multiple objects. Mostly used when a script has a single window listening to events. For example, let's say that you want to keep a constant reference of the current glyph in a separate [vanilla window](https://vanilla.robotools.dev/en/latest/objects/Window.html). The window does not belong to any open font, so the source glyph could come from any opened ufos. Tools with functionalities similar to [Outliner](https://github.com/typemytype/outlinerRoboFontExtension) would fit into this *geometry*.

{% showcode topics/merz_vanilla_subscriber_01.py %}

### Observe many glyphs (or font attributes)

This use case might be the most complex one. Extension like [MetricsMachine](https://extensionstore.robofont.com/extensions/metricsMachine/), [Prepolator](https://extensionstore.robofont.com/extensions/prepolator/) or [GroundControl](https://github.com/roboDocs/GroundControl) would definitely use this kind of scheme. When a tool needs to listen to a set of glyphs (never listen to all glyphs in a font otherwise you'll wreck RoboFont performance) coming from different sources plus a bunch of font attributes as `font.kerning`, you should override the `setAdjunctObjectsToObserve` method and register the tool using `registerCurrentFontSubscriber()`. The following example collects the first three non-empty glyph in the current font and display them with a merz gaussian blur filter.

{% showcode topics/observeMultipleGlyphs.py %}

## Event coalescing

Subscriber uses a technique called coalescing events to avoid posting events redundantly. For example, a callback like `currentGlyphDidChange` posts several events during a single mouse drag while editing a contour. Try to edit a glyph while running this script. You can check the log in the output window.

{% showcode topics/mouseFollower.py %}

That's a lot of events, right? That's great if you need to update a real-time visualization using glyph data, as in the "One to one" approach example. It could be an issue if you instead tie a heavier operation to the callback, like autosaving the font you are working on.

{% showcode topics/autosaver.py %}

For this reason, Subscriber provides the opportunity to set a coalescing event delay for each available callback. You just need to create an attribute in your Subscriber subclass with the same method name plus `Delay` and set a `Float` value representing seconds. If you want an event to be posted as soon as possible, set the value to `0`. A few examples

{% showcode topics/delayExamples.py %}

> Any event with `Will` or `Wants` in its name should always have a delay of `0` to prevent unexpected asynchronous behavior.
{: .warning }

Robofont sets two default coalescing events delay values:
+ 1/30s for defcon objects callback, like `currentGlyphDidChange`
    + `defcon.Font`
    + `defcon.Info`
    + `defcon.Kerning`
    + `defcon.Groups`
    + `defcon.Features`
    + `defcon.LayerSet`
    + `defcon.Layer`
    + `defcon.Glyph`

+ No delay for Robofont components callbacks, like `glyphEditorDidSetGlyph`
    + RoboFont
    + Font Document
    + Font Overview
    + Glyph Editor
    + Space Center

> Check the [subscriber reference]({{ site.baseUrl }}/documentation/reference/api/mojo/mojo-subscriber) for a detailed list of callbacks
{: .tip }

> Check these extensive examples of combined usage of `Subscriber` and `Merz`
> + {% internallink 'how-tos/mojo/subscribing-to-current-font' %}
> + {% internallink 'how-tos/mojo/subscribing-to-current-glyph' %}
> + {% internallink 'how-tos/mojo/subscribing-to-glyph-editor' %}
> + {% internallink 'how-tos/mojo/subscribing-to-robofont-application' %}
{: .tip}
