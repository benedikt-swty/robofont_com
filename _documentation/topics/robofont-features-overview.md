---
layout: page
title: RoboFont features overview
treeTitle: Features overview
---

* Table of Contents
{:toc}

*RoboFont does everything a modern font editor should do, and more!*

Editing glyphs
--------------

{% internallink "workspace/font-overview" %}
: Visualizing a font’s glyph set as a grid of cells or table. Adding and removing glyphs, changing the glyph order.

{% internallink "workspace/glyph-editor" %}
: Creating and editing glyph data: contours, components, anchors, guides, layers, images. Highly customizable with extensions!


Organising glyphs
-----------------

{% internallink "workspace/font-overview/groups-editor" %}
: Creating and editing font-specific glyph groups.

{% internallink "workspace/font-overview/smart-sets" text="Smart Sets" %}
: Filtering glyphs according to search queries or a list of glyph names. Saved together with your user preferences or inside the font.


Spacing & Kerning
-----------------

{% internallink "workspace/space-center" %}
: Adjusting glyph metrics, previewing short text samples.

{% internallink "workspace/font-overview/kern-center" %}
: Creating, editing and previewing font kerning.


Customizing the app
-------------------

{% internallink "workspace/preferences-window" %}
: An interface to configure several types of user preferences.

{% internallink "workspace/preferences-editor" %}
: Editing several other configuration values directly.

{% internallink "topics/extensions" text='Extensions' %}
: Using, installing, developing and publishing open-source and commercial extensions.


Mastering fonts
---------------

{% internallink "workspace/font-overview/font-info-sheet" text='Font Info' %}
: Editing several kinds of font metadata.

{% internallink "workspace/font-overview/features-editor" %}
: Writing and editing OpenType features code.

[Feature Preview](http://github.com/typemytype/featurePreviewRoboFontExtension)
: Previewing OpenType features in a UFO font (using a generated test font).

{% internallink "tutorials/generating-fonts" %}
: Generating OpenType CFF/TTF fonts and additional formats.


Proofing fonts
--------------

{% internallink "tutorials/using-test-install" text="Test Install" %}
: Installing the current font into the system for testing.

{% internallink "tutorials/making-proofs-with-drawbot" text="DrawBot extension" %}
: Creating all kinds of visual font samples in various image formats.


Python Scripting
----------------

{% internallink "workspace/scripting-window" %}
: Writing Python code to modify fonts and creating your own tools.

{% internallink "workspace/output-window" %}
: Viewing text output from scripts, debugging.


<!--
Licensing
---------

{% internallink "licensing" text="Licenses" %}
: Several licensing options for students, teachers, professionals and companies.

{% internallink "workspace/license-window" %}
: Easy drag-and-drop license activation with the License Window.
-->
