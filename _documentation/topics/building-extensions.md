---
layout: page
title: Recommendations for Building Extensions
treeTitle: Building Recommendations
tags:
  - extensions
  - recommendations
---

* Table of Contents
{:toc}

## User interface

Here at RoboFont we are not too strict concerning user interface components. If you want to use a series of checkboxes to build an interactive piano keyboard, go for it. Just be aware that checkboxes were probably not designed with that application in mind.

Consider that the user interface components you can access from the `vanilla` module are distributed by Apple along with some [guidelines](http://developer.apple.com/library/content/documentation/UserExperience/Conceptual/OSXHIGuidelines/). If you want to move away from convention, we suggest you to do it consciously.

## Code

### mojo vs. lib

We recommend using tools from the `mojo` module rather than `lib`

```python
# this has been deprecated:
from lib.tools.defaults import getDefault

# use this instead:
from mojo.UI import getDefault
```

also:

```python
from mojo.pens import decomposePen
```

There are two main reasons for following this recommendation:

1. `mojo` editing functions follow the `fontParts` API, so they are easier to use for scripter. Differently, the tools in the `lib` module are more defcon-oriented, so they use another API with a different logic
2. the `lib` module is not a publicly documented API, so it could change over time without explicit warning

### Move to Merz & Subscriber

We really encourage developers to move to `Merz` and `Subscriber`. These new modules increase users experience quality and they make your tool easier to develop.

> - {% internallink "topics/subscriber" text="Subscriber introduction article" %}
> - {% internallink "reference/api/mojo/mojo-subscriber" text="Subscriber reference" %}
> - {% internallink "topics/merz" text="Merz introduction article" %}
> - [Merz reference](https://typesupply.github.io/merz/)
{: .seealso }

### Be kind with user's data

Don't do funky things with user's data! Before uploading extensions to Mechanic, the RoboFont team will check the extension for malicious operations and suggest improvements to the user experience.

### Code style

Code style can be a controversial topic of debate (remember tabs vs spaces?) between programmers. There is no strict rule to follow, just be aware that there is well established [Python Style Guide](https://www.python.org/dev/peps/pep-0008/) out there.

> - {% internallink "/topics/extensions" %}
> - {% internallink "/tutorials/from-tool-to-extension" %}
> - {% internallink "/how-tos/extensions/building-extensions-with-extension-builder" %}
> - {% internallink "/reference/extensions/extension-item-format" %}
> - {% internallink "/tutorials/packaging-extensions" %}
> - {% internallink "/how-tos/extensions/publishing-extensions" %}
> - {% internallink "/tutorials/using-git" %}
{: .seealso }
