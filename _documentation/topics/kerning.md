---
layout: page
title: Kerning
tags:
  - kerning
---

* Table of Contents
{:toc}


Basics
------

Kerning is the local optimization of whitespace between of glyphs by adding/subtracting a certain number of units from the default spacing.

Kerning is expressed as a list of *glyph pairs* and corresponding *kerning values*:

```text
# pseudocode

V A −50
T a -40
f parenright 95
```

{% image how-tos/adding-kern-pairs_on-off.png caption='kerning OFF (top) vs. kerning ON (bottom)' %}

Kerning pairs can also be defined using *kerning groups* – this allows us to cover a larger number of combinations with fewer pairs.

```text
# pseudocode

@T_right: T Tcommaaccent Tcaron Tcedilla Tbar
@r_right: r racute rcaron rcommaaccent
@A_left: A Aacute Acircumflex Agrave Atilde
@a_left: a aacute acircumflex agrave atilde
@f_left: f fi fl germandbls

V @A_left −50
@T_right @a_left -40
@r_right @f_left 15
```

A kerning group should contain glyphs which share the same left- or right-side shape. These can be accented versions of a base glyph, ligatures, or any other glyph with similar forms.

{% image how-tos/adding-kern-pairs_groups.png caption='kerning pairs using groups'%}

## Naming of kerning groups

The {% glossary pseudocode %} example above demonstrates a problem with using `left` and `right` in kerning group names. These qualifiers can mean two opposite things:

1. glyphs which are the left/right glyph in a kerning pair
2. glyphs which share the same left/right-side shape

> In addition to this, there’s also the fact that writing direction can be left-to-right or right-to-left depending on the script – this affects the kerning direction as well.
{: .note }

To remove this ambiguity, it is recommended to think about kerning in terms of “first” and “second” glyphs in a pair. This is in line with the [UFO3 specification] which defines `public.kern1.` and `public.kern2.` as standard prefixes for kerning groups.

[UFO3 specification]: http://unifiedfontobject.org/versions/ufo3/groups.plist/

With that in mind, here’s an updated version of the above example:

```text
# pseudocode

@T_1st: T Tcommaaccent Tcaron Tcedilla Tbar
@r_1st: r racute rcaron rcommaaccent
@A_2nd: A Aacute Acircumflex Agrave Atilde
@a_2nd: a aacute acircumflex agrave atilde
@f_2nd: f fi fl germandbls

V @A_2nd −50
@T_1st @a_2nd -40
@r_1st @f_2nd 15
```


Kerning in OpenType fonts
-------------------------

The OpenType format has two places for storing kerning data:

[kern table]
: ^
  The `kern` table is a simple flat table with kerning pairs and values. Originally a TrueType table, it is also allowed in OpenType-CFF fonts (but not supported by all engines).

  → *Use it only for compatibility with legacy environments.*

[kern feature]
: ^
  Kerning data can also be expressed as {% glossary GPOS %} feature code. This allows other kerning patterns such as triplets and quadruplets, adjustment of glyph placement independent of the advance adjustment, etc.

  → *This is the recommended kerning mechanism for OpenType fonts.*

A font may contain both types of kerning, for use by different environments.

> - [Recommendations (OpenType spec)](http://docs.microsoft.com/en-us/typography/opentype/spec/recom#39kern39-table)
{: .seealso }

[kern table]: http://docs.microsoft.com/en-us/typography/opentype/spec/kern
[kern feature]: http://docs.microsoft.com/en-us/typography/opentype/spec/features_ko#a-namekern-idkernatag-39kern39


Kerning in UFO
--------------

There are two ways in which kerning data can be stored in UFOs:

[kerning.plist]
: Kerning pairs and values are stored as XML and can be accessed with Python at `font.kerning`.

[features.fea]
: Kerning data is stored as OpenType code in a `.fea` file, and can be accessed with Python at `font.features` – but only as a big string with all other features.

### From UFO kerning to OpenType kerning

Only one type of kerning is used when generating OpenType fonts, and kerning in the `kern` feature has precedence over `font.kerning`.

1. If the font contains a `kern` feature, then any data in `font.kerning` is ignored.

2. If `font.kerning` contains data and the font does *not* contain a `kern` feature, RoboFont will generate the `kern` feature automatically for the pairs in `font.kerning`.

[kerning.plist]: http://unifiedfontobject.org/versions/ufo3/kerning.plist/
[features.fea]: http://unifiedfontobject.org/versions/ufo3/features.fea/

- - -

Typeface used in the illustrations: [IBM Plex Serif](http://github.com/IBM/plex)

> - [Kerning pairs should be defined as "visual left to right"](https://github.com/unified-font-object/ufo-spec/issues/96)
> - [Contextual kerning in RF, MM?](https://forum.robofont.com/topic/869/contextual-kerning-in-rb-mm)
{: .seealso }
