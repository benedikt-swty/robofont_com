---
layout: page
title: DrawBot
---

* Table of Contents
{:toc}

DrawBot is an environment for creating graphics programatically in macOS using Python. It is open-source and can be downloaded freely from [drawbot.com](http://drawbot.com).

{% image topics/drawbot-icon.png %}

DrawBot was originally developed by {% glossary Just van Rossum %} as an educational tool for teaching programming to designers, and has grown into a full-fledged platform for automating the creation of all kinds of graphic material.

multiple output formats
: DrawBot can save vector graphics (SVG, PDF), raster images (PNG, JPEG, TIFF, GIF, BMP) and animations (GIF, mov, mp4).

advanced typography
: DrawBot offers a full range of typographic controls, including support for OpenType features and variable fonts, hyphenation, tabulation, access to font metrics etc.

interaction with code
: DrawBot includes a small set of UI controls – sliders, buttons, input fields, etc. – which can be used to add interaction to your scripts. The code editor has special {% internallink "workspace/scripting-window#code-interaction" text='code interaction' %} features for some types of values, and a live coding mode which executes the code as you type.


DrawBot app
-----------

DrawBot is a stand-alone application for macOS. The app includes a Python interpreter, and a development environment with a code editor and a programmable canvas. A {% glossary pip %} package installer is also included.

{% image topics/drawbot.png %}


DrawBot module
--------------

DrawBot can also be used as a Python module, without the UI. After installing the module, you can use `import drawBot` in your scripts and generate images with code.

> - {% internallink 'how-tos/drawbot/drawbot-module' %}
{: .seealso }


DrawBot extension for RoboFont 
------------------------------

DrawBot is available as a [RoboFont extension][DrawBot extension] too!

Because it runs inside RoboFont, this version of DrawBot has access to the entire RoboFont API, including `fontParts.world` objects such as `CurrentFont`, `CurrentGlyph`, `AllFonts` etc. It also provides a handy `drawGlyph(glyph)` function to easily draw a {% internallink "api/fontParts/rglyph" %} objects into the DrawBot canvas.

{% image topics/drawbot-extension.png %}

[DrawBot extension]: http://github.com/typemytype/drawBotRoboFontExtension

> - {% internallink "topics/extensions" %}
{: .seealso }


DrawBot package format
----------------------

DrawBot scripts often require additional files such as image resources, Python modules, data files, etc. The `.drawbot` package format makes it easier to share and distribute such multi-file projects.

{% image topics/drawbot-package-icon.png %}

DrawBot packages can be built using the Package Builder window included in DrawBot. Open a `.drawbot` file by double-clicking or dropping it on top of DrawBot.

> - [DrawBot Package](http://www.drawbot.com/content/drawBotApp/drawBotPackage.html)
{: .seealso }
