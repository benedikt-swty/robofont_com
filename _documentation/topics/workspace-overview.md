---
layout: page
title: Workspace overview
slideShows:
  fontOverview:
    - image: extraDocs/carousel/workspace/font-overview-2.png
      caption: |
        [Smart Sets panel](../../reference/workspace/font-overview/smart-sets-panel)
    - image: extraDocs/carousel/workspace/font-info-2.png
      caption: |
        [Font Info sheet](../../reference/workspace/font-overview/font-info-sheet)
    - image: extraDocs/carousel/workspace/groups-editor-2.png
      caption: |
        [Groups Editor](../../reference/workspace/font-overview/groups-editor)
    - image: reference/workspace/font-overview/features-editor.png
      caption: |
        [Features Editor](../../reference/workspace/font-overview/features-editor)
    - image: extraDocs/carousel/workspace//kern-center.png
      caption: |
        [Kern Center](../../reference/workspace/font-overview/kern-center)
  preferences:
    - image: reference/workspace/preferences-window/preferences_glyph-view-display.png
      caption: |
        [Preferences Window](../../reference/workspace/preferences-window)
    - image: reference/workspace/preferences-window/preferences_glyph-view-appearance.png
    - image: reference/workspace/preferences-window/preferences_font-overview_character-set.png
    - image: reference/workspace/preferences-window/preferences_short-keys.png
    - image: reference/workspace/preferences-editor.png
      caption: |
        [Preferences Editor](../../reference/workspace/preferences-editor)
  autoPlay: false
  height: 550
---

* Table of Contents
{:toc}

*This page gives an overview of the RoboFont user interface.*


When you open RoboFont, no window will appear – just the standard macOS {% internallink 'workspace/application-menu' %} at the top of the main screen, with quick access to all the main windows and commands.

{% image reference/workspace/application-menu.png %}


Font Overview
-------------

The main window in RoboFont is the {% internallink 'workspace/font-overview' %}. It appears when a new font is created (*File > New* or ⌘ N), or when an existing font is opened (*File > Open* or ⌘ O).

{% image reference/workspace/font-overview.png caption='Font Overview window' %}

The Font Overview shows all glyphs in a font, and allows you to do many things with them. Double-clicking a glyph cell opens the Glyph Editor to edit the glyph.

Attached to the Font Overview are several specialized panels and sheets to edit different kinds of font data ({% internallink 'workspace/font-overview/font-info-sheet' text='font info' %}, {% internallink 'workspace/font-overview/groups-editor' text='groups' %}, {% internallink 'workspace/font-overview/kern-center' text='kerning' %}, {% internallink 'workspace/font-overview/features-editor' text='features' %}) or do something to the glyph set ({% internallink 'workspace/font-overview/search-glyphs-panel' text='search' %}, {% internallink 'workspace/font-overview/sort-glyphs-sheet' text='sort' %}, {% internallink 'workspace/font-overview/smart-sets-panel' text='collect' %} and {% internallink 'workspace/font-overview/add-glyphs-sheet' text='add' %} glyphs).

{% include slideshows items=page.slideShows.fontOverview %}


Glyph Editor
------------

The {% internallink 'workspace/glyph-editor' %} is where the contours and other kinds of glyph-level data (width and margins, anchors, components, layers, guides, images) can be edited.

{% image tutorials/glyph-editor.png %}

The Glyph Editor’s top bar offers interactive tools to manipulate points and contours, to draw new contours, to measure distances and to cut contours. Several other [editing tools](#) are available as extensions.

{% image topics/glyph-editor_tools.png caption='Editing Tool, Pen Tool, Slice Tool, Measuring Tool' %}

The Glyph Editor supports zooming in/out and allows you to navigate through all layers in the glyph. The *Display* menu at the bottom left can be used to show/hide different kinds of glyph data.


Inspector
---------

The {% internallink 'workspace/inspector' %} (*Window > Inspector* or ⌘ I) provides additional tools to visualize and edit data in the current glyph.

<div style='column-count:3;'>
{% image reference/workspace/inspector_glyph.png %}
{% image reference/workspace/glyph-editor/inspector_layers.png %}
{% image reference/workspace/glyph-editor/inspector_transform.png %}
</div>


Space Center
------------

The {% internallink 'workspace/space-center' %} is a dedicated window for previewing small strings of text and editing glyph metrics.

{% image reference/workspace/space-center.png %}

The Space Center offers special tools to aid with spacing, such as the *beam* to measure margins at a given horizontal cross-section, or the *Cut Off* display mode for better visualization of countershapes. Several other display options are available from the menu at the top right.

The Space Matrix at the bottom supports numerical values and also references and calculations with other glyphs.


Single Font Window
------------------

RoboFont uses the *Multi Window mode* by default, with {% internallink 'workspace/font-overview' %}, in which {% internallink 'workspace/glyph-editor' %}, {% internallink 'workspace/space-center' %} and {% internallink 'workspace/inspector' %} as separate windows. This mode allows you to move the windows freely across multiple screens, and open multiple Glyph Editors or Space Centers for the same font.

{% image reference/workspace/window-modes_multi.png caption='Multi Window mode' %}

RoboFont also offers a *Single Window mode* in which these windows are united as sections of a single window. This compact mode is useful when working with a single monitor and/or a small screen.

{% image reference/workspace/window-modes_single.png caption='Single Window mode' %}


Preferences
-----------

The {% internallink 'workspace/window-modes' text='window mode' %} and lots of other options can be set in the {% internallink 'workspace/preferences-window' %}. Additional settings are accessible with the {% internallink 'workspace/preferences-editor' %}.

{% include slideshows items=page.slideShows.preferences %}


Scripting
-----------

RoboFont’s main scripting environment is the {% internallink 'workspace/scripting-window' %}, which allows you write and run Python code and navigate through a folder with scripts. There’s also an {% internallink 'workspace/output-window' %} to catch print statements and tracebacks from scripts which are not running in the Scripting Window.

{% image reference/workspace/scripting-window.png caption='Scripting Window'%}

A {% internallink 'workspace/package-installer' %} is also available to install external packages in RoboFont using {% glossary pip %}

{% image reference/workspace/package-installer.png caption='Package Installer' %}

- - -

Other windows
-------------

Other RoboFont windows not mentioned above:

<div class='row'>
<div class='col' markdown=1>
{% image reference/workspace/about-window.png %}
</div>
<div class='col' markdown=1 style='vertical-align:top;'>
{% internallink 'workspace/about-window' %}
: show app credits and version info
</div>
</div>

<div class='row'>
<div class='col' markdown=1>
{% image reference/workspace/license-window.png %}
</div>
<div class='col' markdown=1 style='vertical-align:top;'>
{% internallink 'workspace/license-window' %}
: display and validate the user’s license file
</div>
</div>

<div class='row'>
<div class='col' markdown=1>
{% image reference/workspace/font-overview/external-changes.png %}
</div>
<div class='col' markdown=1 style='vertical-align:top;'>
{% internallink 'reference/workspace/external-changes-window' %}
: a modal dialog to selectively integrate external changes to UFOs which are currently open in RoboFont
</div>
</div>

<div class='row'>
<div class='col' markdown=1>
{% image topics/extension-builder-small.png %}
</div>
<div class='col' markdown=1 style='vertical-align:top;'>
{% internallink 'workspace/extension-builder' %}
: tool for creating your own extension packages
</div>
</div>

