---
layout: page
title: RoboFab vs. FontParts APIs
tags:
  - RF3
  - RoboFab
  - FontParts
---

* Table of Contents
{:toc}

*This page gives an overview of the differences between the RoboFab and FontParts APIs.*

Scripts using the RoboFab API can be updated to the FontParts API with only a few small changes.


API differences
---------------

### General

<table>
  <tr>
    <th width="45%">RoboFab</th>
    <th width="55%">FontParts</th>
  </tr>
  <tr>
    <td><code>OpenFont(ufoPath, showUI=False)</code></td>
    <td><code>OpenFont(ufoPath, showInterface=False)</code></td>
  </tr>
  <tr>
    <td><code>*.move((x, y))</code></td>
    <td><code>*.moveBy((x, y))</code></td>
  </tr>
  <tr>
    <td><code>*.scale((x, y), center=None)</code></td>
    <td><code>*.scaleBy((x, y), origin=None)</code></td>
  </tr>
  <tr>
    <td><code>*.skew(value, offset=None)</code></td>
    <td><code>*.skewBy((x, y), origin=None)</code></td>
  </tr>
  <tr>
    <td><code>*.rotate(value, offset=None)</code></td>
    <td><code>*.rotateBy((x, y), origin=None)</code></td>
  </tr>

  <tr>
    <td><code>*.update()</code></td>
    <td><code>*.changed()</code></td>
  </tr>
</table>

### RFont

<table>
  <tr>
    <th width="45%">RoboFab</th>
    <th width="55%">FontParts</th>
  </tr>
  <tr>
    <td><code>.selection</code></td>
    <td><code>.selectedGlyphNames</code></td>
  </tr>
  <tr>
    <td><code>.guides</code></td>
    <td><code>.guidelines</code></td>
  </tr>
  <tr>
    <td><code>.addGuide()</code></td>
    <td><code>.appendGuideline()</code></td>
  </tr>
  <tr>
    <td><code>.removeGuide()</code></td>
    <td><code>.removeGuideline()</code></td>
  </tr>
  <tr>
    <td><code>.clearGuides()</code></td>
    <td><code>.clearGuidelines()</code></td>
  </tr>
  <tr>
    <td><code>.generate(path, format)</code></td>
    <td><code>.generate(format, path)</code></td>
  </tr>
  <tr>
    <td><code>.compileGlyph()</code></td>
    <td>implemented in RF3 <span class="red">(deprecated in FontParts)</span></td>
  </tr>
</table>

> The `RFont.compileGlyph` method was deprecated in FontParts because it was considered too limited.
>
> [Glyph Construction] is recommended as a more powerful approach to building glyphs. The `glyphConstruction` module is now embedded in RoboFont 3.
> 
> See {% internallink 'how-tos/building-accented-glyphs-with-a-script' %} for more information and examples.
{: .note }

[Glyph Construction]: http://github.com/typemytype/GlyphConstruction

### RGlyph

<table>
  <tr>
    <th width="45%">RoboFab</th>
    <th width="55%">FontParts</th>
  </tr>
  <tr>
    <td><code>.getLayer('foreground')</code></td>
    <td><code>.getLayer('public.default')</code></td>
  </tr>
  <tr>
    <td><code>.box</code></td>
    <td><code>.bounds</code></td>
  </tr>
  <tr>
    <td><code>.mark</code></td>
    <td><code>.markColor</code></td>
  </tr>
  <tr>
    <td><code>.selection</code></td>
    <td><span class="red">(deprecated in FontParts)</span> see <a href='#object-selection-api'>Object selection API</a></td>
  </tr>
  <tr>
    <td><code>.isCompatible(aGlyph, report)</code></td>
    <td><code>.isCompatible(aGlyph)</code></td>
  </tr>
  <tr>
    <td><code>.guides</code></td>
    <td><code>.guidelines</code></td>
  </tr>
  <tr>
    <td><code>.addGuide()</code></td>
    <td><code>.appendGuideline()</code></td>
  </tr>
  <tr>
    <td><code>.clearGuides()</code></td>
    <td><code>.clearGuidelines()</code></td>
  </tr>
  <tr>
    <td><code>.autoUnicodes()</code></td>
    <td>implemented in RF3 <span class="red">(deprecated in FontParts)</span></td>
  </tr>
  <tr>
    <td><code>.rasterize()</code></td>
    <td><span class="red">deprecated in RF3 &amp; FontParts</span></td>
  </tr>
</table>

> - {% internallink "tutorials/scripts-glyph#rasterizing-a-glyph-shape" text="Rasterizing a glyph shape" %}
{: .seealso }

### RAnchor

<table>
  <tr>
    <th width="45%">RoboFab</th>
    <th width="55%">FontParts</th>
  </tr>
  <tr>
    <td><code>.position</code></td>
    <td><code>anchor.x, anchor.y</code></td>
  </tr>
</table>


Importing font objects
----------------------

In RoboFab, the main font objects for the current environment were imported from the `robofab.world` module:

```python
from robofab.world import RFont, RGlyph # etc
```

In FontParts, the corresponding objects are found in the `fontParts.world` module:

```python
from fontParts.world import RFont, RGlyph # etc
```

> FontParts objects are needed only when working outside of RoboFont. When coding in RoboFont, you can use the native objects from `mojo.roboFont` instead.
{: .tip }

> - {% internallink "topics/fontparts#font-objects-out-of-the-box" text="Font objects out-of-the-box" %}
{: .seealso }


Pens
----

RoboFab pens were reorganized and moved to other packages.

**FontParts does not include pens.**

Converter pens were moved into the [ufoLib], to isolate the implementation of the `PointPen` infrastructure and protocol. Other pens were moved to the new [fontPens] package, and other to [fontTools].

> The `ufoLib` is now part of FontTools. From RoboFont 3.2 onwards, all pens from the `ufoLib.pointPen` module can also be found in `fontTools.pens.pointPen`.
>
> The `ufoLib` module will be deprecated.
{: .note }

The following table provides a list of all RoboFab pens and their new locations.

<table>
  <tr>
    <th colspan='2'><code>robofab.pens.adapterPens</code></th>
  </tr>
  <tr>
    <td><code>PointToSegmentPen</code></td>
    <td><code>ufoLib.pointPen.PointToSegmentPen</code></td>
  </tr>
  <tr>
    <td><code>SegmentToPointPen</code></td>
    <td><code>ufoLib.pointPen.SegmentToPointPen</code></td>
  </tr>
  <tr>
    <td><code>TransformPointPen</code></td>
    <td><code>fontPens.transformPointPen.TransformPointPen</code></td>
  </tr>
  <tr>
    <td><code>GuessSmoothPointPen</code></td>
    <td><code>fontPens.guessSmoothPointPen.GuessSmoothPointPen</code></td>
  </tr>
  <tr>
    <th colspan='2'><code>robofab.pens.angledMarginPen</code></th>
  </tr>
  <tr>
    <td><code>AngledMarginPen</code></td>
    <td><code>fontPens.angledMarginPen.AngledMarginPen</code></td>
  </tr>
  <tr>
    <td><code>getAngledMargins</code></td>
    <td><code>fontPens.angledMarginPen.getAngledMargins</code></td>
  </tr>
  <tr>
    <td><code>setAngledLeftMargin</code></td>
    <td><code>fontPens.angledMarginPen.setAngledLeftMargin</code></td>
  </tr>
  <tr>
    <td><code>setAngledRightMargin</code></td>
    <td><code>fontPens.angledMarginPen.setAngledRightMargin</code></td>
  </tr>
  <tr>
    <td><code>centerAngledMargins</code></td>
    <td><code>fontPens.angledMarginPen.centerAngledMargins</code></td>
  </tr>
  <tr>
    <td><code>guessItalicOffset</code></td>
    <td><code>fontPens.angledMarginPen.guessItalicOffset</code></td>
  </tr>
  <tr>
    <th colspan='2'><code>robofab.pens.boundsPen</code></th>
  </tr>
  <tr>
    <td><code>ControlBoundsPen</code></td>
    <td><code>fontTools.pens.boundsPen.ControlBoundsPen</code></td>
  </tr>
  <tr>
    <td><code>BoundsPen</code></td>
    <td><code>fontTools.pens.boundsPen.BoundsPen</code></td>
  </tr>
  <tr>
    <th colspan='2'><code>robofab.pens.digestPen</code></th>
  </tr>
  <tr>
    <td><code>DigestPointPen</code></td>
    <td><code>fontPens.digestPointPen.DigestPointPen</code></td>
  </tr>
  <tr>
    <td><code>DigestPointStructurePen</code></td>
    <td><code>fontPens.digestPointPen.DigestPointStructurePen</code></td>
  </tr>
  <tr>
    <th colspan='2'><code>robofab.pens.marginPen</code></th>
  </tr>
  <tr>
    <td><code>MarginPen</code></td>
    <td><code>fontPens.marginPen.MarginPen</code></td>
  </tr>
  <tr>
    <th colspan='2'><code>robofab.pens.pointPen</code></th>
  </tr>
  <tr>
    <td><code>AbstractPointPen</code></td>
    <td><code>ufoLib.pointPen.AbstractPointPen</code></td>
  </tr>
  <tr>
    <td><code>BasePointToSegmentPen</code></td>
    <td><code>ufoLib.pointPen.BasePointToSegmentPen</code></td>
  </tr>
  <tr>
    <td><code>PrintingPointPen</code></td>
    <td><code>fontPens.printPointPen.PrintPointPen</code></td>
  </tr>
  <tr>
    <td><code>SegmentPrintingPointPen</code></td>
    <td><code>fontPens.printPointPen.PrintPointPen</code></td>
  </tr>
  <tr>
    <td><code>PrintingSegmentPen</code></td>
    <td><code>fontPens.printPen.PrintPen</code></td>
  </tr>
  <tr>
    <td><code>SegmentPrintingPointPen</code></td>
    <td><code>fontPens.printPen.PrintPen</code></td>
  </tr>
  <tr>
    <th colspan='2'><code>robofab.pens.printingPens</code></th>
  </tr>
  <tr>
    <td><code>PrintingPointPen</code></td>
    <td><code>fontPens.printPointPen.PrintPointPen</code></td>
  </tr>
  <tr>
    <td><code>PrintingSegmentPen</code></td>
    <td><code>fontPens.printPen.PrintPen</code></td>
  </tr>
  <tr>
    <td><code>SegmentPrintingPointPen</code></td>
    <td><code>fontPens.printPointPen.PrintPointPen</code></td>
  </tr>
  <tr>
    <th colspan='2'><code>robofab.pens.reverseContourPointPen</code></th>
  </tr>
  <tr>
    <td><code>ReverseContourPointPen</code></td>
    <td><code>ufoLib.pointPen.ReverseContourPointPen</code></td>
  </tr>
  <tr>
    <th colspan='2'><code>robofab.pens.filterPen</code></th>
  </tr>
  <tr>
    <td><code>distance</code></td>
    <td><code>fontPens.penTools.distance</code></td>
  </tr>
  <tr>
    <td><code>ThresholdPen</code></td>
    <td><code>fontPens.thresholdPen.ThresholdPen</code></td>
  </tr>
  <tr>
    <td><code>thresholdGlyph</code></td>
    <td><code>fontPens.thresholdPen.thresholdGlyph</code></td>
  </tr>
  <tr>
    <td><code>_estimateCubicCurveLength</code></td>
    <td><code>fontPens.penTools.estimateCubicCurveLength</code></td>
  </tr>
  <tr>
    <td><code>_mid</code></td>
    <td><code>fontPens.penTools.middlePoint</code></td>
  </tr>
  <tr>
    <td><code>_getCubicPoint</code></td>
    <td><code>fontPens.penTools.getCubicPoint</code></td>
  </tr>
  <tr>
    <td><code>FlattenPen</code></td>
    <td><code>fontPens.flattenPen.FlattenPen</code></td>
  </tr>
  <tr>
    <td><code>flattenGlyph</code></td>
    <td><code>fontPens.flattenPen.flattenGlyph</code></td>
  </tr>
</table>

> - [Moving pens out of Robofab](http://github.com/robotools/robofab/issues/52)
{: .seealso }

[fontPens]: http://github.com/robotools/fontPens
[ufoLib]: http://github.com/fonttools/fonttools/tree/master/Lib/fontTools/ufoLib
[fontTools]: http://github.com/fonttools/fonttools


Object selection API
--------------------

FontParts introduces a new object selection API, which offers fine-grained control over selections in different types of objects.

### Object Selection Flag

The `selected` attribute (a boolean) is supported by these objects:

- {% internallink "reference/api/fontParts/rfont" %}
- {% internallink "reference/api/fontParts/rlayer" %}
- {% internallink "reference/api/fontParts/rglyph" %}
- {% internallink "reference/api/fontParts/rcontour" %}
- {% internallink "reference/api/fontParts/rsegment" %}
- {% internallink "reference/api/fontParts/rbpoint" %}
- {% internallink "reference/api/fontParts/rpoint" %}
- {% internallink "reference/api/fontParts/rcomponent" %}
- {% internallink "reference/api/fontParts/ranchor" %}
- {% internallink "reference/api/fontParts/rimage" %}
- {% internallink "reference/api/fontParts/rguideline" %}

### Sub-Object Selected Objects

Objects with sub-objects can now also return a list of selected sub-objects:

<table>
  <tr>
    <th colspan='2'>Font</th>
  </tr>
  <tr>
    <td><code>font.selectedLayers</code></td>
    <td>list of <code>RLayer</code> objects</td>
  </tr>
  <tr>
    <td><code>font.selectedLayerNames</code></td>
    <td>list of layer names (strings)</td>
  </tr>
  <tr>
    <td><code>font.selectedGlyphs</code></td>
    <td>list of <code>RGlyph</code> objects</td>
  </tr>
  <tr>
    <td><code>font.selectedGlyphNames</code></td>
    <td>list of glyph names (strings)</td>
  </tr>
  <tr>
    <td><code>font.selectedGuidelines</code></td>
    <td>list of <code>RGuideline</code> objects</td>
  </tr>
  <tr>
    <th colspan='2'>Layer</th>
  </tr>
  <tr>
    <td><code>layer.selectedGlyphs</code></td>
    <td>list of <code>RGlyph</code> objects</td>
  </tr>
  <tr>
    <td><code>layer.selectedGlyphNames</code></td>
    <td>list of glyph names (strings)</td>
  </tr>
  <tr>
    <th colspan='2'>Glyph</th>
  </tr>
  <tr>
    <td><code>glyph.selectedContours</code></td>
    <td>list of <code>RContour</code> objects</td>
  </tr>
  <tr>
    <td><code>glyph.selectedComponents</code></td>
    <td>list of <code>RComponent</code> objects</td>
  </tr>
  <tr>
    <td><code>glyph.selectedAnchors</code></td>
    <td>list of <code>RAnchor</code> objects</td>
  </tr>
  <tr>
    <td><code>glyph.selectedGuidelines</code></td>
    <td>list of <code>RGuideline</code> objects</td>
  </tr>
  <tr>
    <th colspan='2'>Contour</th>
  </tr>
  <tr>
    <td><code>contour.selectedSegments</code></td>
    <td>list of <code>RSegment</code> objects</td>
  </tr>
  <tr>
    <td><code>contour.selectedPoints</code></td>
    <td>list of <code>RPoint</code> objects</td>
  </tr>
  <tr>
    <td><code>contour.selectedBPoints</code></td>
    <td>list of <code>bPoint</code> objects</td>
  </tr>
  <tr>
    <th colspan='2'>Segment</th>
  </tr>
  <tr>
    <td><code>segment.selectedPoints</code></td>
    <td>list of <code>RPoint</code> objects</td>
  </tr>
</table>

> - [Define object selection API](http://github.com/robotools/fontParts/issues/37)
{: .seealso }


Interface
---------

RoboFab dialogs are also not included in FontParts. Use RoboFont’s `mojo.UI` module instead.

<table>
  <tr>
    <th colspan='2'><code>interface.all.dialogs</code></th>
  </tr>
  <tr>
    <td><code>Message</code></td>
    <td><code>mojo.UI.Message</code></td>
  </tr>
  <tr>
    <td><code>AskString</code></td>
    <td><code>mojo.UI.AskString</code></td>
  </tr>
  <tr>
    <td><code>AskYesNoCancel</code></td>
    <td><code>mojo.UI.AskYesNoCancel</code></td>
  </tr>
  <tr>
    <td><code>GetFile</code></td>
    <td><code>mojo.UI.GetFile</code></td>
  </tr>
  <tr>
    <td><code>GetFolder</code></td>
    <td><code>mojo.UI.GetFolder</code></td>
  </tr>
  <tr>
    <td><code>PutFile</code></td>
    <td><code>mojo.UI.PutFile</code></td>
  </tr>
  <tr>
    <td><code>SelectFont</code></td>
    <td><code>mojo.UI.SelectFont</code></td>
  </tr>
  <tr>
    <td><code>SelectGlyph</code></td>
    <td><code>mojo.UI.SelectGlyph</code></td>
  </tr>
  <tr>
    <td><code>FindGlyph</code></td>
    <td><code>mojo.UI.FindGlyph</code></td>
  </tr>
</table>


Miscellaneous tools
-------------------

Tools from the `robofab.misc` module can now be found in `fontTools.misc`.

<table>
  <tr>
    <th colspan='2'><code>robofab.misc.arrayTools</code></th>
  </tr>
  <tr>
    <td><code>calcBounds</code></td>
    <td><code>fontTools.misc.arrayTools.calcBounds</code></td>
  </tr>
  <tr>
    <td><code>updateBounds</code></td>
    <td><code>fontTools.misc.arrayTools.updateBounds</code></td>
  </tr>
  <tr>
    <td><code>pointInRect</code></td>
    <td><code>fontTools.misc.arrayTools.pointInRect</code></td>
  </tr>
  <tr>
    <td><code>pointsInRect</code></td>
    <td><code>fontTools.misc.arrayTools.pointsInRect</code></td>
  </tr>
  <tr>
    <td><code>vectorLength</code></td>
    <td><code>fontTools.misc.arrayTools.vectorLength</code></td>
  </tr>
  <tr>
    <td><code>asInt16</code></td>
    <td><code>fontTools.misc.arrayTools.asInt16</code></td>
  </tr>
  <tr>
    <td><code>normRect</code></td>
    <td><code>fontTools.misc.arrayTools.normRect</code></td>
  </tr>
  <tr>
    <td><code>scaleRect</code></td>
    <td><code>fontTools.misc.arrayTools.scaleRect</code></td>
  </tr>
  <tr>
    <td><code>offsetRect</code></td>
    <td><code>fontTools.misc.arrayTools.offsetRect</code></td>
  </tr>
  <tr>
    <td><code>insetRect</code></td>
    <td><code>fontTools.misc.arrayTools.insetRect</code></td>
  </tr>
  <tr>
    <td><code>sectRect</code></td>
    <td><code>fontTools.misc.arrayTools.sectRect</code></td>
  </tr>
  <tr>
    <td><code>unionRect</code></td>
    <td><code>fontTools.misc.arrayTools.unionRect</code></td>
  </tr>
  <tr>
    <td><code>rectCenter</code></td>
    <td><code>fontTools.misc.arrayTools.rectCenter</code></td>
  </tr>
  <tr>
    <td><code>intRect</code></td>
    <td><code>fontTools.misc.arrayTools.intRect</code></td>
  </tr>
  <tr>
    <th colspan='2'><code>robofab.misc.bezierTools</code></th>
  </tr>
  <tr>
    <td><code>calcQuadraticBounds</code></td>
    <td><code>fontTools.misc.bezierTools.calcQuadraticBounds</code></td>
  </tr>
  <tr>
    <td><code>calcCubicBounds</code></td>
    <td><code>fontTools.misc.bezierTools.calcCubicBounds</code></td>
  </tr>
  <tr>
    <td><code>splitLine</code></td>
    <td><code>fontTools.misc.bezierTools.splitLine</code></td>
  </tr>
  <tr>
    <td><code>splitQuadratic</code></td>
    <td><code>fontTools.misc.bezierTools.splitQuadratic</code></td>
  </tr>
  <tr>
    <td><code>splitCubic</code></td>
    <td><code>fontTools.misc.bezierTools.splitCubic</code></td>
  </tr>
  <tr>
    <td><code>splitQuadraticAtT</code></td>
    <td><code>fontTools.misc.bezierTools.splitQuadraticAtT</code></td>
  </tr>
  <tr>
    <td><code>splitCubicAtT</code></td>
    <td><code>fontTools.misc.bezierTools.splitCubicAtT</code></td>
  </tr>
  <tr>
    <td><code>solveQuadratic</code></td>
    <td><code>fontTools.misc.bezierTools.solveQuadratic</code></td>
  </tr>
  <tr>
    <td><code>solveCubic</code></td>
    <td><code>fontTools.misc.bezierTools.solveCubic</code></td>
  </tr>
</table>


glifLib
-------

Tools from the `robofab.glifLib` module (used for reading and writing `.glif` files) now live in `ufoLib.glifLib`.

> The `ufoLib` is now part of FontTools. From RoboFont 3.2 onwards, all tools from the `ufoLib.glifLib` module can also be found in `fontTools.ufoLib.glifLib`.
>
> The `ufoLib` module will be deprecated.
{: .note }

<table>
  <tr>
    <th colspan='2'><code>robofab.glifLib</code></th>
  </tr>
  <tr>
    <td><code>GlyphSet</code></td>
    <td><code>ufoLib.glifLib.GlyphSet</code></td>
  </tr>
  <tr>
    <td><code>readGlyphFromString</code></td>
    <td><code>ufoLib.glifLib.readGlyphFromString</code></td>
  </tr>
  <tr>
    <td><code>writeGlyphToString</code></td>
    <td><code>ufoLib.glifLib.writeGlyphToString</code></td>
  </tr>
  <tr>
    <td><code>glyphNameToFileName</code></td>
    <td><code>ufoLib.glifLib.glyphNameToFileName</code></td>
  </tr>
</table>


- - -

> - [oneToThreeRoboFontExtension](http://github.com/typemytype/oneToThreeRoboFontExtension)
> - {% internallink "topics/recommendations-upgrading-RF3" %}
{: .seealso }
