---
layout: page
title: Welcome to RoboFont!
treeTitle: Welcome
---

RoboFont is a UFO-based font editor for macOS.

RoboFont provides a simple to use environment for creating typefaces.

Some of the main features of RoboFont are:

* written from scratch in Python with scalability in mind
* full scripting access to objects and the application interface
* it can be used as a platform to build additional tools
* does not perform any 'auto-magic'
* does not overload the user with a multitude of options
* uses the UFO format as its native file format

The operating philosophy behind RoboFont is:  

<div style="font-size:1.6em;"><em>The tools you choose influence your creative process.</em></div>

Because of this, RoboFont provides many opportunities for the user to tailor the application to their design process. It is strict about not performing 'auto-magic' on one’s font files, ‘auto-’ anything is avoided if possible. This means that it does not do and does not have some features of other font editing applications. Because the application is {% internallink 'topics/extensions' text='extensible' %}, if a user finds a need for a feature that isn’t part of RoboFont, it can be added. This gives users control to design the application according to their own workflow.

However, the core of RoboFont is a {% internallink 'topics/robofont-features-overview' text='fully-featured drawing editor' %}, containing all the basic tools a typeface designer needs for drawing and modifying a design.
