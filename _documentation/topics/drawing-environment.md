---
layout: page
title: The drawing environment
slideShows:
  extensions:
    - image: extraDocs/carousel/extensions/Ramsay-St.png
      caption: |
        [Ramsay St.](http://github.com/typemytype/ramsayStreetRoboFontExtension) by Frederik Berlaen
    - image: extraDocs/carousel/extensions/GlyphConstruction.png
      caption: |
        [GlyphConstruction](http://github.com/typemytype/glyphconstruction) by Frederik Berlaen
    - image: extraDocs/carousel/extensions/OverlayUFOs.png
      caption: |
        [Overlay UFOs](http://github.com/FontBureau/fbOpenTools) by David Jonathan Ross
    - image: extraDocs/carousel/extensions/Outliner.png
      caption: |
        [Outliner](http://github.com/typemytype/outlinerRoboFontExtension) by Frederik Berlaen
    - image: extraDocs/carousel/extensions/Prepolator.png
      caption: |
        [Prepolator](http://extensionstore.robofont.com/extensions/prepolator/) by Tal Leming
    - image: extraDocs/carousel/extensions/word-o-mat.png
      caption: |
        [word-o-mat](http://github.com/ninastoessinger/word-o-mat) by Nina Stössinger
    - image: extraDocs/carousel/extensions/CornerTools.png
      caption: |
        [CornerTools](http://github.com/roboDocs/CornerTools) by Loïc Sander
    - image: extraDocs/carousel/extensions/MetricsMachine.png
      caption: |
        [MetricsMachine](http://extensionstore.robofont.com/extensions/metricsMachine/) by Tal Leming
    - image: extraDocs/carousel/extensions/GlyphBrowser.png
      caption: |
        [GlyphBrowser](http://github.com/LettError/glyphBrowser) by Erik van Blokland
    - image: extraDocs/carousel/extensions/SpeedPunk.png
      caption: |
        [SpeedPunk](http://github.com/yanone/speedpunk) by Yanone
    - image: extraDocs/carousel/extensions/GlyphNanny.png
      caption: |
        [Glyph Nanny](http://github.com/typesupply/glyph-nanny) by Tal Leming
  autoPlay: true
  height: 550
tags:
  - contours
  - designing
---

* Table of Contents
{:toc}

*This page gives a quick overview into RoboFont’s drawing environment.*

Drawing is done in the {% internallink 'workspace/glyph-editor' %}, which can be opened by double clicking on any glyph cell in the {% internallink 'workspace/font-overview' %}.

{% image tutorials/glyph-editor.png %}


Navigation
----------

While working in the Glyph Editor, you can easily switch to other glyphs and layers using keyboard shorcuts.

{% video topics/navigation.mp4 %}

Previous / next glyph
: Use the shortkeys `Cmd )` and `Cmd (` to go to the next / previous glyphs.

Jump to glyph
: Press `J` to open the *Jump to glyph* pop-up window to go to any other glyph in the font. Start typing the glyph name, and the dialog will show a list of possible glyphs to choose from.

Jump to layer
: Use the shortkeys `Cmd Alt ↓` / `Cmd Alt ↑` to switch to the previous / next layer in the current glyph.

Layers popup
: Press `L` to open the *Layers* popup window, which can be used to switch to another layer, move the current glyph to another layer, or to flip the contents of the current layer with another layer.


Display options
---------------

*The Glyph Editor offers unlimited options to fine-tune its appearance and visualization tools.*

Glyph preview
: ^
  {% video reference/workspace/preview.mp4 %}

  Use the `backspace` key to toggle the preview mode on/off while designing a glyph, so you can quickly visualize and evaluate the glyph shape.

Display options menu
: ^
  {% image reference/workspace/display-layers.gif %}

  Use the menu at the bottom left to customize the way glyph data is displayed, turning different kinds of data on/off as needed.

Layer display options
: ^
  {% image /reference/workspace/glyph-editor/inspector_layers.png %}

  Use the {% internallink "workspace/inspector#layer-display-options" text='Layers Inspector' %} to adjust the display options for each layer


<table>
  <tr>
    <th width="10%">icon</th>
    <th width="90%">layer attributes</th>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_visibility.png" /></td>
    <td>visibility</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_fill.png" /></td>
    <td>fill</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_stroke.png" /></td>
    <td>stroke</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_points.png" /></td>
    <td>points</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_positions.png" /></td>
    <td>point coordinates</td>
  </tr>
</table>


Customize colors
: ^
  {% image /reference/workspace/preferences-window/preferences_glyph-view-appearance.png %}
  
  The glyph view’s display colors can be configured in the {% internallink 'workspace/preferences-window/glyph-view' %}. Use the [ThemeManager] extension to apply existing themes and create your own.

More display features
: ^
  {% include slideshows items=page.slideShows.extensions %}
  
  Use extensions to add special visualization features, for example:

  - show reference glyphs with [Ramsay St.] and [OverlayUFOs]
  - visualize curvature with [Speed Punk]
  - preview accented glyphs with [Anchor Overlay Tool]
  - view bezier curve construction with [De Casteljau] and [Check Parallel Tool]
  
  …and much more!


[Ramsay St.]: https://github.com/typemytype/ramsayStreetRoboFontExtension
[OverlayUFOs]: https://github.com/FontBureau/fbOpenTools
[Speed Punk]: https://github.com/yanone/speedpunk
[Anchor Overlay Tool]: https://github.com/jenskutilek/AnchorOverlayTool
[De Casteljau]: http://github.com/luke-snider/de-casteljau
[Check Parallel Tool]: https://github.com/jtanadi/CheckParallelTool
[ThemeManager]: https://github.com/connordavenport/Theme-Manager

> - {% internallink 'tutorials/drawing-glyphs' %}
{: .seealso }
