---
layout: page
title: The history of RoboFont
treeTitle: History
draft: true
---

* Table of Contents
{:toc}

> - {% internallink "topics/robofont-pre-history" %}
{: .seealso }


The Missing UFO Font Editor
---------------------------

Around 2010, the experience of building {% internallink "topics/robofont-pre-history#standalone-apps" text='standalone apps' %} had shown that it was possible to create UFO-based macOS apps in Python, and most of the pieces needed for that were already available as {% internallink "topics/robofont-pre-history#open-source-libraries" text='open-source libraries' %}.

In a UFO-based workflow with several specialized apps, the dependency of FontLab as a central drawing environment had created a bottleneck – it required back and forth conversion between UFO and `.vfb`, which was cumbersome and slow. Customization options in FontLab were also very limited – only a subset of its API was available in Python, and not in a object-oriented way which allowed subclassing and reuse.

### RoboHint

With the advent of webfonts in 2009 and the greater demand for screen fonts, a new interest in TrueType hinting was sparked. Petr van Blokland was one of the partners in WebType, one of the newly established webfont providers. He was interested in developing a TrueType hinting tool for macOS that could be integrated into a UFO-based workflow. But because of the low-level nature of TrueType hinting, building such a tool would require access to a font editor’s internals, and that was not possible with FontLab at the time.

Without time to build such an editor himself, Petr approached Frederik Berlaen – who had been his student at {% glossary TypeMedia %}, and whom he had seen build Kalliculator within a year – and asked if he could build a simple, scriptable font editor which could serve as the basis for his hinting tool. Frederik accepted the challenge, and that font editor became RoboFont.


RoboFont 1.0
------------

RoboFont marks a new approach to creating a scriptable font editor: instead of adding Python to an existing font editor, a new font editor was built entirely in Python.

### Standing on the shoulders of giants

Many of the necessary building blocks were already available as open-source libraries. The backbone of the application was built using with `defcon`, which makes the font objects accessible to the interface and takes care of sending notifications to all registered {% internallink "topics/the-observer-pattern" text='observers' %}. The interface was built using `vanilla` and `defconAppKit`, which provides several font-specific UI components such as the glyph cell view in the {% internallink 'reference/workspace/font-overview' %}, the {% internallink 'reference/workspace/font-overview/font-info-sheet' %}, parts of the {% internallink 'reference/workspace/space-center' %}, etc.

- font generation using the AFDKO

### Building the missing pieces

A lot of new code had to be written.

- The {% internallink 'reference/workspace/glyph-editor' %} and all its interactive tools were created from scratch around Apple’s native `NSEvent` API.

- The {% internallink 'reference/workspace/preferences-window' %} and a system to store user settings had to be created.

- most of the {% internallink 'reference/workspace/scripting-window' %}

- auto-update, license validation, ...

Internally, the new code was split into the `lib` and `mojo` modules – the first containing private tools, the second intended as a public API for use by developers.

### Scripting API

RoboFont font objects were created by wrapping defcon objects. The API was copied from RoboFab, so it would be easy to port existing scripts to RoboFont. Several RoboFont-specific methods and attributes were also added. Following RoboFab convention, the ‘native’ defcon objects were available with `obj.naked()`. 

### booleanOperations

A new code library, [booleanOperations], was developed around the open-source [PyClipper] in order to provide boolean operations with contours, which are essential to any vector editor. A new API for boolean glyph math was added to the glyph objects.

[booleanOperations]: #
[PyClipper]: #

### Extensions platform

RoboFont was designed from the ground up to be scriptable and extensible. Since the beginning, the idea was to create a very basic core app which could be extended with ‘plugins’ written in Python. RoboHint was the first example of such a plugin, but many more followed. 

An extension package format was created to make it easy to install and share extensions. Plugins are loaded on startup and can add themselves to the RoboFont UI. Because most RoboFont APIs are scriptable with Python, the space of possibilities is huge.

### Website and documentation

The original RoboFont documentation was written by Frederik, with some assistance from Ben Kiel and Gustavo Ferreira. It consisted of an introduction into the ideas behind RoboFont, a description of the interface, and a reference of the API. A RoboFont user forum was also created for support and questions.

### Release

{% image topics/RoboFont-landed.png %}

After 18 months in development, RoboFont was officially released in September 2011 at the ATypI Conference in Reykjavik, Iceland. The reception was overwhelming. Some people were puzzled by the apparent “lack of features” in comparison to other font editors.


RoboFont 1.*
------------

- gradual updates until version 1.8
- lots of open-source extensions

### Mechanic

Mechanic – the RoboFont extension to manage RoboFont extensions – was conceived and developed by Jack Jennings. [Released in 2015][Mechanic Blog], it quickly became an essential tool for many RoboFont users. It established a bridge between extension developers and designers, and stimulated the creation of more and better extensions. Today Mechanic hosts nearly 100 open-source extensions by 30 different authors.

[Mechanic Blog]: http://web.archive.org/web/20170915150601/http://blog.robofontmechanic.com:80/

RoboFont 2, the app that never was
----------------------------------

RoboFont 2 was an intermediate step between RoboFont 1 and RoboFont 3 which never saw the light of day (the beta version is a rare collector’s item).

| application | language | format | scripting API |
|-------------|----------|--------|---------------|
| RoboFont 1  | Python 2 | UFO 2  | RoboFab       |
| RoboFont 2  | Python 3 | UFO 2  | RoboFab       |
| RoboFont 3  | Python 3 | UFO 3  | FontParts     |

### Python 3

With the end of life date for Python 2.X coming near, it was necessary to undertake the upgrade to Python 3. The experience upgrading the simpler DrawBot to py3 served as the final encouragement. This required upgrading all underlying libraries (vanilla, defcon, fontTools, etc) to py3 first. 

### UFO 3

The upgrade to py3 was shortly followed by an upgrade of the source format from UFO2 to UFO3 (the UFO3 spec was published in 2012).

Having both Python and UFO in version 3, it seemed fitting to bump the RoboFont version to 3 too.


RoboFont 3
----------

- Python 3
- UFO3
- FontParts API

### New documentation and website

Gustavo Ferreira joins the team as documentation editor and general assistant. New website and documentation, new Forum.

### Mechanic 2

Mechanic stopped working in Python 3 and required a complete rewrite.

Mechanic 2 introduced a different architecture with extension metadata stored in centrally in the Mechanic 2 server. A much more flexible system which allowed multiple streams of JSON data.

### Commercial extensions

- new Extension Store
- Prepolator and MetricsMachine converted into RF extensions.


RF 3.X updates
--------------

- steady stream of updates: 3.1, 3.2, 3.3, 3.4
- several new extensions
- major update to Mechanic 2 by Antonio Cavedoni
- Skateboard extension to replace/expand Superpolator
- support for dark mode and more options for themes
- lots of activity in the forum
- public betas


RF 3.5b
------

The only RoboFont 3 that works on Big Sur.

RoboFont 4
----------

A complete rewrite of all the drawing to screen happening in the glyph view.

### Documentation rework

Rework of the documentation: reorganisation of content sections, more introductory material and tutorials.


