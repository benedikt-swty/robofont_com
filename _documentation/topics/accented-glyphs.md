---
layout: page
title: Accented glyphs
tags:
  - accents
  - anchors
  - components
slideShows:
  accentedWorkflow:
    - image: topics/accentedGlyphs_Aacute.png
      caption: Accented Glyph (Aacute)
    - image: topics/accentedGlyphs_acute.png
      caption: Spacing accent (acute.case)
    - image: topics/accentedGlyphs_A.png
      caption: Base Glyph (A)
  autoPlay: true
  height: 550
---

* Table of Contents
{:toc}

*This page provides background information about {% internallink "tutorials/building-accented-glyphs" %}.*


Spacing accents vs. combining accents
-------------------------------------

OpenType fonts may contain two types of accent glyphs (marks): *spacing* and *combining*.

<table>
  <tr>
    <td width='12%'></td>
    <th width='38%'>spacing accents</th>
    <th width='40%'>combining accents</th>
  </tr>
  <tr>
    <th>width</th>
    <td>positive width</td>
    <td>zero width</td>
  </tr>
  <tr>
    <th>usage</th>
    <td>by itself between other glyphs</td>
    <td>composing characters dynamically</td>
  </tr>
  <tr>
    <th>range</th>
    <td>Latin-1</td>
    <td>Combining Diacritical Marks</td>
  </tr>
</table>

**Spacing accents** have a positive width and are surrounded by whitespace, so they can be set between other characters in a string of text. Their codepoints can be found in the *Latin-1* Unicode range. For example you can find the acute spacing accent at U+00B4.

**Combining accents** have no width. They exist only for creating composed characters dynamically using OpenType features. Their codepoints can be found in the *Combining Diacritical Marks* Unicode range. For example you can find the acute combining accent at U+0301.

{% image topics/accentedGlyphs_spacing-comb.png caption="Spacing accent (left) and combining accent (right) from IBM Plex" %}


Precomposed vs. decomposed characters
-------------------------------------

OpenType fonts may support two types of accented glyphs: *precomposed* and *decomposed*.

<table>
  <tr>
    <td width='12%'></td>
    <th width='38%'>precomposed glyphs</th>
    <th width='40%'>decomposed glyphs</th>
  </tr>
  <tr>
    <th>data</th>
    <td>exists as glyphs in the font</td>
    <td>exists as feature code and anchors</td>
  </tr>
  <tr>
    <th>input</th>
    <td>spacing accent + base character</td>
    <td>base character + combining accent</td>
  </tr>
  <tr>
    <th>support</th>
    <td>works everywhere</td>
    <td>needs support for OpenType features</td>
  </tr>
</table>

**Precomposed accented glyphs** exist as actual glyphs in the font. They are usually built using components. This is the older approach to accented glyphs used in PostScript and TrueType fonts, and works in all environments. Input of precomposed accented characters follows the sequence `<spacing accent> + <base character>`.

**Decomposed accented glyphs** exist as OpenType feature code referencing other glyphs as components. This is the newer approach to accented glyphs introduced by the OpenType format, and is not supported in all environments. Input of decomposed accented characters follows the sequence `<base character> + <combining accent>`.

> [Generate All Mark Files](https://github.com/adobe-type-tools/python-scripts/blob/master/generateAllMarkFiles.py) script by Adobe
{: .seealso }


Alternate accent glyphs
-----------------------

Depending on the design of your typeface, it may be interesting to create alternate versions of certain accent glyphs to fine-tune their shapes to different base glyphs.

Typical examples of alternate sets of accents include:

- flatter accents for capitals
- narrower/wider accents for narrow/wide base glyphs
- language-sensitive variants
- stylistic alternates

{% image topics/accentedGlyphs-regular+alt.png caption="Regular tilde and alternative tilde from IBM Plex" %}


Positioning accents
-------------------

### The ‘old’ anchor-based workflow

Older accent building workflows use anchors to align accents to base glyphs and between each other, and rely on a naming convention using `_` (underscore).

Example: in order to build the accented glyph `aacute` and align accent glyph to base glyph using anchors, the base glyph `a` needs an anchor named `top`, an the accent glyph `acute` needs an anchor named `_top`.

{% include slideshows items=page.slideShows.accentedWorkflow %}

### New positioning options in Glyph Construction

Glyph Construction introduces several other methods for positioning components, using guidelines, distances, percentages, named reference points, transformation matrixes, vertical metrics, etc.

> - [Glyph Construction > positioning methods](http://github.com/typemytype/GlyphConstruction#positioning)
{: .seealso }
