---
layout: page
title: Interactive tools
---

* Table of Contents
{:toc}


Core tools
----------

RoboFont’s {% internallink 'workspace/glyph-editor' %} comes with four {% internallink 'workspace/glyph-editor/tools' text='core tools' %} for interacting with glyphs:

- {% internallink 'glyph-editor/tools/editing-tool' %}
- {% internallink 'glyph-editor/tools/bezier-tool' %}
- {% internallink 'glyph-editor/tools/slice-tool' %}
- {% internallink 'glyph-editor/tools/measurement-tool' %}

These interactive tools are built around macOS [NSEvent] objects, which represent input actions such as mouse clicks, mouse position, pressed keys, etc.

Custom tools
------------

Custom interactive tools can be created by subclassing the `BaseEventTool` or one of the other core tools which are available from the {% internallink 'reference/api/mojo/mojo-events' text='`mojo.events`' %} module:

```python
# subclass base tool
from mojo.events import BaseEventTool
# or one of the core tools
from mojo.events import EditingTool, BezierDrawingTool, SliceTool, MeasurementTool
```

[NSEvent]: http://developer.apple.com/documentation/appkit/nsevent

### Examples

Several custom interactive tools are available as open-source extensions:

- [PixelTool](http://github.com/typemytype/pixelToolRoboFontExtension)
- [ShapeTool](http://github.com/typemytype/shapeToolRoboFontExtension)
- [RoboLasso](http://github.com/productiontype/RoboLasso)
- [ScaleEditTool](http://github.com/klaavo/scalingEditTool)
- [BlueZoneEditor](http://github.com/andyclymer/BlueZoneEditor-roboFontExt)
- [BoundingTool](http://github.com/FontBureau/fbOpenTools/tree/master/BoundingTool)
- [CheckParallelTool](http://github.com/jtanadi/CheckParallelTool)
- [CornerTool](https://github.com/roboDocs/CornerTools)
