---
layout: page
title: Making a complex font family
draft: true
draft-hidden: true
tags:
  - mastering
---

A ‘complex’ type family in this context is any family which deviates from the *RIBBI model* (Regular / Italic / Bold / Bold Italic).

This could be, for example, a family with:

- more weights than bold-italic
- styles different from regular/italic

> - add illustration?
{: .todo}

Such families require setting additional bits of font metadata to work properly:

- OS/2 weight
- OS/2 width
- family and style names 
- subfamily names (style flags)

{% comment %}

- PS stems
- order in font menus (correct numbers are needed)
- hinting
- interpolation
- proofing

{% endcomment %}