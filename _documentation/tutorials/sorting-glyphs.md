---
layout: page
title: Sorting glyphs
tags:
  - character set
  - glyph names
---

* Table of Contents
{:toc}

There are two ways to sort glyphs in the {% internallink 'workspace/font-overview' %}. The first one modifies the actual glyph order in the font, the second one affects only the display order in the UI.


Sorting glyphs using the Sort Glyphs sheet
------------------------------------------

The {% internallink "workspace/font-overview/sort-glyphs-sheet" %} provides different sorting methods to choose from:

{% image tutorials/sort-glyphs-sheet.png %}

When a sorting method is applied using the Sort Glyphs sheet, both the display order in the Font Overview and the actual glyph order stored in the font are modified. This order is preserved when the font is closed, and is used when generating font binaries.


Sorting glyphs with a script
----------------------------

The sorting methods available from the Sort Glyphs sheet can be applied with a script:

```python
font = CurrentFont()
newGlyphOrder = font.naked().unicodeData.sortGlyphNames(font.glyphOrder, sortDescriptors=[dict(type="unicode", ascending=True, allowPseudoUnicode=True)])
font.glyphOrder = newGlyphOrder
```

> To display template cells for glyphs which are not included in the font, use `font.templateGlyphOrder` instead of `font.glyphOrder`.
{: .note }


Sorting glyphs using Font Overview’s List mode
----------------------------------------------

The Font Overview’s {% internallink "workspace/font-overview#list-mode" text='List mode' %} offers another way of sorting glyphs, using basic glyph attributes such as width, left or right margin, number of contours, mark colors, etc.

Sorting with List mode does **not** modify the actual glyph order in the font – just the visual order in the Font Overview interface.

{% image how-tos/sorting-glyphs_list-sort.png %}

To sort the glyphs based on one of the attributes, simply click on its column header. Click again to reverse the sorting order from ascending to descending, and vice versa.

The glyph order is maintained when the display mode is switched back to Tiles mode.

> This approach can be used to quickly sort glyphs by color, for example.
{: .tip }


- - -

> - [RoboFont Forum > tags > sorting glyphs](http://forum.robofont.com/tags/sorting%20glyphs)
{: .seealso }
