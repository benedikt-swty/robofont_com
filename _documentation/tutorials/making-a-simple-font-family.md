---
layout: page
title: Making a simple font family
draft: true
draft-hidden: true
tags:
  - mastering
---

- typographical quartett: regular / italic / bold / bold-italic
- style-linking (set `styleMapStyle` value)
- set `usWeight`

| familyName | style name  | style map style | usWeight | italicAngle |
|------------|-------------|-----------------|----------|-------------|
| MyFont     | Regular     | Regular         | 400      | -           |
| MyFont     | Bold        | Bold            | 700      | -           |
| MyFont     | Italic      | Italic          | 400      | 7           |
| MyFont     | Bold Italic | Bold Italic     | 700      | 7           |
