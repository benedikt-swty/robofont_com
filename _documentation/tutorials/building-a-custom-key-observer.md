---
layout: page
title: Building a custom key observer
treeTitle: Custom key observer
tags:
  - scripting
  - subscriber
---

* Table of Contents
{:toc}

RoboFont has an interface for defining keyboard shortcuts or “Hot Keys” for menu items, the {% internallink 'workspace/preferences-window/short-keys' %}. With a bit of code, we can also create custom Hot Keys for actions that are not available in the UI.

*In this tutorial, we’ll show how to use a Subscriber object to make the {% internallink 'workspace/glyph-editor' %} jump to the `foreground` layer when the `f` key is pressed.*


Creating a basic controller object
----------------------------------

We’ll start by subclassing a Subscriber object, using a `class` definition.

```python
from mojo.subscriber import Subscriber

class JumpToForeground(Subscriber):
    pass


JumpToForeground()
```

In the above script, a new `JumpToForeground` class is defined by subclassing a `Subscriber` object. The class is empty, there is no need to `__init__` or `build` anything in this case, we need no window for this tool.


Registering the tool with `Subscriber`
--------------------------

Let’s start by registering the tool along with the glyph editor with a function from the `subscriber` module:

```python
from mojo.subscriber import Subscriber, registerGlyphEditorSubscriber

class JumpToForeground(Subscriber):
    pass


registerGlyphEditorSubscriber(JumpToForeground)
```

So, we'are not initiating the tool ourselves anymore, but we are passing its name to the `registerGlyphEditorSubscriber` function. Under the hood, `Subscriber` will take care of constructing the class when a glyph editor opened and it will assign our tool to the newly opened glyph editor. Our tool will remain alive until the glyph editor is closed. So, we won't need to stop listening to events once the tool is closed, RoboFont will take care of it for us.


Adding a callback method
---------------------------------

```python
from mojo.subscriber import Subscriber, registerGlyphEditorSubscriber

class JumpToForeground(Subscriber):

    debug = True

    def glyphEditorDidKeyDown(self, info):
        print(info)


registerGlyphEditorSubscriber(JumpToForeground)
```

The {% internallink "reference/api/mojo/mojo-subscriber" text="`Subscriber` reference" %} lists all the events that `Subscriber` can listen. There is one event particularly suited for our case, the `glyphEditorDidKeyDown` method: this method will be called each time a key is pressed while the glyph editor is active.

The Event Info Dictionary
---------------------------------

If you print the event info object in the RoboFont output window, you'll see quite a rich dictionary with several keys. We are interested in the `deviceState` dictionary, where we can find which key triggered the callback:

```plaintext
'deviceState': {
    'keyDown': 'f',
    'keyDownWithoutModifiers': 'f',
    'up': False,
    'down': False,
    'left': False,
    'right': False,
    'shiftDown': 0,
    'capLockDown': 0,
    'optionDown': 0,
    'controlDown': 0,
    'commandDown': 0,
    'locationInWindow': <NSPoint x=615.7727661132812 y=626.8971557617188>,
    'locationInView': <NSPoint x=2052.7727661132812 y=2583.8971557617188>,
    'clickCount': 0
    }
```

Now we can use this information to filter the events and jump to the foreground only when `'f'` is pressed.


Observing one key and switching layers
---------------------------------

The operations is just a matter of using a conditional statement in combination with the `SetCurrentLayerByName` method from the `mojo.UI` module.

```python
from mojo.UI import SetCurrentLayerByName
from mojo.subscriber import Subscriber, registerGlyphEditorSubscriber

class JumpToForeground(Subscriber):

    debug = True

    def glyphEditorDidKeyDown(self, info):
        if info['deviceState']['keyDownWithoutModifiers'] == "f":
            SetCurrentLayerByName("foreground")


registerGlyphEditorSubscriber(JumpToForeground)
```


Setting the script as a start-up script
---------------------------------------

And that’s it! Save this script in a place you can keep track of, and from the {% internallink 'workspace/preferences-window/extensions' %}, you can add this as a *Start Up Script* so that it’s always run one time when RoboFont launches.

> - {% internallink 'tutorials/setting-up-a-startup-script' %}
{: .seealso }

- - -

Contributed by [Andy Clymer](http://andyclymer.com/), updated by Roberto Arista for RoboFont 4
