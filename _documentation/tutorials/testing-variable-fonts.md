---
layout: page
title: Testing variable fonts
tags:
  - variable fonts
  - interpolation
  - designspace
---

* Table of Contents
{:toc}


Testing with DrawBot
--------------------

Variable fonts are supported natively in macOS 10.3 and higher.

DrawBot supports variable fonts too. The following commands are available:

[`fontVariations(wdth=0.6, wght=0.1, ...)`](http://www.drawbot.com/content/text/textProperties.html#drawBot.fontVariations)
: selects a location in the current variable font using parameters

[`listFontVariations(fontName=None)`](http://www.drawbot.com/content/text/textProperties.html#drawBot.listFontVariations)
: returns a dictionary with variations data for the current font

The MutatorSans repository includes two DrawBot scripts for testing and previewing variable fonts:

{% image how-tos/creating-variable-fonts_testing-DrawBot.png %}

[previewMutatorSans.py](http://github.com/LettError/mutatorSans/blob/master/drawbot/previewMutatorSans.py)
: preview the variable font interactively using sliders

[animateMutatorSans.py](http://github.com/LettError/mutatorSans/blob/master/drawbot/animateMutatorSans.py)
: animate font variations along the width and weight axes

> - [Tutorial: How to animate a variable font](http://forum.drawbot.com/topic/50/tutorial-request-how-to-animate-a-variable-font)
{: .seealso }


Testing in the browser
----------------------

Variable fonts are supported in all major browsers.

Use the CSS property `font-variation-settings` to select an instance in the font’s design space using parameters:

```css
.example { font-variation-settings: "wght" 500, "wdth" 327; }
```

The MutatorSans repository includes a simple [HTML test page] which shows how to:

- animate font variation parameters with CSS
- control parameters interactively using sliders

{% image how-tos/creating-variable-fonts_testing-html.png %}


There are also a few websites that you can use to test your variable fonts:
+ [Font Gauntlet](https://fontgauntlet.com)
+ [Wakamai Fondue](https://wakamaifondue.com)

[HTML test page]: http://github.com/LettError/mutatorSans/blob/master/test.html

Testing with FontGoggles
------------------------

[FontGoggles](https://fontgoggles.org) supports variable fonts. Just drag and drop your .ttf file on the application window and switch to the variations tab.

{% image /tutorials/fontGoogles-testingVariableFonts.png %}


