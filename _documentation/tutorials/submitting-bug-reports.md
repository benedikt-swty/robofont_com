---
layout: page
title: Submitting bug reports
tags:
  - getting help
  - bugs
---

* Table of Contents
{:toc}

A bug is an error in a computer program that causes it to produce an incorrect or unexpected result, or to behave in unintended ways. The process of fixing bugs is called *debugging*.

It’s possible that, while working with RoboFont, you come across a bug. If this happens, don’t despair! [Software has bugs. This is normal.](http://m.signalvnoise.com/software-has-bugs-this-is-normal-f64761a262ca)

**The sooner a bug is reported, the sooner it can be fixed.**

This page contains information to help you correctly identify and report bugs.


Many places for bugs to hide
----------------------------

RoboFont is built from several modules and components. The process of getting a bug fixed depends on where in RoboFont it is found:

- in RoboFont’s own source code
- in one of the {% internallink "reference/embedded-libraries" text="code libraries embedded in RoboFont" %}
- in one of the installed {% internallink "topics/extensions" text="extensions" %}
- in your own self-made scripts and tools
- in one of the additional libraries installed in your machine

> It’s also possible that the bug is not a *software bug* but a *data bug*: the information stored in the UFO can be corrupted. If that happens, you can edit the contents of the UFO file manually with a text editor to repair it.
{: .note }

> Use the {% internallink 'workspace/safe-mode' %} to check if a bug is in RoboFont itself or in one of the installed extensions.
{: .tip }

### Bugs in RoboFont or in embedded libraries

Bugs in RoboFont must fixed in the RoboFont source code; bugs in one of the embedded libraries must be fixed in that library’s code. In either case, the fix will become available in the next update of RoboFont.

Bugs in RoboFont or in one of its embedded libraries (fontParts, vanilla, defcon etc.) should be reported on the [Problems & Bugs] section of the RoboFont Forum.

[Problems & Bugs]: http://forum.robofont.com/category/5/problems-bugs

### Bugs in extensions

Bugs in extensions are the responsibility of the extension’s developer, and must be reported through a different channel.

- In the case of open-source extensions hosted on GitHub, you can create an [issue][GitHub issues] on the extension’s repository.
- In the case of extensions acquired through the [Extension Store], please [submit a bug report](#submitting-bug-reports) in the forum.

[GitHub issues]: http://guides.github.com/features/issues/
[Extension Store]: http://extensionstore.robofont.com/

### Bugs in your own code

Bugs in your own code should be fixed by yourself :) See {% internallink "tutorials/dealing-with-errors" %}.

If you get stuck, don’t hesitate to {% internallink 'asking-for-help' text='ask for help' %} on the forum.


Submitting bug reports
----------------------

All RoboFont users are encouraged to use the Forum for bug reports and questions, so other RoboFont users can be informed about it, and maybe even help to fix the problem. This open process allows us to create a shared knowledge base for current and future RoboFont users.

To submit a bug report, please go the RoboFont Forum and create a new discussion in the section [Problems & Bugs]. We’ll have a look at your issue and give feedback within a reasonable timespan. The thread will carry a *unsolved* tag until the bug or problem is fixed.

### Tips for an effective bug report

> Before you file a bug report, please make sure that you are using the latest version of RoboFont. If you are running an outdated RoboFont, there’s a chance that the bug you are dealing with has already been fixed.
{: .tip }

1. Please limit each bug report to one issue only.

2. Make sure to include clear and concise steps to reproduce the issue, and attach any necessary information such as {% internallink "tutorials/dealing-with-errors#error-and-traceback-example" text='tracebacks' %} and [logs](#console-log).

3. If applicable, provide a reproducible test case (eg. UFO files), or screenshots/video showing the issue.

4. Please include information about your current version of RoboFont (version number and build number), your current version of macOS, and information about any other library or tool related to the problem.

### Console log

The log is a plain text file which collects RoboFont console output while the application is running. It provides useful debugging information, specially when the error causes the application to crash (so you can’t see the tracebacks in the Output window).

The log file lives inside the user’s `Application Support` folder. There are separate files for RoboFont 1, RoboFont 3, RoboFont 4:

```text
/Users/username/Library/Application Support/RoboFont/robofont-4-py3.log
/Users/username/Library/Application Support/RoboFont/robofont-3-py3.log
/Users/username/Library/Application Support/RoboFont/robofont.log
```

> Use the keyboard shortcut `Ctrl+Alt+Cmd+O` to reveal the log file in Finder.
{: .tip }
