---
layout: page
title: How to get glyphs
tags:
  - FontParts
---

* Table of Contents
{:toc}

*This page gives an overview of the different ways to get a {% internallink 'reference/api/fontParts/rglyph' text='glyph object' %}.*


Asking a font for a glyph
-------------------------

A font behaves like a dictionary, with glyph names as keys and glyph objects as values. If we have a font object, we can ask it for one of its glyphs by name:

```python
font = CurrentFont()
glyph = font['A']
print(glyph)
```

```plaintext
<RGlyph at 4724219600>
```


Adding a new glyph to a font
----------------------------

We can also ask a font to create a new glyph with a given name:

```python
f.newGlyph('hello')
```

If a glyph with this name already exists in the font, it will be overwritten.


Current glyph
-------------

As with `CurrentFont`, we can get the glyph which is currently open in the {% internallink 'workspace/glyph-editor' text='Glyph Window' %}:

```python
glyph = CurrentGlyph()
print(glyph)
```

```plaintext
<RGlyph at 4663441680>
```

- If several Glyph Windows are open, `CurrentGlyph` will return the one at the top.
- If no Glyph Window is open:
  - If one glyph is selected in the Font Overview, that glyph will be returned.
  - If no glyph or several glyphs are selected in the Font Overview, `None` will be returned.


RGlyph
------

The `RGlyph` object can be used to create a new ‘orphan’ glyph which does not belong to any font. This can be useful as an intermediate step in a transformation, for example. When we’re done, we can insert the glyph into a font using `font.insertGlyph`.

```python
# create a new orphan glyph
glyph = RGlyph()

# do something to the glyph...

# insert the into a font
font.insertGlyph(glyph, name='hi')
```


Getting selected glyphs
-----------------------

If a font is open in the inteface, we can select one or more glyphs in the {% internallink 'workspace/font-overview' %}. We can get the selected glyphs as a list of glyph names, or as a list of glyph objects.

```python
f = CurrentFont()

# get selected glyphs as glyph objects
for glyph in f.selectedGlyphs:
    print(glyph)
```

```plaintext
<RGlyph 'a' ('foreground') at 4705008216>
<RGlyph 'c' ('foreground') at 5186352520>
<RGlyph 'd' ('foreground') at 5186352128>
<RGlyph 'b' ('foreground') at 5186353024>
```

```python
# get selected glyphs as glyph names
for glyph in f.selectedGlyphNames:
    print(glyph)
```

```plaintext
a
b
c
d
```


Getting all glyphs in a font
----------------------------

If we need to access all glyphs in the order in which they appear in a font, we can use the `glyphOrder` attribute to get a list of glyph names:

```python
f = CurrentFont()

for glyphName in f.glyphOrder:
    print(glyphName)
```

```plaintext
space
A
Agrave
Aacute
Acircumflex
Atilde
Adieresis
...
```


Getting template glyphs
-----------------------

[Template glyphs] are *placeholders* for creating new glyphs in the {% internallink 'workspace/font-overview' %}. There are a few ways to access them from a script:

`font.templateGlyphOrder`
: Returns the font’s glyph order *including* template glyphs.

`font.templateGlyphs`
: Returns all template glyphs in the font as a list of glyph objects.

`font.templateSelectedGlyphNames`
: Returns the names of all selected glyphs in the font *including* template glyphs.

[Template glyphs]: ../../../../workspace/font-overview/#template-glyphs
