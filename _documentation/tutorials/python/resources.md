---
layout: page
title: Additional resources
---

- [Python For Designers](http://pythonfordesigners.com/)
- [Python Tutorial](http://docs.python.org/3.7/tutorial)
- [How To Think Like a Computer Scientist](http://openbookproject.net/thinkcs/python/english2e/index.html)
- [Dive Into Python 3](http://diveintopython3.net/)
- [Python Practice Book](http://anandology.com/python-practice-book/index.html)
- [The History of Python](http://python-history.blogspot.nl/)
- [Python’s Design Philosophy](http://python-history.blogspot.nl/2009/01/pythons-design-philosophy.html)
- [A Brief Timeline of Python](http://python-history.blogspot.nl/2009/01/brief-timeline-of-python.html)