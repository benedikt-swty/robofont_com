---
layout: page
title: Custom modules
---

* Table of Contents
{:toc}

If you can’t find what you need in the {% internallink 'python/standard-modules' text='Standard Library' %} or in the {% internallink 'python/external-modules' text='Python Package Index' %}, you can write your own modules in Python and use them in your scripts.

*Creating your own modules is a way to organise your code and make it reusable across projects.*


Creating a module
-----------------

In its simplest form, a module is just a regular Python file containing functions, variables and objects.

```python
# myModule.py

'''An example module with silly tools.'''

designer = 'Johann Gambolputty de von Ausfern-schplenden-schlitter-crasscrenbon-fried-digger-dingle-dangle-dongle-dungle-burstein-von-knacker-thrasher-apple-banger-horowitz-ticolensic-grander-knotty-spelltinkle-grandlich-grumble-meyer-spelterwasser-kurstlich-himbleeisen-bahnwagen-gutenabend-bitte-ein-nürnburger-bratwustle-gerspurten-mitzweimache-luber-hundsfut-gumberaber-shönendanker-kalbsfleisch-mittler-aucher von Hautkopft of Ulm'
year = 2020

def setDesignerInfo(font):
    font.info.openTypeNameDesigner = designer
    font.info.copyright = f'copyright {year} {designer}'

# more tools here...
```

The file which contains the module can be stored anywhere on your computer.

The contents of a module can be imported by any script in the same parent folder:

> aFolder
> ├── myModule.py
> └── aScript.py
{: .asCode }

```python
# aScript.py
from myModule import designer
print(designer)
```

> Large packages can be a lot more complex, with lots of single files spread across multiple folders and subfolders. <!-- Submodules can be imported using the dot syntax. -->
{: .note }

> - [Packaging Python Projects](http://packaging.python.org/tutorials/packaging-projects/)
{: .seealso }

> If your code needs to work outside of RoboFont
> : use font objects from `fontParts.world`
>
> If your code will be used only in RoboFont
> : use font objects from `mojo.roboFont` directly
{: .tip }


Installing the module
---------------------

In order to make the module available to *all* scripts, it needs to be installed in the desired Python environment – see instructions in {% internallink "reference/embedded-libraries#external-libraries" text="installing external libraries" %}.


Using the module
----------------

Once the module has been installed, you can simply import it into your scripts:

```python
import myModule
```

### Scripting in RoboFont

When scripting in the {% internallink 'workspace/scripting-window' %} inside RoboFont, all font objects – `CurrentFont`, `CurrentGlyph`, `AllFonts`, etc. – are available out-of-the-box:

```python
from myModule import setDesignerInfo

font = CurrentFont()
setDesignerInfo(font)
```

> If you need to use RoboFont’s native font objects in a module, you can import them from `mojo.roboFont`.
{: .tip }

### Scripting outside the box

To make scripts that work outside of RoboFont (for example in Terminal or in a code editor), you’ll need to get the appropriate font objects from `fontParts.world`:

```python
from fontParts.world import OpenFont
from myModule import setDesignerInfo

font = OpenFont(ufo)
setDesignerInfo(font)
font.save()
font.close()
```


Maintaining and distributing your tool
----------------------------------------

- {% internallink '/tutorials/using-git' text="create a git repository" %} to store the source code and keep track of updates

- convert the into a RoboFont extension for easier integration with RoboFont
    - {% internallink '/topics/extensions' %}
    - {% internallink '/tutorials/creating-extensions' %}
    - {% internallink '/tutorials/from-tool-to-extension' %}

- submit to Mechanic for distribution to other RoboFont users
    - {% internallink '/how-tos/extensions/publishing-extensions' %}
    - {% internallink '/topics/mechanic-2' %}
