---
layout: page
title: Built-in functions
---

* Table of Contents
{:toc}

Python comes with many built-in functions which are always available as part of the language. We’ve already seen some of them in the previous sections. This page gives an overview of the most important ones.

> - [Built-in Functions](http://docs.python.org/3.7/library/functions)
{: .seealso }


Type conversions
----------------

type()
: ^
  Returns the type of a given object:
  
  ```python
  >>> type('hello')
  ```
  
  ```plaintext
  <type 'str'>
  ```
  
  ```python
  >>> type(1)
  ```
  
  ```plaintext
  <type 'int'>
  ```
  
  ```python
  >>> type(33.3)
  ```
  
  ```plaintext
  <type 'float'>
  ```

Python has several built-in functions to convert from one data type to another, these functions are object constructors. They have the same name as the data types, so it’s easy to remember them:

str()
: ^
  Transforms something into a string, or creates a new empty string if called without any argument:
  
  ```python
  >>> a = str(10)
  >>> a, type(a), len(a)
  ```
  
  ```plaintext
  ('10', <class 'str'>, 2)
  ```
  
  ```python
  >>> b = str()
  >>> b, type(b), len(b)
  ```
  
  ```plaintext
  ('', <class 'str'>, 0)
  ```
  
list()
: ^
  Transforms something into a list, or creates a new list if called without any argument:
  
  ```python
  >>> list('abcdefghijklmn')
  ```
  
  ```plaintext
  ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n']
  ```
  
  ```python
  >>> L = list()
  >>> L, type(L), len(L)
  ```
  
  ```plaintext
  ([], <class 'list'>, 0)
  ```

dict()
: ^
  Creates a dictionary from arguments (tuples, keyword arguments), or a new empty dictionary if called without any arguments:

  ```python
  >>> dict([('hello', 1), ('world', 2)]
  ```
  
  ```plaintext
  {'hello': 1, 'world': 2}
  ```
  
  ```python
  >>> dict(hello=1, world=2)
  ```
  
  ```plaintext
  {'hello': 1, 'world': 2}
  ```
  
  ```python
  >>> d = dict()
  >>> type(d), len(d), d
  ```
  
  ```plaintext
  (<class 'dict'>, 0, {})
  ```

float()
: ^
  Transforms an integer or string into a floating point number:
  
  ```python
  >>> float(27)
  ```
  
  ```plaintext
  27.0
  ```
  
  When called without any argument, `float()` creates a new float with value `0.0`:
  
  ```python
  >>> float()
  ```
  
  ```plaintext
  0.0
  ```
  
  Conversion from string to float only works if the string looks like a number:
  
  ```python
  >>> float('27.0')
  ```
  
  ```plaintext
  27.0
  ```
  
  ```python
  >>> float('27')
  ```
  
  ```plaintext
  27.0
  ```
  
  ```python
  >>> float('27,0')
  ```
  
  ```plaintext
  Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
  ValueError: could not convert string to float: '27,0'
  ```

bool()
: ^
  Transforms any data type into a boolean value:
  
  ```python
  >>> bool(1)
  ```
  
  ```plaintext
  True
  ```
  
  ```python
  >>> bool('')
  ```
  
  ```plaintext
  False
  ```
  
  When called without any argument, `bool()` returns `False`:
  
  ```python
  >>> bool()
  ```
  
  ```plaintext
  False
  ```

  > See {% internallink "python/comparisons#testing-truthiness" text="testing truthiness" %} for examples of conversions from other data types.
  {: .seealso }

set()
: ^
  Creates a new set object from a collection:

  ```python
  >>> set('abracadabra')
  ```
  
  ```plaintext
  {'r', 'b', 'c', 'a', 'd'}
  ```
  
  ```python
  >>> set([1, 5, 10, 50, 10, 5, 1])
  ```
  
  ```plaintext
  {1, 10, 50, 5}
  ```
  
  ```python
  >>> set({'a': 5, 'b': 3, 'c': 1})
  ```
  
  ```plaintext
  {'c', 'a', 'b'}
  ```
  
  ```python
  >>> set(((10, 20), (20, 25), (10, 20)))
  ```
  
  ```plaintext
  {(20, 25), (10, 20)}
  ```
  
  When called without any argument, `set()` creates a new empty set:
  
  ```python
  >>> set()
  ```
  
  ```plaintext
  set()
  ```

tuple()
: ^
  Creates a tuple from a collection:
  
  
  ```python
  >>> tuple([1, 2, 3])
  ```
  
  ```plaintext
  (1, 2, 3)
  ```
  
  ```python
  >>> tuple('abc')
  ```
  
  ```plaintext
  ('a', 'b', 'c')
  ```
  
  ```python
  >>> tuple({'a': 5, 'b': 3, 'c': 1})
  ```
  
  ```plaintext
  ('a', b', 'c')
  ```
  
  When called without any argument, `tuple()` creates a new empty tuple:
  
  ```python
  >>> tuple()
  ```
  
  ```plaintext
  ()
  ```


Collections and loops
---------------------

len()
: ^
  Returns the number of items in a collection:
  
  ```python
  >>> L = ['A', 'B', [1, 2, 3], 'D']
  >>> len(L)
  ```
  
  ```plaintext
  4
  ```
  
  ```python
  >>> len('I will not buy this record, it is scratched.')
  ```
  
  ```plaintext
  44
  ```
  
range()
: ^
  Returns an iterator which yields a sequence of numbers. It is typically used to repeat an action a number of times:
  
  ```python
  >>> range(0, 4)
  ```
  
  ```plaintext
  range(0, 4)
  ```
  
  ```python
  >>> for i in range(4):
  ...     i
  ```
  
  ```plaintext
  0
  1
  2
  3
  ```
  
enumerate()
: ^
  Returns an iterator which adds a counter to each item in a collection:
  
  ```python
  >>> L = ['Alpha', 'Bravo', 'Charlie', 'Delta']
  >>> enumerate(L)
  ```
  
  ```plaintext
  <enumerate object at 0x1039f6558>
  ```
  
  ```python
  >>> list(enumerate(L))
  ```
  
  ```plaintext
  [(0, 'Alpha'), (1, 'Bravo'), (2, 'Charlie'), (3, 'Delta')]
  ```
  
  The `enumerate()` function is typically used to loop over the items in a collection:
  
  ```python
  >>> for index, item in enumerate(L):
  ...     index, item
  ```
  
  ```plaintext
  (0, 'Alpha')
  (1, 'Bravo')
  (2, 'Charlie')
  (3, 'Delta')
  ```
  
sorted()
: ^
  Returns a sorted copy of a given collection. This is useful if we need to do a sorted loop without actually sorting the collection:
  
  ```python
  >>> L = ['Mike', 'Bravo', 'Charlie', 'Tango', 'Alpha']
  >>> for item in sorted(L):
  ...     item
  ```
  
  ```plaintext
  'Alpha'
  'Bravo'
  'Charlie'
  'Mike'
  'Tango'
  ```
  
reversed()
: ^
  The `reversed()` function is similar to `sorted()`: it returns a copy of a given collection with the items in inverse order.
  
  ```python
  >>> for item in reversed(L):
  ...     item
  ```
  
  ```plaintext
  'Alpha'
  'Tango'
  'Charlie'
  'Bravo'
  'Mike'
  ```

zip()
: ^
  Takes two separate lists, and produces a new list with pairs of values (one from each list):
  
  ```python
  >>> L1 = ['A', 'B', 'D', 'E', 'F', 'G', 'H', 'I']
  >>> L2 = [1, 2, 3, 4, 5, 6]
  >>> zip(L1, L2)
  ```
  
  ```plaintext
  <zip object at 0x1039f8608>
  ```
  
  ```python
  >>> list(zip(L1, L2))
  ```
  
  ```plaintext
  [('A', 1), ('B', 2), ('D', 3), ('E', 4), ('F', 5), ('G', 6)]
  ```
  
  The resulting list has the same number of items as the smallest of the two lists.
  
  The `zip()` function can be used to create a dictionary from a list of keys and a list of values:
  
  ```python
  >>> D = dict(zip(L1, L2))
  >>> D
  ```
  
  ```plaintext
  {'A': 1, 'B': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6}
  ```
  
map()
: ^
  Returns an iterator which applies a function to every item of a given collection. Use `list()` to convert the iterator into a list.
  
  ```python
  >>> def square(n):
  ...     return n*n
  ...
  >>> L1 = [13, 27, 4, 19, 22]
  >>> L2 = list(map(square, L1))
  >>>
  >>> print(L2)
  ```
  
  ```plaintext
  [169, 729, 16, 361, 484]
  ```
  
  Expressions with `map()` can be rewritten using *list comprehensions*:
  
  ```python
  >>> L2 = [square(n) for n in L1]
  ```
  
  The `map()` function may also take multiple collections, and pass multiple arguments to the function at each iteration.
  
  ```python
  >>> def calculate(v1, v2, v3):
  ...     return v1 * v2 - v3
  ...
  >>> L1 = [1, 2, 3, 4, 5]
  >>> L2 = [11, 12, 13, 14, 15, 16]
  >>> L3 = [21, 22, 23, 24, 25, 26, 27]
  >>> L4 = list(map(calculate, L1, L2, L3))
  >>>
  >>> print(L4)
  ```
  
  ```plaintext
  [-10, 2, 16, 32, 50]
  ```
  
  > If the given collections have different lengths, the iterator will stop when the shortest one (`L1`) reaches its end.
  {: .note }
  
filter()
: ^
  Takes a function and a collection as arguments, and returns an iterator which produces a list containing only those items from the original list for which the given function returns `True`.
  
  ```python
  >>> def test(n):
  ...     return n > 10
  ...
  >>> L1 = [3, 21, 7, 9, 14, 33, 11, 8]
  >>> L2 = list(filter(test, L1))
  >>>
  >>> print(L2)
  ```
  
  ```plaintext
  [21, 14, 33, 11]
  ```
  
  > Expressions with `filter()` can also be written using list comprehensions:
  > 
  > ```python
  > >>> L2 = [n for n in L1 if n > 10]
  > ```
  {: .tip }


Numerical
---------

Some built-in functions are useful when working with numbers:

abs()
: ^
  The `abs()` function returns the absolute value of a number:
  
  ```python
  >>> abs(-100)
  ```
  
  ```plaintext
  100
  ```

sum()
: ^
  The `sum()` function returns the sum of all numbers in a list:
  
  ```python
  >>> L = [10, 300.7, 50, 33.1]
  >>> sum(L)
  ```
  
  ```plaintext
  393.8
  ```

min() and max()
: ^
  The `min()` function returns the smallest number in a list of numbers:
  
  ```python
  >>> min(L)
  ```
  
  ```plaintext
  10
  ```
  
  The `max()` function returns the largest number in a list of numbers:
  
  ```python
  >>> max(L)
  ```
  
  ```plaintext
  300.7
  ```

round()
: ^
  The `round()` function rounds a number to a given precision.
  
  When called without any argument, `round()` converts a float to the nearest integer:
  
  ```python
  >>> round(3.1416)
  ```
  
  ```plaintext
  3
  ```
  
  If two multiples are equally close, rounding is done toward the even number:
  
  ```python
  >>> round(0.5)
  ```
  
  ```plaintext
  0
  ```
  
  ```python
  >>> round(1.5)
  ```
  
  ```plaintext
  2
  ```
  
  The function can also take a second argument to specify the number of digits after the decimal point:
  
  ```python
  >>> round(3.141592653589793, 4)
  ```
  
  ```plaintext
  3.1416
  ```


Boolean
-------

any()
: ^
  Evaluates all statements in a collection and returns:
  
  - `True` if at least one statement is `True`
  - `False` if all statements are `False`
  
  ```python
  >>> any([0, 0, 0, 0])
  ```
  
  ```plaintext
  False
  ```
  
  ```python
  >>> any([0, 0, 1, 0])
  ```
  
  ```plaintext
  True
  ```

all()
: ^
  Evaluates all statements in a collection, and returns:
  
  - `True` if all statements are `True`
  - `False` if at least one statement is `False`
  
  ```python
  >>> all([0, 0, 0, 0])
  ```
  
  ```plaintext
  False
  ```
  
  ```python
  >>> all([0, 0, 1, 0])
  ```
  
  ```plaintext
  False
  ```
  
  ```python
  >>> all([1, 1, 1, 1])
  ```
  
  ```plaintext
  True
  ```


Attributes
----------

dir()
: ^
  Returns a list of names (attributes and methods) in a given object. This is useful to get an idea of how an object is structured and what it can do.
  
  Here’s an example using `dir()` to view the contents of an imported module:
  
  ```python
  >>> import math
  >>> dir(math)
  ```
  
  ```plaintext
  ['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil', 'copysign', 'cos', 'cosh', 'degrees', 'e', 'erf', 'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'gcd', 'hypot', 'inf', 'isclose', 'isfinite', 'isinf', 'isnan', 'ldexp', 'lgamma', 'log', 'log10', 'log1p', 'log2', 'modf', 'nan', 'pi', 'pow', 'radians', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'tau', 'trunc']
  ```
  
  When called without any argument, `dir()` returns a list of names in the current local scope.
  
  If you’re starting a new interactive session in Terminal, `dir()` will return only *private* attributes and methods (with names starting with double underscores) which are made available by the Python language:
  
  ```python
  >>> dir()
  ```
  
  ```plaintext
  ['__annotations__', '__builtins__', '__doc__', '__loader__', '__name__', '__package__', '__spec__']
  ```
  
  Variable names declared during the session are added to the local scope, and are included in `dir()` results too:
  
  ```python
  >>> 'myVariable' in dir()
  ```
  
  ```plaintext
  False
  ```
  
  ```python
  >>> myVariable = 120
  >>> 'myVariable' in dir()
  ```
  
  ```plaintext
  True
  ```
  
  If you are scripting in RoboFont, you can use `dir()` to see that all the main [fontParts.world] objects are available out-of-the box in the {% internallink 'workspace/scripting-window' %}:
  
  [fontParts.world]: http://fontparts.readthedocs.io/en/latest/objectref/fontpartsworld/index.html
  
  ```python
  print(dir())
  ```
  
  ```plaintext
  ['AllFonts', 'CreateCursor', 'CurrentAnchors', 'CurrentComponents', 'CurrentContours', 'CurrentFont', 'CurrentGlyph', 'CurrentGuidelines', 'CurrentLayer', 'CurrentPoints', 'CurrentSegments', 'FontsList', 'NewFont', 'OpenFont', 'OpenWindow', 'RAnchor', 'RComponent', 'RContour', 'RFeatures', 'RFont', 'RGlyph', 'RGroups', 'RInfo', 'RKerning', '__builtins__', '__file__', '__name__', 'help']
  ```
  
getattr()
: ^
  Used to get the value of an object’s attribute. Instead of accessing the attribute directly, we pass the object and the attribute name to the `getattr()` function, and get a value back. This ‘indirect’ approach allows us to write code that is more abstract and more reflexive.
  
  Here’s a simple example using an open font in RoboFont. Let’s imagine that we want to print out some font info attributes. We could access them directly one by one, like this:
  
  ```python
  font = CurrentFont()
  print(font.info.familyName)
  print(font.info.styleName)
  print(font.info.openTypeNameDesigner)
  ```
  
  ```plaintext
  RoboType
  Roman
  Frederik Berlaen
```

Using `getattr()`, we can separate the definition of the attribute names from the retrieval of the values:

```python
attrs = ['familyName', 'styleName', 'openTypeNameDesigner']
for attr in attrs:
    print(getattr(font.info, attr))
```

setattr()
: ^
  The `setattr()` function is complementary to `getattr()` – it allows us to set the value of an object’s attribute.
  
  Here’s an example script which sets some font info values in the current font. The font info is defined in a dictionary, but it could be coming from an external file or a dialog.
  
  ```python
  font = CurrentFont()
  
  fontInfo = {
    'familyName' : 'MyFamily',
    'styleName' : 'Black Italic',
    'styleMapFamilyName' : 'Black',
    'styleMapStyleName' : 'italic',
  }
  
  for attr, value in fontInfo.items():
      setattr(font.info, attr, value)
  ```
  
> There’s also a `delattr()` function to delete an attribute from an object.
{: .note }


Character encoding
------------------

chr()
: ^
  Returns a unicode character for a given unicode value (as a decimal number):

  ```python
  >>> chr(192)
  ```
  
  ```plaintext
  'À'
  ```

ord()
: ^
  Does the opposite from `char()` – returns the unicode number for a given character:
  
  ```python
  >>> ord('A')
  ```
  
  ```plaintext
  65
  ```
