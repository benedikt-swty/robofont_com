---
layout: page
title: Setting production and final names
tags:
  - character set
  - glyph names
draft: false
---

* Table of Contents
{:toc}

## Setting production and final names with the UI

Production names and final names can be set in {% internallink 'reference/workspace/font-overview/font-info-sheet/robofont' %}.

{% image how-tos/using-production-names_font-info.png %}


public.postscriptNames
----------------------

The GOADB data can be stored in the UFO lib, so it is passed to `makeotf` when generating a font.

UFO3/RF3 supports a dictionary with production names (keys) and final names (values) in the font’s `lib.plist`, under the standard key `public.postscriptnames`.

```python
f = CurrentFont()
f.lib["public.postscriptNames"] = {"A.test": "A", "Something": "C"}
f.save()
```

It is still possible to store glyph names using the same data structure in the `com.typesupply.ufo2fdk.glyphOrderAndAliasDB`, but the public lib is preferred. If both libs are stored in the ufo, the `public.postscriptNames` will be used and the other one ignored.

{% image how-tos/using-production-names_ufo.png caption="UFO file (production names)" %}

{% image how-tos/using-production-names_otf.png caption="OTF file (final names)" %}

> Notice that `.notdef` and `space` glyphs are inserted automatically by RoboFont. These glyphs are required for a working OpenType font.
{: .note }

> - [lib.plist > public.postscriptnames (UFO3 Specification)](http://unifiedfontobject.org/versions/ufo3/lib.plist#publicpostscriptnames)
{: .seealso }
