---
layout: page
title: Making a multilingual font
draft: true
draft-hidden: true
tags:
  - character set
  - glyph names
  - accents
  - mastering
---

*This tutorial explains how to create large fonts which support many languages and writing systems.*

* Table of Contents
{:toc}


Character set
-------------

### Language support

- unicode codepages do not map to individual languages
- there are multiple sources of information about characters and languages

> Multilingual typography goes beyond simple character support – it also covers typographic conventions such as the shape of diacritics (for example in Czech, Slovak, Polish), the white space around punctuation (French), preferences for certain glyph shapes (different styles of quotes in English vs. in French, German; shape of Eng in African languages vs. in Saami; etc), and so on.
{: .note } 

### Glyph names and encoding

- glyph names & unicodes for non-latin


Font info
---------

### Coverage metadata

- unicode range / supported languages

### Providing localized font names

- name table with font info in multiple languages


Proofing
--------

### Space Center

- non-latin UFO preview in Space Center

### Proofing generated fonts

- html proofs: assign language tags
