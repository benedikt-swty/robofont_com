---
layout: page
title: Checking contour quality
tags:
  - contours
  - designing
---

* Table of Contents
{:toc}

*This page provides information about what constitutes good contours, and introduces some tools which can help you spot problems and improve your contours.*


Characteristics of good contours
--------------------------------

### Use the correct point types

- Use the appropriate point type according to your needs. {% internallink "/reference/workspace/glyph-editor/contours#point-representations" text="Here is" %} a summary of which point types are available in UFOs
- If you need a smooth transition between curve to curve or line to curve, remember to activate {% internallink "/reference/workspace/glyph-editor/contours#point-smoothness" text="point smoothness" %}

### Place points at extrema

- it helps keeping your drawings consistent
- it helps rasterizers to fit your outlines to the pixel matrix
- you can add extreme points automatically, using the {% internallink "/reference/workspace/glyph-editor/contours#contextual-menu" text="glyph editor contextual menu" %} or the {% internallink "/reference/api/fontParts/rglyph#RGlyph.extremePoints" text="RGlyph API" %}
- if angled handles are needed, place them at 45 degrees or at the same angle in all sources

> - [Extreeeme45](https://github.com/jansindl3r/Extreeeme45)
{: .seealso }

### Balanced handles

- You should strive for a balanced length for the handles of a curve segment. In other words, unless necessary, avoid extreme differences between handles of the same segment.

{% image tutorials/balanced-handles-emoji.png %}

- The following diagram shows different options for adjusting the length of your handles while keeping handles perpendicular or parallel to the baseline

{% image tutorials/balanced-handles-options.png %}

> Here are some extensions you can find on Mechanic for managing your handles
> - [CurveEqualizer](https://github.com/jenskutilek/Curve-Equalizer)
> - [Check Parallel Tool](https://github.com/jtanadi/CheckParallelTool)
{: .seealso }

### Curve continuity

Balancing handles length is crucial to manage the continuity between two curved segments. The following diagram shows how a curved outline is affected by different combination of handles.

{% image tutorials/balanced-handles-continuity.png %}

The [Speed Punk](https://github.com/yanone/speedpunk) extension helps you keep track of curve continuity directly in the glyph editor

{% image /extraDocs/carousel/extensions/SpeedPunk.png caption="Speed Punk by Yanone "%}

### Overlapping shapes

Drawing a shape with overlapping parts gives you more control, so that changing one part doesn’t affect the other. This practice is especially helpful while drawing glyphs with diagonal components, like A or V

{% image tutorials/A.png %}

> Here are some extensions to help you manage contours overlaps
> - [Add Overlaps](https://github.com/asaumierdemers/AddOverlap)
> - [Cross Overlap](https://github.com/thomgb/RF-Extensions/tree/master/AddCrossOverlap)
{: .seealso }

> It’s usually fine to have overlaps in the source files, since they can be removed when {% internallink 'tutorials/generating-fonts' text='generating fonts' %} if necessary. {% internallink 'topics/variable-fonts' %} can be generated with overlaps.
{: .note }

Useful quality check tools
--------------------------

Mechanic offers some extensions to help you audit your outlines right in the glyph editor:

{% image /extraDocs/carousel/extensions/GlyphNanny.png caption="Glyph Nanny by Tal Leming "%}

- [Glyph Nanny](https://github.com/typesupply/glyph-nanny)
- [Red Arrow](https://github.com/jenskutilek/RedArrow)

