---
layout: page
title: mojo
tags:
  - scripting
  - merz
tree:
  - ../api/merz/*
treeCanHide: true
---

The `merz` module is the advised way of drawig in RoboFont.

{% tree "/documentation/reference/api/merz/*" levels=1 "%}
