---
layout: page
title: About Window
---

The About window displays information about your current version of RoboFont.

{% image reference/workspace/about-window.png %}

Next to credits and copyrights, the window also shows version information for:

- RoboFont (version number, build number)
- embedded {% glossary AFDKO %} (version number)
- embedded {% glossary GlyphNameFormatter %} (version number, git commit)

This information is important when {% internallink 'tutorials/submitting-bug-reports' %}.

> - {% internallink 'technical-specification' %}
> - [Glyph Name Formatter](https://github.com/LettError/glyphNameFormatter) repository
{: .seealso }