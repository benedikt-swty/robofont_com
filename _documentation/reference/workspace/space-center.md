---
layout: page
title: Space Center
tags:
  - spacing
---

* Table of Contents
{:toc}

The *Space Center* is a dedicated window for previewing text and adjusting the glyph metrics in a font.

The preview text can be edited by typing characters or glyph names into the text input field, or by selecting one of the default strings from the drop-down list.

{% image reference/workspace/space-center.png %}

Several [display options](#display-options) are available from the popup menu at the right.

To modify the font’s spacing, use the *Space Matrix* at the bottom of the window to edit the width, left margin and right margin of each glyph.

Undo/redo is supported for the selected glyph.


Text Input Fields
-----------------

The text input fields support different input formats:

<table>
  <tr>
    <th width='30%'>description</th>
    <th width='30%'>example</th>
    <th width='40%'>result</th>
  </tr>
  <tr>
    <td>any text</td>
    <td><code>abc</code></td>
    <td>
      tries to match glyph names:<br/>
      <code>a</code>, <code>b</code>, <code>c</code>
    </td>
  </tr>
  <tr>
    <td>unicode characters</td>
    <td><code>&é</code></td>
    <td>
      tries to match unicode values:
      <code>ampersand</code>, <code>eacute</code>
    </td>
  </tr>
  <tr>
    <td>glyph names</td>
    <td><code>/ampersand/eacute</code></td>
    <td>get glyphs by name</td>
  </tr>
  <tr>
    <td>glyph name and layer</td>
    <td><code>/ampersand@background</code></td>
    <td>get glyphs by name from a given layer</td>
  </tr>
  <tr>
    <td>active glyph</td>
    <td><code>/?</code></td>
    <td>display the current active glyph</td>
  </tr>
  <tr>
    <td>font selection</td>
    <td><code>/!</code></td>
    <td>display the current selected glyphs in the Font Overview</td>
  </tr>
  <tr>
    <td>glyphs in group</td>
    <td><code>/:groupName</code></td>
    <td>display all glyphs for a group name if the group name exists</td>
  </tr>
  <tr>
    <td>new line</td>
    <td><code>\n</code></td>
    <td>insert a line break</td>
  </tr>
</table>

The following shortcuts are available when typing in the main text input field:

<table>
  <tr>
    <th width='30%'>shortcut</th>
    <th width='70%'>action</th>
  </tr>
  <tr>
    <td>⌥ + Escape</td>
    <td>display a list of all glyph names in the font</td>
  </tr>
  <tr>
    <td>⇧ + Enter</td>
    <td>insert a line break character <code>\n</code></td>
  </tr>
</table>

### Default strings

A list of default input strings is available from the drop-down menu below the main input field. These strings can be edited in the {% internallink "preferences-window/space-center" %}.

{% image reference/workspace/space-center_default-strings.png %}

### Control glyphs

The two smaller input fields at the left/right side are used to define control glyphs which are displayed before/after each glyph in the main string.

{% image reference/workspace/space-center_pre-after.png %}


Editing glyph metrics
---------------------

Glyph metrics can be edited by selecting and dragging a glyph or its margins, or by using the arrow keys to increase/decrease the values for *Width*, *Left* and *Right*.

### Editing with the mouse

<table>
  <tr>
    <th width='20%'>modifiers</th>
    <th width='40%'>action</th>
    <th width='40%'>result</th>
  </tr>
  <tr>
    <td></td>
    <td>click on a glyph</td>
    <td>Select the glyph.</td>
  </tr>
  <tr>
    <td></td>
    <td>click outside of a glyph</td>
    <td>Deselect the glyph.</td>
  </tr>
  <tr>
    <td>⌥</td>
    <td>click + drag left/right margin</td>
    <td>Move the left/right margin in all glyph layers.</td>
  </tr>
  <tr>
    <td></td>
    <td>click + drag left/right margin</td>
    <td>Move the left/right margin.</td>
  </tr>

  <tr>
    <td></td>
    <td>click + drag contours</td>
    <td>Move the glyph horizontally.</td>
  </tr>
  <tr>
    <td>⌥</td>
    <td>click + drag contours</td>
    <td>Move the glyph vertically.</td>
  </tr>
  <tr>
    <td>⌥ + ⌘</td>
    <td>click + drag contours</td>
    <td>Move the glyph freely.</td>
  </tr>
</table>

### Editing with the arrow keys

If a glyph is selected, its width and margins can be modified using the arrow keys.

<table>
  <tr>
    <th width='20%'>modifiers</th>
    <th width='15%'>←</th>
    <th width='15%'>→</th>
    <th width='50%'>affects</th>
  </tr>
  <tr>
    <td></td>
    <td>-1</td>
    <td>+1</td>
    <td>width (right margin)</td>
  </tr>
  <tr>
    <td>⇧</td>
    <td>-10</td>
    <td>+10</td>
    <td>width (right margin)</td>
  </tr>
  <tr>
    <td>⇧ + ⌘</td>
    <td>-100</td>
    <td>+100</td>
    <td>width (right margin)</td>
  </tr>
  <tr>
    <td>⌥</td>
    <td>-1</td>
    <td>+1</td>
    <td>left margin</td>
  </tr>
  <tr>
    <td>⌥ + ⇧</td>
    <td>-10</td>
    <td>+10</td>
    <td>left margin</td>
  </tr>
  <tr>
    <td>⌥ + ⇧ + ⌘</td>
    <td>-100 </td>
    <td>+100 </td>
    <td>left margin</td>
  </tr>
</table>

### Nummerical fields

The Space Matrix supports a simple math syntax which makes it possible to define values as calculations with values from other glyphs.

<table>
  <tr>
    <th width='30%'>description</th>
    <th width='20%'>example</th>
    <th width='50%'>result</th>
  </tr>
  <tr>
    <td>any number</td>
    <td><code>123</code></td>
    <td>Sets value to the given number.</td>
  </tr>
  <tr>
    <td>equal sign followed by glyph name</td>
    <td><code>=a</code></td>
    <td>Sets value to be equal to the value of the assigned glyph.</td>
  </tr>
  <tr>
    <td>simple math expression</td>
    <td><code>=e+10-z*10</code><!--  or <code>-=10+a</code> --></td>
    <td>Sets value to be equal to the result of the evaluated expression.</td>
  </tr>
</table>

Values in nummerical fields can also be modified incrementally using the arrow keys.

<table>
  <tr>
    <th width='20%'>modifiers</th>
    <th width='15%'>↑</th>
    <th width='15%'>↓</th>
    <th width='50%'>affects</th>
  </tr>
  <tr>
    <td></td>
    <td>-1</td>
    <td>+1</td>
    <td>selected value</td>
  </tr>
  <tr>
    <td>⇧</td>
    <td>-10</td>
    <td>+10</td>
    <td>selected value</td>
  </tr>
  <tr>
    <td>⇧ + ⌘ </td>
    <td>-100</td>
    <td>+100</td>
    <td>selected value</td>
  </tr>
</table>

Arrows and tab keys can be used to navigate to adjacent values using only the keyboard.

<table>
  <tr>
    <th width='20%'>modifiers</th>
    <th width='30%'>keys</th>
    <th width='50%'>description</th>
  </tr>
  <tr>
    <td>⌥</td>
    <td>↑ or → or ↓ or ←</td>
    <td>Jump to adjacent input fields.</td>
  </tr>
  <tr>
    <td></td>
    <td>tab</td>
    <td>Jump to the next input field.</td>
  </tr>
  <tr>
    <td>⇧</td>
    <td>tab</td>
    <td>Jump to the previous input field.</td>
  </tr>
  <tr>
    <td>⌥</td>
    <td>tab</td>
    <td>Jump to the input field below.</td>
  </tr>
</table>


Display options
---------------

The popup menu at the top right (gears icon) gives access to several display options.

{% image reference/workspace/space-center_options.png %}

<table>
  <thead>
    <tr>
      <th width='30%'>option</th>
      <th width='70%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Show Kerning</td>
      <td>
        <p>Toggle display of kerning.</p>
        <p>Only kerning defined in the UFO is shown. Kerning defined in the <code>kern</code> feature is not displayed.</p>
      </td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Suffix ▸</td>
      <td>
        <p>Use glyphs with the selected suffix whenever available.</p>
        <p>The suffix list is generated from all glyph names containing a dot. Example, if the font contains a glyph named <code>ampersand.alt</code>, the suffix <code>.alt</code> will appear in the list.</p>
      </td>
    </tr>
    <tr>
      <td>Show Layers ▸</td>
      <td>Display additional layers in the preview.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Multi Line</td>
      <td>Break text into multiple lines to fit into the window.</td>
    </tr>
    <tr>
      <td>Single Line</td>
      <td>Display text as a single line.</td>
    </tr>
    <tr>
      <td>Water Fall</td>
      <td>Display a waterfall sample with a range of text sizes.</td>
    </tr>
    <tr>
      <td>Cut Off</td>
      <td>Invert foreground and background shapes between extremes. You can edit the size of the Cut Off section using cmd ⌘ + drag when no glyph is selected. Default values are baseline and xHeight, you can modify these values in the Preferences Editor. This is a great way to visualize countershapes while spacing.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Inverse</td>
      <td>Invert foreground and background colors.</td>
    </tr>
    <tr>
      <td>Show Metrics</td>
      <td>Show metrics values for selected glyph.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Left To Right</td>
      <td>Display text from left to right.</td>
    </tr>
    <tr>
      <td>Right To Left</td>
      <td>Display text from right to left.</td>
    </tr>
    <tr>
      <td>Center</td>
      <td>Display text aligned to center.</td>
    </tr>
    <tr>
      <td>Upside Down</td>
      <td>Flip text vertically.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Beam</td>
      <td>Measure margins at the intersections with a movable horizontal beam.</td>
    </tr>
    <tr>
      <td>Guides</td>
      <td>Show font guidelines.</td>
    </tr>
    <tr>
      <td>Blues</td>
      <td>Show blue zones.</td>
    </tr>
    <tr>
      <td>Family Blues</td>
      <td>Show family blue zones.</td>
    </tr>
    <tr>
      <td>Images</td>
      <td>Show images in glyphs.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Fill</td>
      <td>Display the fill of the glyph contours.</td>
    </tr>
    <tr>
      <td>Stroke</td>
      <td>Display the stroke of the glyph contours.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Line Space</td>
      <td>Add extra space between lines of text.</td>
    </tr>
    <tr>
      <td>Tracking</td>
      <td>Add extra space between glyphs.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Show Control Glyphs</td>
      <td>Toggle the display of control glyphs in the Space Matrix.</td>
    </tr>
    <tr>
      <td>Show Space Matrix</td>
      <td>Toggle the Space Matrix.</td>
    </tr>
    <tr>
      <td>Show Template Glyphs</td>
      <td>Toggle the display of template glyphs in the preview and matrix.</td>
    </tr>
  </tbody>
</table>


Updating the preview
--------------------

When glyphs which appear in the Space Center are modified, the preview is updated automatically if the total number of glyphs is smaller than the value defined in *Maximum glyph count for live updating* in the {% internallink 'workspace/preferences-window/space-center' %}.

For input strings longer than this value, a small *refresh* icon will appear at the top right of the preview whenever a glyph is modified. Click on the icon or simply scroll the view to update the preview.

{% image reference/workspace/space-center_needs-update.png %}

- - -

> - {% internallink 'how-tos/mojo/space-center' %}
{: .seealso }
