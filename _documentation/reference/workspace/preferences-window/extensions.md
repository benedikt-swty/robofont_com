---
layout: page
title: Extensions Preferences
treeTitle: Extensions
tags:
  - extensions
---

* Table of Contents
{:toc}

The *Extensions* section collects settings related to {% internallink "topics/extensions" %}, start-up scripts and shell scripting.


Extensions
----------

Use the *Extensions* tab to install and uninstall extensions.

{% image reference/workspace/preferences-window/preferences_extensions.png %}

<table>
  <thead>
    <tr>
      <th width='35%'>option</th>
      <th width='65%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Plugins folder</td>
      <td>Set the default folder for installed extensions.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>+</td>
      <td>Install an extension.</td>
    </tr>
    <tr>
      <td>-</td>
      <td>Uninstall an extension.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Search</td>
      <td>Search for extensions by name.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Sort tools</td>
      <td>Set the order of tools in the Glyph Editor’s toolbar.</td>
    </tr>
  </tbody>
</table>


Start Up Scripts
----------------

Use the *Start Up Scripts* tab to collect scripts which will be executed when RoboFont starts up. The list supports individual `.py` files and folders containing scripts.

{% image reference/workspace/preferences-window/preferences_extensions-startup-scripts.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>+</td>
    <td>Add a start-up script file or folder to the list.</td>
  </tr>
  <tr>
    <td>-</td>
    <td>Remove a script file or folder from the list.</td>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Select an item and drag it into another position to reorder the list of scripts.</td>
  </tr>
</table>


Shell
-----

Use the *Shell* tab to configure the RoboFont shell command, so you can run RoboFont scripts from Terminal.

{% image reference/workspace/preferences-window/preferences_extensions-shell.png %}

Select *Allow remote scripting* to enable the RoboFont shell command.

> - {% internallink "using-robofont-from-command-line" %}
{: .seealso }
