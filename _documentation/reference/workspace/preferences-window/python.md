---
layout: page
title: Python Preferences
treeTitle: Python
tags:
  - scripting
---

* Table of Contents
{:toc}

The *Python* section contains settings related to the Python scripting environment in RoboFont.

{% image reference/workspace/preferences-window/preferences_python.png %}

Use the options in this section to customize the colors and the coding font in the {% internallink "scripting-window" %} and {% internallink "output-window" %}. These settings are also used by the {% internallink "features-editor" %}, the {% internallink "preferences-editor" %} and all instances of the [CodeEditor](/documentation/reference/api/mojo/mojo-ui/#mojo.UI.CodeEditor).

## Syntax colors

The table allows you to customize the code editor’s syntax coloring scheme. Each entry in the table stands for a different type of term in the Python language. To change a color, double click the color cell to open the color picker.

## Options

<table>
  <thead>
    <tr>
      <th width='35%'>option</th>
      <th width='65%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Font</td>
      <td>The coding font used in the code editor.</td>
    </tr>
    <tr>
      <td>Background</td>
      <td>Background color of the code editor.</td>
    </tr>
    <tr>
      <td>Text selection</td>
      <td>Text selection color in the code editor.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Clear text output before running script</td>
      <td>Removes all text in the Output Window before running a script.</td>
    </tr>
    <tr>
      <td>Live update output</td>
      <td>Update Scripting Window output during script execution.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Debugger Background</td>
      <td>Background color of the Output Window.</td>
    </tr>
    <tr>
      <td>Debugger text</td>
      <td>Text color in the Output Window.</td>
    </tr>
    <tr>
      <td>Pop up Output window</td>
      <td>Open the Output Window whenever something is printed, or when a warning is raised.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Script folder</td>
      <td>The default folder for Python scripts. The <em>Scripts</em> menu shows a list of all <code>.py</code> files contained in this folder.</td>
    </tr>
  </tbody>
</table>
