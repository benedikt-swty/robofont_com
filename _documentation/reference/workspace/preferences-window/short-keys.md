---
layout: page
title: Short Keys Preferences
treeTitle: Short Keys
---

The *Short Keys* section allows you to define custom keyboard short keys for every menu item in RoboFont.

{% image reference/workspace/preferences-window/preferences_short-keys.png %}

## Adding short keys for menu items

1. Find the menu item you wish to edit.
2. Double click the *Short Key* cell for this item.
3. A text input field will pop up next to the table cell.
4. Press the short keys on your keyboard, and they will appear in the floating input field. To clear a short key, click on the cross at the right.
5. Click anywhere in the table to close the popup and save the short keys.

{% image reference/workspace/preferences-window/preferences_short-keys-edit.png %}

> If the same short key is used for two menu items, it will work only with the first one.
{: .note }
