---
layout: page
title: Glyph View Preferences
treeTitle: Glyph View
---

* Table of Contents
{:toc}

The *Glyph View* section contains settings 
to customize the appearance of the {% internallink "workspace/glyph-editor" %} and its behavior while editing glyphs.

## Display

The *Display* tab collects settings for various values and options.

{% image reference/workspace/preferences-window/preferences_glyph-view-display.png %}

### Options

<table>
  <thead>
    <tr>
      <th width='40%'>option</th>
      <th width='60%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Artboard Width</td>
      <td>Width of the artboard.</td>
    </tr>
    <tr>
      <td>Artboard Height</td>
      <td>Height of the artboard.</td>
    </tr>
    <tr>
      <td>Oncurve Size</td>
      <td>Size of the on-curve points.</td>
    </tr>
    <tr>
      <td>Offcurve Size</td>
      <td>Size of the off-curve points.</td>
    </tr>
    <tr>
      <td>Grid X</td>
      <td>Width of the grid units.</td>
    </tr>
    <tr>
      <td>Grid Y</td>
      <td>Height of the grid units.</td>
    </tr>
    <tr>
      <td>Snap Points To</td>
      <td>
        <p>Snap all points to a multiple of this value while editing.</p>
        <p>If you set this value to <code>0</code> (zero), point coordinates will be floating points.</p>
        <p>If you set this value to <code>5</code>, all point coordinates will be multiples of <code>5</code>.</p>
      </td>
    </tr>
    <tr>
      <td>Outline Errors</td>
      <td>Display an Outline Error if the difference between the segment’s starting and end points is smaller then this value.</td>
    </tr>
    <tr>
      <td>Selection Stroke Width</td>
      <td>
        <p>Stroke width of selected paths.</p>
        <p>If this value is <code>0</code> (zero), the selection path will be drawn without anti-alias.</p>
      </td>
    </tr>
    <tr>
      <td>⇧ Increment</td>
      <td>The increment when using Shift down.</td>
    </tr>
    <tr>
      <td>⌘ ⇧ Increment</td>
      <td>The increment when using Command + Shift down.</td>
    </tr>
    <tr>
      <td>Info Text Size</td>
      <td>Font size of text items.</td>
    </tr>
    <tr>
      <td>Large Info Text Size</td>
      <td>Font size of large text items.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Draw With</td>
      <td>The default drawing curve type when opening a new document. It can be either <em>Cubic Beziers (PostScript)</em> or <em>Quadratic Beziers (TrueType)</em>.</td>
    </tr>
    <tr>
      <td>Next Glyph</td>
      <td>The list used when hitting <em>Next Glyph</em> or <em>Previous Glyph</em>. It can be either the “From Selected Set” or “From ‘All Glyphs’ set”.</td>
    </tr>
    <tr>
      <td>Preserve Curve on Deleting Point</td>
      <td>If a point is deleted, this value decides if the curve should be preserved or not.</td>
    </tr>
    <tr>
      <td>Display Italic Angle</td>
      <td>Use the Italic Angle in the Glyph View and Space Center.</td>
    </tr>
    <tr>
      <td>Slant Point Coordinates</td>
      <td>Slant all point coordinates. This is only a display option – points will be stored in the UFO as absolute point coordinates.</td>
    </tr>
    <tr>
      <td>Offset Point Coordinates</td>
      <td>Offset all point coordinates with the <em>Italic Slant Offset</em> setting in the font info.</td>
    </tr>
    <tr>
      <td>Draw Dragging Cross Lines</td>
      <td>Display the dragging cross line while dragging objects in the Glyph View.</td>
    </tr>
    <tr>
      <td>Draw Coordinates Background Color</td>
      <td>Draw a darker color in the back of the point coordinates.</td>
    </tr>
    <tr>
      <td>Measurement Show Glyph Metrics</td>
      <td>Use glyph metrics during measuring. (left and right margins)</td>
    </tr>
    <tr>
      <td>Use ⇧ 45° constrain</td>
      <td>Enable using the 45° constrain while dragging with Shift pressed.</td>
    </tr>
    <tr>
      <td>Measurement Show Font Metrics</td>
      <td>Use font metrics during measuring (descender, baseline, x height, ascender, cap height).</td>
    </tr>
    <tr>
      <td>Contours can close by dragging</td>
      <td>An open contour can close while dragging the first or last point on the first or last point.</td>
    </tr>
    <tr>
      <td>Contour Closing Snap Distance</td>
      <td>The snap distance used to snap the first or last point while closing an open contour.</td>
    </tr>
    <tr>
      <td>Draw Strokes Using Antialias</td>
      <td>Use anti-alias to draw strokes. This is an option for Retina displays where the device pixel is rather small.</td>
    </tr>
    <tr>
      <td>Transform Box with Off Curves</td>
      <td>The transform box will enclose all off-curve points.</td>
    </tr>
    <tr>
      <td>Snap Off Curves to guides</td>
      <td>Snap off curves while dragging to guidelines.</td>
    </tr>
    <tr>
      <td>Undo Levels</td>
      <td>Set the number of undo levels, use <code>0</code>, zero for no limit.</td>
    </tr>
  </tbody>
</table>

## Appearance

The *Appearance* tab collects settings for various colors used in the Glyph View.

To edit a color, double click on a color cell to open a color picker.

{% image reference/workspace/preferences-window/preferences_glyph-view-appearance.png %}

## Hot Keys

The *Hot Keys* tab contains a table to create and configure custom keyboard shortcuts.

{% image reference/workspace/preferences-window/preferences_glyph-view-hotkeys.png %}
