---
layout: page
title: Scripting Window
tags:
  - scripting
---

* Table of Contents
{:toc}

The *Scripting Window* is a simple code editor for writing and running Python scripts. It can be opened from the menu *Python > Scripting Window*, or using the keyboard shortcut ⌥ ⌘ R.

{% image reference/workspace/scripting-window.png %}

The code editor supports syntax highlighting for Python. The colors can be configured in the {% internallink "preferences-window/python" %}.

> The Scripting Window makes the main {% internallink 'topics/fontparts' text='FontParts objects' %} available out-of-the-box.
{: .tip }


Options
-------

An options menu is available by clicking on the gears icon at the bottom left:

{% image reference/workspace/scripting-window_options.png %}

<table>
  <thead>
    <tr>
      <th width="30%">title</th>
      <th width="70%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Show line numbers</td>
      <td>Show line number before each line.</td>
    </tr>
    <tr>
      <td>Indent using spaces</td>
      <td>Indent code using spaces instead of tabs.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Window float on top</td>
      <td>Make the Scripting Window float on top of other windows.</td>
    </tr>
  </tbody>
</table>


Toolbar
-------

The toolbar gives access to the Scripting Window’s main functions:

<table>
  <thead>
    <tr>
      <th width="30%">title</th>
      <th width="70%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Run</td>
      <td>Execute the current script.</td>
    </tr>
    <tr>
      <td>Comment</td>
      <td>Comment the selected line(s).</td>
    </tr>
    <tr>
      <td>Uncomment</td>
      <td>Uncomment the selected line(s).</td>
    </tr>
    <tr>
      <td>Indent</td>
      <td>Add indentation to selected line(s).</td>
    </tr>
    <tr>
      <td>Dedent</td>
      <td>Remove indentation from selected line(s).</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Save</td>
      <td>Save the current script as a <code>.py</code> file.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Reload</td>
      <td>Reload the script from disk. Useful if the script has been edited by another application.</td>
    </tr>
    <tr>
      <td>New</td>
      <td>Create a new empty script.</td>
    </tr>
    <tr>
      <td>Open</td>
      <td>Open an existing script from <code>.py</code> file.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Edit With…</td>
      <td>Edit script with another application.</td>
    </tr>
  </tbody>
</table>


Script Browser
--------------

The Script Browser is a collapsible side panel which makes it possible to navigate through all scripts in a chosen folder. Use the icon at the bottom left of the window to show or hide it.

{% image reference/workspace/scripting-window_script-browser.png %}

choose a folder
: click on the pop up button at the top left to choose a folder with scripts

open a script
: double-click a script to open it in the editor

> Scripts in the selected folder are also listed under the *Scripts* entry in the {% internallink 'workspace/application-menu' %}.
{: .note }


Code interaction
----------------

The code editor offers special ways to interact with some types of values in your code. When selected, these values can be modified interactively by using modifier keys together with the mouse or the arrow keys.

bool
: ^
  Boolean values can be toggled on/off like a switch with the mouse, or with the keyboard using the arrow keys.

  <table>
    <tr>
      <th width="30%">modifiers</th>
      <th width="70%">↑ or → or ↓ or ←</th>
    </tr>
    <tr>
      <td>⌘</td>
      <td>Toggle value on/off.</td>
    </tr>
  </table>

int / float
: ^
  The value of integers and floats can be modified dynamically with the mouse like sliders, or with the keyboard using the arrow keys.

  <table>
    <tr>
      <th width="30%">modifiers</th>
      <th width="35%">↑ or →</th>
      <th width="35%">↓ or ←</th>
    </tr>
    <tr>
      <td><em>⌘</em></td>
      <td><code>+1</code></td>
      <td><code>-1</code></td>
    </tr>
    <tr>
      <td>⌘ + ⌥</td>
      <td><code>+0.1</code></td>
      <td><code>-0.1</code></td>
    </tr>
    <tr>
      <td>⌘ + ⇧</td>
      <td><code>+10</code></td>
      <td><code>-10</code></td>
    </tr>
    <tr>
      <td>⌘ + ⌥ + ⇧</td>
      <td><code>+0.01</code></td>
      <td><code>-0.01</code></td>
    </tr>
  </table>

tuple
: ^
  Selected pairs of numbers can be modified together with the mouse or with the keyboard, like a movement in xy space.

  <table>
    <tr>
      <th width="30%">modifiers</th>
      <th width="17.5%">→</th>
      <th width="17.5%">←</th>
      <th width="17.5%">↑</th>
      <th width="17.5%">↓</th>
    </tr>
    <tr>
      <td><em>⌘</em></td>
      <td>x: <code>+1</code></td>
      <td>x: <code>-1</code></td>
      <td>y: <code>+1</code></td>
      <td>y: <code>-1</code></td>
    </tr>
    <tr>
      <td>⌘ + ⌥</td>
      <td>x: <code>+0.1</code></td>
      <td>x: <code>-0.1</code></td>
      <td>y: <code>+0.1</code></td>
      <td>y: <code>-0.1</code></td>
    </tr>
    <tr>
      <td>⌘ + ⇧</td>
      <td>x: <code>+10</code></td>
      <td>x: <code>-10</code></td>
      <td>y: <code>+10</code></td>
      <td>y: <code>-10</code></td>
    </tr>
    <tr>
      <td>⌘ + ⌥ + ⇧</td>
      <td>x: <code>+0.01</code></td>
      <td>x: <code>-0.01</code></td>
      <td>y: <code>+0.01</code></td>
      <td>y: <code>-0.01</code></td>
    </tr>
  </table>


Menu title and keyboard shortcut
--------------------------------

RoboFont scripts may include a custom title and a keyboard shortcut. If available, the title is displayed in the *Scripts* menu instead of the file name.

Title and shortcut must be added at the top of your script using the following syntax:

```python
# menuTitle : my script
# shortCut  : command+shift+alt+t

print('hello world')
```

Shortcuts must be constructed with the following format:

```text
command+shift+control+alt+<input>
```

Not all modifier keys are required.

The input key can be a character or any of the following keys:

```text
space tab backtab arrowup arrowdown arrowleft arrowright f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 f29 f30 f31 f32 f33 f34 f35 enter backspace delete home end pageup pagedown
```

- - -

> - {% internallink 'tutorials/using-external-code-editors' %}
{: .seealso }
