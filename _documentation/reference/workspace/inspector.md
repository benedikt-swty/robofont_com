---
layout: page
title: Inspector
---

* Table of Contents
{:toc}

The *Inpector panel* is a floating panel which gives access to several kinds of data in the current glyph. The current glyph can be a glyph in the {% internallink 'workspace/glyph-editor' %} or a selected glyph in the {% internallink 'workspace/font-overview' %}.

To open the Inspector, choose *Windows > Inspector* from the main application menu, or use the keyboard shortcut ⌘ + I.

{% image reference/workspace/inspector_collapsed.png %}

Each section of the Inspector can be collapsed or expanded by clicking on its title. Some sections can be resized.


Glyph
-----

The *Glyph* section allows you to view and edit basic attributes of the current glyph.

{% image reference/workspace/inspector_glyph.png %}

### Attributes

<table>
  <tr>
    <th width="30%">option</th>
    <th width="70%">description</th>
  </tr>
  <tr>
    <td>Name</td>
    <td>The name of the current glyph.</td>
  </tr>
  <tr>
    <td>Unicode</td>
    <td>The unicode(s) of the current glyph.</td>
  </tr>
  <tr>
    <td>Width</td>
    <td>The advance width of the current glyph.</td>
  </tr>
  <tr>
    <td>Left</td>
    <td>The left margin of the current glyph.</td>
  </tr>
  <tr>
    <td>Right</td>
    <td>The right margin of the current glyph.</td>
  </tr>
  <tr>
    <td>Mark</td>
    <td>The mark color of the current glyph.</td>
  </tr>
  <tr>
    <td>Exclude during export</td>
    <td>A flag indicating if the glyph should be skipped when generating fonts</td>
  </tr>
</table>

### Editing glyph names

When a glyph name is edited, a sheet appears in the font window with options to:

- rename all references to this glyph in components, groups and kerning
- set the unicode value automatically based on the glyph name
- rename the same glyph in other layers

{% image reference/workspace/font-overview_rename-glyph-sheet.png %}

> Glyph names which already exist in the font are not allowed when renaming glyphs.
{: .note }

### Editing unicode values

When the unicode value is edited, a sheet appears in the font window with options to:

- set the glyph name automatically
- if the glyph name is changed, rename all references to this glyph in components, groups and kerning

{% image reference/workspace/font-overview_change-unicode-sheet.png %}


Preview
-------

The *Preview* section shows a simple live preview of the current glyph.

{% image reference/workspace/inspector_preview.png %}


Layers
------

The *Layers* section offers an interface for creating and managing layers, and for setting the display attributes of each layer.

{% image reference/workspace/glyph-editor/inspector_layers.png %}

The **current layer** is indicated by a circle with a black dot.

The **default layer** is indicated by a star.

### Actions

<table>
  <tr>
    <th width="30%">action</th>
    <th width="70%">description</th>
  </tr>
  <tr>
    <td>drag and drop</td>
    <td>Change the layer order in the font.</td>
  </tr>
  <tr>
    <td>click and hold</td>
    <td>Rename a layer.</td>
  </tr>
  <tr>
    <td>right click</td>
    <td>Open a contextual menu with an option to make the current layer the default one.</td>
  </tr>
  <tr>
    <td>double click on color</td>
    <td>Open a color palette to choose the layer color.</td>
  </tr>
  <tr>
    <td>+ / -</td>
    <td>Add or remove a layer.</td>
  </tr>
  <tr>
    <td>display controls</td>
    <td>
      <p>Control how the layer is displayed in the Glyph View.</p>
      <ul>
        <li><em>if the selected layer is the active layer:</em><br/>
            display controls affect the display of the active layer.
        </li>
        <li><em>if the selected layer is not the active layer</em><br/>
            display controls affect the display of the layers in the background
        </li>
      </ul>
    </td>
  </tr>
</table>

> Layer names must contain ASCII characters only, and spaces, slashes and semicolons are not allowed.
{: .note }

### Layer display options

Use the icon buttons to toggle different display attributes for layers:

<table>
  <tr>
    <th width="10%">icon</th>
    <th width="90%">layer attributes</th>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_visibility.png" /></td>
    <td>visibility</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_fill.png" /></td>
    <td>fill</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_stroke.png" /></td>
    <td>stroke</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_points.png" /></td>
    <td>points</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/layer-display-options_positions.png" /></td>
    <td>point coordinates</td>
  </tr>
</table>

> - {% internallink "glyph-editor/layers" %}
{: .seealso }

### Contextual menu

Right-click on a layer to open a contextual menu with the option to make that layer the default one.

{% image reference/workspace/inspector_layers-contextual-menu.png %}


Transform
---------

The *Transform* section offers several tools to apply transformations to one or more glyphs.

{% image reference/workspace/glyph-editor/inspector_transform.png %}

### Alignment options

Use the icons at the top to define an origin point for the transformations, and to flip and align shapes.

<table>
  <tr>
    <th width="20%">option</th>
    <th width="80%">description</th>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/inspector_transform_origin.png" /></td>
    <td>Select the origin point for the transformation. If no point is selected <code>(0,0)</code> will be used as origin.</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/inspector_transform_mirror-x.png" /></td>
    <td>Mirror horizontally.</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/inspector_transform_mirror-y.png" /></td>
    <td>Mirror vertically.</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/inspector_transform_align-bottom.png" /></td>
    <td>Align selected objects to the bottom.</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/inspector_transform_align-middle.png" /></td>
    <td>Center selected objects vertically.</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/inspector_transform_align-top.png" /></td>
    <td>Align selected objects to the top.</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/inspector_transform_align-left.png" /></td>
    <td>Align selected objects to the left.</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/inspector_transform_align-center.png" /></td>
    <td>Center selected objects horizontally.</td>
  </tr>
  <tr>
    <td><img src="../../../../images/reference/workspace/inspector_transform_align-right.png" /></td>
    <td>Align selected objects to the right.</td>
  </tr>
</table>

### Transformations

Set the individual transformation settings to define a transformation matrix.

<table>
  <tr>
    <th width="20%">option</th>
    <th width="80%">description</th>
  </tr>
  <tr>
    <td>Move</td>
    <td>Move selected objects by <code>(x,y)</code> values. Optionally equalize <code>x</code> and <code>y</code>.</td>
  </tr>
  <tr>
    <td>Scale</td>
    <td>Scale selected objects according to <code>(x,y)</code> percentage values. Optionally equalize <code>x</code> and <code>y</code>.</td>
  </tr>
  <tr>
    <td>Rotate</td>
    <td>Rotate selected objects by an angle (in degrees).</td>
  </tr>
  <tr>
    <td>Skew</td>
    <td>Skew selected objects by angles <code>α,β</code>. Optionally equalize <code>α</code> and <code>β</code>.</td>
  </tr>
  <tr>
    <td>Snap</td>
    <td>Snap selected objects to a grid.</td>
  </tr>
</table>

### Transform options

The *Transform* button opens a popup menu with options to control the scope of the transformation within the font.

{% image reference/workspace/inspector_transform_apply.png %}

<table>
  <thead>
    <tr>
      <th width="45%">option</th>
      <th width="55%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Transform</td>
      <td>Apply all transformations to the current glyph. The transformation can be a combination of move, scale, skew and snap transformations.</td>
    </tr>
    <tr>
      <td>Transform Selected Glyphs</td>
      <td>Apply all transformations to the selected glyphs in the Font Overview.</td>
    </tr>
    <tr>
      <td>Transform All Glyphs</td>
      <td>Apply all transformations to all glyphs in the font.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Mirror Selected Glyphs Vertical</td>
      <td>Mirror all selected glyphs vertically.</td>
    </tr>
    <tr>
      <td>Mirror Selected Glyphs Horizontal</td>
      <td>Mirror all selected glyphs horizontally.</td>
    </tr>
    <tr>
      <td>Mirror All Glyphs Vertical</td>
      <td>Mirror all glyphs vertically.</td>
    </tr>
    <tr>
      <td>Mirror All Glyphs Horizontal</td>
      <td>Mirror all glyphs horizontally.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Transform Again</td>
      <td>Apply the same transformation again on the current glyph. This can be handy if the transformation values were set by a Transform action in the Glyph View.</td>
    </tr>
    <tr>
      <td>Transform Again Selected Glyphs</td>
      <td>Apply the same transformation again on the selected glyphs.</td>
    </tr>
    <tr>
      <td>Transform Again All Glyphs</td>
      <td>Apply the same transformation again on all glyphs in the font.</td>
    </tr>
  </tbody>
</table>

### Transform scope

The gear icon opens a second popup menu with options to control the scope of the transformation within each glyph.

{% image reference/workspace/inspector_transform_options.png %}

<table>
  <thead>
    <tr>
      <th width="45%">option</th>
      <th width="55%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Transform Contours</td>
      <td>Apply transformations to contours.</td>
    </tr>
    <tr>
      <td>Transform Metrics</td>
      <td>Apply transformation to metrics.</td>
    </tr>
    <tr>
      <td>Transform Components</td>
      <td>Apply transformation to components.</td>
    </tr>
    <tr>
      <td>Transform Anchors</td>
      <td>Apply transformation to anchors.</td>
    </tr>
    <tr>
      <td>Transform Image</td>
      <td>Apply transformation to image.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Align Off Curve Points</td>
      <td>Apply alignment to off-curve points.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Set Italic Angle as Skew α Value</td>
      <td>Set the skew angle as the font’s Italic Angle.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Reset Panel</td>
      <td>Reset the panel to default settings.</td>
    </tr>
  </tbody>
</table>

> - {% internallink "glyph-editor/transform" %}
{: .seealso }


Points
------

The *Points* section shows a list of all contours and points in the current glyph.

{% image reference/workspace/glyph-editor/inspector_points.png %}

### Attributes

<table>
  <tr>
    <th width="25%">option</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>✔</td>
    <td>A flag indicating if the contour or point is selected.</td>
  </tr>
  <tr>
    <td>contours</td>
    <td>A list of contours. Each contour is a collapsible list of points.</td>
  </tr>
  <tr>
    <td>X / Y</td>
    <td>The position of each point.</td>
  </tr>
  <tr>
    <td>smoothness</td>
    <td>
      <p>A flag indicating if the point is <em>smooth</em> or <em>unsmooth</em>.</p>
    </td>
  </tr>
  <tr>
    <td>name</td>
    <td>
      <p>A name for the point <em>(optional)</em>. Must not be unique.</p>
    </td>
  </tr>
</table>

### Contextual menu

Right-click in the list to open a contextual menu with options to select and delete points.

{% image reference/workspace/inspector_points-contextual-menu.png %}

> - {% internallink "glyph-editor/contours" %}
{: .seealso }


Components
----------

The *Components* section shows a list of all components in the current glyph.

{% image reference/workspace/glyph-editor/inspector_components.png %}

Double-click on any value to edit it.

### Attributes

<table>
  <tr>
    <th width="25%">option</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>✔︎</td>
    <td>A flag indicating if the component is selected.</td>
  </tr>
  <tr>
    <td>Base Glyph</td>
    <td>The glyph referenced by the component.</td>
  </tr>
  <tr>
    <td>Transformation</td>
    <td>The component’s <a href='../../../topics/transformations'>transformation matrix</a> representing its translation, scale, rotation and skew settings.</td>
  </tr>
</table>

### Contextual menu

Right-click in the list to open a contextual menu with options to select and delete components.

{% image reference/workspace/inspector_points-contextual-menu.png %}

> - {% internallink "glyph-editor/components" %}
{: .seealso }


Anchors
-------

The *Anchors* section shows a list of all anchors in the current glyph.

{% image reference/workspace/glyph-editor/inspector_anchors.png %}

Double-click any value to edit it.

### Attributes

<table>
  <tr>
    <th width="25%">option</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>✔</td>
    <td>A flag indicating if the anchor is selected.</td>
  </tr>
  <tr>
    <td>X / Y</td>
    <td>The position of the anchor.</td>
  </tr>
  <tr>
    <td>Name</td>
    <td>The name of the anchor.</td>
  </tr>
  <tr>
    <td>Color</td>
    <td>The color of the anchor. <em><em>(optional)</em></em></td>
  </tr>
</table>

### Contextual menu

Right-click in the list to open a contextual menu with options to select and delete anchors.

{% image reference/workspace/inspector_points-contextual-menu.png %}

> - {% internallink "glyph-editor/anchors" %}
{: .seealso }


Guidelines
----------

The *Guidelines* section shows a list of all guidelines available in the current glyph, including both font-level (global) and glyph-level (local) guidelines.

{% image reference/workspace/glyph-editor/inspector_guidelines.png %}

Click and hold to edit any value. Double-click on the color cell to edit the color.

### Attributes

<table>
  <tr>
    <th width="25%">option</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>X / Y</td>
    <td>The guideline’s origin position.</td>
  </tr>
  <tr>
    <td>Angle</td>
    <td>The angle of the guideline in degrees.</td>
  </tr>
  <tr>
    <td>Name</td>
    <td>The name of the guideline. <em>(optional)</em></td>
  </tr>
  <tr>
    <td>Global</td>
    <td>
      <p>A flag indicating if the guideline is <em>global</em> (font-level) or <em>local</em> (glyph-level). Click on the checkbox to make a local guideline global, and vice versa.</p>
    </td>
  </tr>
  <tr>
    <td>Color</td>
    <td>The color of the guideline. <em>(optional)</em></td>
  </tr>
</table>

> - {% internallink "glyph-editor/guidelines" %}
{: .seealso }


Note
----

The *Note* section contains a simple text box for adding an optional note to the current glyph. This note is stored in the UFO only and does not make it into compiled fonts.

{% image reference/workspace/inspector_note.png %}
