---
layout: page
title: Glyph Editor
tags:
  - contours
tree:
  - tools
  - contours
  - components
  - anchors
  - guidelines
  - layers
  - images
  - display-options
  - transform
treeCanHide: true
---

* Table of Contents
{:toc}

The *Glyph Editor* is where all visual glyph data is created and edited, one glyph at a time.

{% image tutorials/glyph-editor.png %}

The Glyph Editor can be opened by double-clicking a glyph cell in the {% internallink 'workspace/font-overview' %}.


Interactive tools
-----------------

The Glyph Editor offers a set of {% internallink "workspace/glyph-editor/tools" text='basic tools' %} to interact with glyph shapes:

<style>
#glyph-editor-tool .col { text-align: center; }
</style>

<div id="glyph-editor-tool" class='row'>
<div class='col' markdown=1>
{% image reference/workspace/toolbarToolsArrow.png %}  
[Editing Tool](tools/editing-tool)
</div>
<div class='col' markdown=1>
{% image reference/workspace/toolbarToolsPen.png %}
[Bezier Tool](tools/bezier-tool)
</div>
<div class='col' markdown=1>
{% image reference/workspace/toolbarToolsSlice.png %}
[Slice Tool](tools/slice-tool)
</div>
<div class='col' markdown=1>
{% image reference/workspace/toolbarToolMeasurement.png %}
[Measurement Tool](tools/measurement-tool)
</div>
</div>

Many other interactive tools are available as extensions.


Glyph data
----------

A glyph may contain different kinds of visual and non-visual data.

Visual glyph data can be edited interactively in the Glyph View:

- {% internallink "glyph-editor/contours" %}
- {% internallink "glyph-editor/components" %}
- {% internallink "glyph-editor/anchors" %}
- {% internallink "glyph-editor/guidelines" %}
- {% internallink "glyph-editor/layers" %}
- {% internallink "glyph-editor/images" %}

All kinds of glyph data can be edited using the {% internallink "inspector" text="Inspector panel" %}.

{% image reference/workspace/inspector_glyph.png %}


Display options
---------------

The Glyph View can display several types of visual glyph data. Use the {% internallink "glyph-editor/display-options" %} menu (bottom left) to toggle the visibility of each display layer.

{% image reference/workspace/glyph-editor_display-options.png %}

Display options allow you to optimize the Glyph Editor display to the task at hand, hiding irrelevant information and leaving only what’s relevant visible.

> You can assign a *short cut* to toggle each display option from the preferences (glyph view > hot keys)
{: .note }

{% image reference/workspace/display-layers.gif %}



Navigation
----------

### Zooming and panning

Use the shortkeys `Cmd +` / `Cmd -` or `Alt ⌥` + scrollwheel to zoom in / out in the Glyph View. 

Use `Cmd 0` to fit the glyph contents into the view.

To choose an exact zoom level, use the zoom menu at the bottom left:

{% image reference/workspace/glyph-editor_zoom-menu.png %}

To move around in the Glyph View, press the space bar and drag, or use the scrollwheel.

### Preview Glyph

Use the shortcut `<` to preview the glyph.

{% video reference/workspace/preview.mp4 %}

While pressing `<`, you can add the modifier `Alt ⌥` and use the horizontal arrows to jump to previous and next glyph. If you use vertical arrows, you can move through the glyph layers. 

{% video reference/workspace/jump_in_preview_mode.mp4 %}

### Jump to previous / next glyphs

Use the shortkeys `Cmd+(` and `Cmd+)` to switch to the previous / next glyph.

> The previous / next glyph order may refer to the whole font, or just to the selected {% internallink 'documentation/topics/smartsets' %}, depending on the settings in the {% internallink 'workspace/preferences-window/glyph-view' %}.
{: .note}

### Jump to Glyph popup

To go to any other glyph in the font, press `J` to open the *Jump to Glyph* pop-up window, and start typing the glyph name. The dialog will show a list of possible glyphs to choose from.

{% image reference/workspace/jump_to_glyph.png %}

Transform Mode
--------------

{% internallink "glyph-editor/transform" %} allows you to apply scale, rotation and skew transformations interactively to different types of objects.

{% image reference/workspace/glyph-editor_transform.png %}


Contextual menu
---------------

The contextual menu can be opened with a right-click using the {% internallink 'glyph-editor/tools/editing-tool' %}.

{% image reference/workspace/glyph-editor_editing-tool_contextual-menu.png %}

<table>
  <thead>
    <tr>
      <th width="35%">action</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Copy To Layer →</td>
      <td>It opens a submenu listing all the other layers where the contents of the current layer can be copied</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Remove Overlap</td>
      <td>Remove overlaps in all contours.</td>
    </tr>
    <tr>
      <td>Add Extreme Points</td>
      <td>Add extreme points to all contours.</td>
    </tr>
    <tr>
      <td>Close Open Contours</td>
      <td>Close all open contours.</td>
    </tr>
    <tr>
      <td>Reverse Contours</td>
      <td>Reverse all contours.</td>
    </tr>
    <tr>
      <td>Auto Contour order</td>
      <td>Try to order the contours.</td>
    </tr>
    <tr>
      <td>Correct Direction (PS)</td>
      <td>Correct contour direction (PostScript) in all contours.</td>
    </tr>
    <tr>
      <td>Correct Direction (TT)</td>
      <td>Correct contour direction (TrueType) in all contours.</td>
    </tr>
    <tr>
      <td>Convert Contour to Cubic</td>
      <td>Convert selected quadratic beziers to cubic</td>
    </tr>
    <tr>
      <td>Convert Contour to Quadratic</td>
      <td>Convert selected cubic beziers to quadratic</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Add Component</td>
      <td>Open the <em>Add Component</em> sheet.</td>
    </tr>
    <tr>
      <td>Components →</td>
      <td>It opens a submenu listing link to all components reference glyphs and a "Decompose All" option.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Add Anchor</td>
      <td>Open an <em>Add Anchor</em> sheet.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Add Guideline</td>
      <td>Adds a guideline to the glyph.</td>
    </tr>
    <tr>
      <td>Remove Guidelines</td>
      <td>Remove all glyph guidelines.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Lock Sibearings</td>
      <td>Lock all sidebearings (global setting).</td>
    </tr>
    <tr>
      <td>Lock Guidelines</td>
      <td>Lock all guidelines (global setting).</td>
    </tr>
    <tr>
      <td>Lock Image</td>
      <td>Lock all images (global setting).</td>
    </tr>
  </tbody>
</table>

Clicking on different types of glyph objects reveals a specific contextual menu for each:

<!-- layers?? -->

- [contours](contours#contextual-menu)
- [anchors](anchors#contextual-menu)
- [components](components#contextual-menu)
- [images](images#contextual-menu)
- [guidelines](guidelines#contextual-menu)


Glyph menu
----------

While the Glyph Editor is active, the options in the {% internallink "workspace/application-menu#glyph" text="Glyph menu" %} become available.

{% image reference/workspace/application-menu_glyph.png %}
