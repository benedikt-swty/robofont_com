---
layout: page
title: Preferences Editor
tags:
  - preferences
---

The *Preferences Editor* is a special window for editing raw Preferences settings in [json] format. It can be opened from the *RoboFont > Preferences Editor* menu while pressing ⌥, or using the shortcut keys ⌥ ⌘ , (Alt + Command + comma).

{% image reference/workspace/preferences-editor.png %}

The Prefences Editor gives access to more settings than available via the {% internallink "preferences-window" %}.

[json]: http://en.wikipedia.org/wiki/JSON

Use the editor to modify the values. When you close the window, you will be asked to save or discard the changes. Here follows the default values from the preferences editor:

```python
###########################
# Edit defaults with json #
###########################

{
   # Output window background color
   "DebuggerBackgroundColor": [0, 0, 0, 1],
   # Output window text color
   "DebuggerTextColor": [1, 1, 1, 1],
   # Editor background color
   "PyDEBackgroundColor": [1, 1, 1, 1],
   # Editor hightlight color
   "PyDEHightLightColor": [0.6463, 0.8026, 0.9992, 1],
   # Editor hightlight text Colors
   "PyDETokenColors": {
      "Token": "#000000",
      "Token.Comment": "#A3A3A3",
      "Token.Error": "#FF0000",
      "Token.Generic.Emph": "#FF00B3",
      "Token.Generic.Heading": "#813E94",
      "Token.Generic.Strong": "#6F00FF",
      "Token.Generic.Subheading": "#1A8BAD",
      "Token.Keyword": "#4978FC",
      "Token.Keyword.Namespace": "#1950FD",
      "Token.Literal.Number": "#CC5858",
      "Token.Literal.Number.Float": "",
      "Token.Literal.Number.Hex": "",
      "Token.Literal.Number.Oct": "",
      "Token.Literal.String": "#FC00E7",
      "Token.Literal.String.Doc": "#FC00E7",
      "Token.Name": "",
      "Token.Name.Attribute": "#ff0086",
      "Token.Name.Builtin": "#31A73E",
      "Token.Name.Builtin.Pseudo": "#FF8700",
      "Token.Name.Class": "#ff0086",
      "Token.Name.Constant": "#0086d2",
      "Token.Name.Decorator": "",
      "Token.Name.Exception": "#FF1400",
      "Token.Name.Function": "#ff0086",
      "Token.Name.Namespace": "",
      "Token.Name.Tag": "#fb660a",
      "Token.Name.Variable": "#fb660a",
      "Token.Operator": "#6D37C9",
      "Token.Operator.Word": "#6D37C9",
      "Token.Punctuation": "#6E6E6E",
      "Token.Text": ""
   },
   # Enable auto updater
   "SUEnableAutomaticChecks": 0,
   # Auto save minutes interval
   "autoSaveMinutes": 10,
   # Auto save path
   "autoSavePath": "/Users/username/Library/Autosave Information",
   # Font size for displaying larg text
   "bigTextFontSize": 15,
   # A dictionary of character sets
   "charsets": {
      "Latin-1": [
          "space", "exclam", "quotesingle", "quotedbl", "numbersign", "dollar", "percent", "ampersand", "parenleft", "parenright", "asterisk", "plus", "comma", "hyphen", "period", "slash", "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "colon", "semicolon", "less", "equal", "greater", "question", "at", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "bracketleft", "backslash", "bracketright", "asciicircum", "underscore", "grave", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "braceleft", "bar", "braceright", "asciitilde", "exclamdown", "cent", "sterling", "currency", "yen", "brokenbar", "section", "dieresis", "copyright", "ordfeminine", "guillemetleft", "logicalnot", "registered", "macron", "degree", "plusminus", "twosuperior", "threesuperior", "acute", "mu", "paragraph", "periodcentered", "cedilla", "onesuperior", "ordmasculine", "guillemetright", "onequarter", "onehalf", "threequarters", "questiondown", "Agrave", "Aacute", "Acircumflex", "Atilde", "Adieresis", "Aring", "AE", "Ccedilla", "Egrave", "Eacute", "Ecircumflex", "Edieresis", "Igrave", "Iacute", "Icircumflex", "Idieresis", "Eth", "Ntilde", "Ograve", "Oacute", "Ocircumflex", "Otilde", "Odieresis", "multiply", "Oslash", "Ugrave", "Uacute", "Ucircumflex", "Udieresis", "Yacute", "Thorn", "germandbls", "agrave", "aacute", "acircumflex", "atilde", "adieresis", "aring", "ae", "ccedilla", "egrave", "eacute", "ecircumflex", "edieresis", "igrave", "iacute", "icircumflex", "idieresis", "eth", "ntilde", "ograve", "oacute", "ocircumflex", "otilde", "odieresis", "divide", "oslash", "ugrave", "uacute", "ucircumflex", "udieresis", "yacute", "thorn", "ydieresis", "dotlessi", "circumflex", "caron", "breve", "dotaccent", "ring", "ogonek", "tilde", "hungarumlaut", "quoteleft", "quoteright", "minus"
      ]
   },
   # Check for external changes in the UFO file.
   "checkExternalChanges": 1,
   # Minimum distance for creating a bcp while dragging
   "createBCPdraggingMin": 10,
   # Background Layer Color
   "defaultBackgroundLayerColor": [0, 0.8, 0.2, 0.7],
   # Foreground Layer Color
   "defaultForegroundLayerColor": [0.5, 0, 0.5, 0.7],
   # Guideline magnetic distance
   "defaultGuidelineMagnetic": 5,
   # Guideline show measurment
   "defaultGuidelineShowMeasurment": 0,
   # Maximum amount of tools in the toolbar
   "eventMaxItemsInToolbar": 7,
   # Start up scripts
   "extensionExternalTools": [
      
   ],
   # Font overview cell size
   "fontCollectionViewGlyphSize": 60,
   # The font compiler tool used to generate binary font data: 'afdko' or 'fontmake'.
   "fontCompilerTool": "afdko",
   # Show smart sets
   "fontWindowShowSetsPane": 1,
   # Show find pane
   "fontWindowShowSmartListArgumentsPane": 1,
   # Header line separator color
   "glypHCellHeaderLineColor": [0, 0, 0, 0.2],
   # Alternative text color
   "glyphCellAlternateTextColor": [1, 1, 1, 1],
   # Background color
   "glyphCellBackgroundColor": [1, 1, 1, 1],
   # Glyph changed color
   "glyphCellChangedColor": [0.3, 0.3, 0.3, 0.8],
   # Glyph changed highlight color
   "glyphCellChangedHighlightColor": [0.4, 0.4, 0.4, 0.8],
   # Glyph color
   "glyphCellGlyphColor": [0, 0, 0, 1],
   # Default glyph layer color
   "glyphCellGlyphLayerDefaultColor": [0.5, 0.5, 0.5, 0.7],
   # Header color
   "glyphCellHeaderColor": [0.6, 0.6, 0.6, 0.4],
   # Header highlight color
   "glyphCellHeaderHighlightColor": [0.7, 0.7, 0.7, 0.4],
   # Layer indicator color
   "glyphCellLayerIndicatorColor": [0.5, 0, 0.5, 0.7],
   # Metrics color
   "glyphCellMetricsColor": [0, 0, 0, 0.08],
   # Metrics line color
   "glyphCellMetricsLineColor": [0, 0, 0, 0.08],
   # Note indicator color
   "glyphCellNoteIndicatorColor": [0.5, 0.5, 0.5, 1],
   # Skip glyph export indicator color
   "glyphCellSkipExportIndicatorColor": [0.9, 0.17, 0.3, 1],
   # Template glyph cell background color
   "glyphCellTemplateGlyphBackgroundColor": [0.9, 0.9, 0.9, 1],
   # Template text color
   "glyphCellTemplateGlyphTextColor": [1, 1, 1, 1],
   # Template text shadow color
   "glyphCellTemplateGlyphTextShadowColor": [0.6, 0.6, 0.6, 1],
   # Text color
   "glyphCellTextColor": [0.22, 0.22, 0.27, 1],
   # Unicode replace character for template glyphs
   "glyphCellViewUnicodeReplacement": "✍",
   # Collection background color
   "glyphCollectionBackgroundColor": [0.6, 0.6, 0.6, 1],
   # Collection grid color
   "glyphCollectionGridColor": [0.6, 0.6, 0.6, 1],
   # Insertion location color
   "glyphCollectionInsertionLocationColor": [0.16, 0.3, 0.85, 1],
   # Insertion location shadow color
   "glyphCollectionInsertionLocationShadowColor": [1, 1, 1, 1],
   # Add Anchor
   "glyphViewAddAnchorKey": "",
   # Add Component
   "glyphViewAddComponentKey": "",
   # Add Extreme points
   "glyphViewAddExtremesKey": "",
   # Add a Guideline on mouse down.
   "glyphViewAddGuidelineKey": "g",
   # Allow to join contours on hovering
   "glyphViewAllowsJoiningContours": 1,
   # Alternate Fill Color
   "glyphViewAlternateFillColor": [0.3, 0.3, 0.3, 1],
   # Anchor Color
   "glyphViewAnchorColor": [1, 0, 0, 1],
   # Anchor Index Text Background Color
   "glyphViewAnchorIndexBackgroundColor": [0, 0, 0, 0.4],
   # Anchor Index Text Color
   "glyphViewAnchorIndexColor": [1, 1, 1, 1],
   # Anchor Text Color
   "glyphViewAnchorTextColor": [1, 0, 0, 1],
   # Minimal Horizontal Art Board Border
   "glyphViewArtBoardHorizontalBorder": 1000,
   # Minimal Vertical Art Board Border
   "glyphViewArtBoardVerticalBorder": 750,
   # Auto Contour Order
   "glyphViewAutoContourOrderKey": "",
   # Background Color
   "glyphViewBackgroundColor": [1, 1, 1, 1],
   # Max image size
   "glyphViewBackgroundImageMaxSize": 1000,
   # Bitmap Color
   "glyphViewBitmapColor": [0, 0, 0, 1],
   # Draw bitmap in the background of the glyph
   "glyphViewBitmapDrawFull": 1,
   # Bitmap sizes
   "glyphViewBitmapSizes": [
       1, 2, 3, 4, 5
   ],
   # Blues Color
   "glyphViewBluesColor": [0.5, 0.95, 0.95, 0.7],
   # Break Contour
   "glyphViewBreakContourKey": "b",
   # Allows to move left margin
   "glyphViewCanMoveLeftMargin": 1,
   # Allow zooming while dragging
   "glyphViewCanZoomWhileDragging": 0,
   # Background Color of the Clipped View
   "glyphViewClipBackgroundColor": [0.6, 0.6, 0.6, 1],
   # Close/Join Contour(s)
   "glyphViewCloseContourKey": "",
   # Snap distance to close contours
   "glyphViewCloseContourSnapDistance": 5,
   # Command shift increment
   "glyphViewCommandShiftIncrement": 100,
   # Component Fill Color
   "glyphViewComponentFillColor": [0.7, 1, 0.7, 1],
   # Component Index Text Background Color
   "glyphViewComponentIndexBackgroundColor": [0, 0, 0, 0.4],
   # Component Index Text Color
   "glyphViewComponentIndexColor": [1, 1, 1, 1],
   # Component Info Text Background Color
   "glyphViewComponentInfoBackgroundColor": [0, 0, 0, 0.3],
   # Component Info Text Color
   "glyphViewComponentInfoColor": [0, 0, 0, 1],
   # Component Stroke Color
   "glyphViewComponentStrokeColor": [0, 0, 0, 0.5],
   # Close Contour at Point Stroke Color
   "glyphViewContinueCloseContourAtPointStrokeColor": [0, 0, 0, 1],
   # Continue Drawing at Point Stroke Color
   "glyphViewContinueDrawingAtPointStrokeColor": [0, 0, 0, 1],
   # Contour Index Text Background Color
   "glyphViewContourIndexBackgroundColor": [0, 0, 0, 0.4],
   # Contour Index Text Color
   "glyphViewContourIndexColor": [1, 1, 1, 1],
   # Copy / Swap to layer
   "glyphViewCopyLayerToLayerKey": "l",
   # Corner Point Fill Color
   "glyphViewCornerPointsFill": [1, 0.3, 0.4, 1],
   # Corner Point Stroke Color
   "glyphViewCornerPointsStroke": [1, 1, 1, 1],
   # Correct Contour Direction
   "glyphViewCorrectDirectionKey": "",
   # Handle Stroke Color (Cubic Beziers, PostScript)
   "glyphViewCubicHandlesStrokeColor": [0.4, 0.4, 0.4, 1],
   # Curve Length Text Background Color
   "glyphViewCurveLengthTextBackgroundColor": [0, 0, 0, 0.3],
   # Curve Length Text Color
   "glyphViewCurveLengthTextColor": [1, 1, 1, 1],
   # Curve Point Fill Color
   "glyphViewCurvePointsFill": [1, 0.3, 0.4, 1],
   # Curve Point Stroke Color
   "glyphViewCurvePointsStroke": [1, 1, 1, 1],
   # Decompose Component
   "glyphViewDecomponentKey": "",
   # Default Text box background color
   "glyphViewDefaultTextBoxBackgroundColor": [0, 0, 0, 0.3],
   # Disable deprecated draw notifications.
   "glyphViewDisableDrawNotifications": 0,
   # Dragging Guideline Stroke Color
   "glyphViewDragLinesColor": [0.6, 0.6, 0.6, 1],
   # Draw the layer color in the background as a tinted color
   "glyphViewDrawLayerBackgroundColor": 1,
   # Echo Path Stroke Color
   "glyphViewEchoStrokeColor": [0.6, 0.6, 0.6, 1],
   # Family Blues Color
   "glyphViewFamilyBluesColor": [0.5, 0.95, 0.95, 0.7],
   # Fill Color
   "glyphViewFillColor": [0, 0, 0, 1],
   # Flip glyph horizontal
   "glyphViewFlipHorizontalKey": "",
   # Flip glyph vertical
   "glyphViewFlipVerticalKey": "",
   # Vertical Metrics Stroke Color
   "glyphViewFontMetricsStrokeColor": [0.4, 0.4, 0.4, 0.5],
   # Global Guides Color
   "glyphViewGlobalGuidesColor": [0, 0, 1, 1],
   # Grid border around the glyph
   "glyphViewGridBorder": 300,
   # Grid Color
   "glyphViewGridColor": [0.5, 0.5, 0.5, 1],
   # Grid x increment
   "glyphViewGridx": 50,
   # Grid y increment
   "glyphViewGridy": 50,
   # BCP handles stroke width
   "glyphViewHandlesStrokeWidth": 1,
   # Image Info Text Background Color
   "glyphViewImageInfoBackgroundColor": [0, 0, 0, 0.3],
   # Image Info Text Color
   "glyphViewImageInfoColor": [0, 0, 0, 1],
   # Default increment
   "glyphViewIncrement": 1,
   # Jump To Glyph (jump back with the capitalizes key)
   "glyphViewJumpToGlyphKey": "j",
   # Layer Down
   "glyphViewLayerDownKey": "",
   # Layer Up
   "glyphViewLayerUpKey": "",
   # Local Guides Color
   "glyphViewLocalGuidesColor": [1, 0, 0, 1],
   # Magnetic Snap and Stroke Color
   "glyphViewMagneticColor": [1, 0.5, 0, 0.7],
   # Margins Background Color
   "glyphViewMarginColor": [0.5, 0.5, 0.5, 0.11],
   # Zero Width Margin Stroke Color
   "glyphViewMarginZeroWidthColor": [0, 0, 0, 0.1],
   # The maximum visible scale value in a glyph view.
   "glyphViewMaximumVisibleZoom": 50,
   # Measurements Secondary Line Color
   "glyphViewMeasurementsBackgroundColor": [0, 0, 0, 0.5],
   # Measurements Line Color
   "glyphViewMeasurementsForegroundColor": [0, 1, 0, 1],
   # Measurements Text Box Background Color
   "glyphViewMeasurementsTextBackgroundColor": [0, 0, 0, 0.3],
   # Measurements Text Color
   "glyphViewMeasurementsTextColor": [0, 0, 0, 1],
   # Use font metrics for measumerment
   "glyphViewMeasurementsUseFontMetrics": 0,
   # Toggle Measurements Use Font Metrics
   "glyphViewMeasurementsUseFontMetricsKey": "",
   # Use glyph metrics for measumerment
   "glyphViewMeasurementsUseGlyphMetrics": 1,
   # Toggle Measurements Use Glyph Metrics
   "glyphViewMeasurementsUseGlyphMetricsKey": "",
   # White background height font info attribute ('xHeight', 'ascender', 'capHeight', 'unitsPerEm') or 'height' to display the glyph height attribute
   "glyphViewMetricsHeightInfoAttribute": "unitsPerEm",
   # Vertical Metrics Titles Color
   "glyphViewMetricsTitlesColor": [0.6, 0.6, 0.6, 1],
   # Zero Metrics Color
   "glyphViewMetricsZeroWithStrokeColor": [0, 0, 1, 1],
   # The minimum visible scale value in a glyph view.
   "glyphViewMininumVisibleZoom": 0.02,
   # Negative Metrics Color
   "glyphViewNegativeMetricsFillColor": [1, 0.7, 0.7, 1],
   # Next Glyph
   "glyphViewNextGlyphKey": "",
   # Next glyph list
   "glyphViewNextGlyphSetting": 0,
   # Offcurve Stroke Color (Cubic Beziers, PostScript)
   "glyphViewOffCurveCubicPointsStroke": [1, 1, 1, 1],
   # Trailing Offcurve Stroke Color
   "glyphViewOffCurveMovePointsStroke": [0.6, 0.6, 0.6, 1],
   # Offcurve Fill Color
   "glyphViewOffCurvePointsFill": [1, 0.4, 0.5, 1],
   # Off curve point size
   "glyphViewOffCurvePointsSize": 3,
   # Offcurve Stroke Color (Quadratic Beziers, TrueType)
   "glyphViewOffCurveQuadPointsStroke": [0.8, 0.8, 0.8, 1],
   # On curve point size
   "glyphViewOncurvePointsSize": 4,
   # Start Point Arrow Color for an open contour
   "glyphViewOpenStartPointsArrowColor": [1, 0.3, 0.7, 1],
   # Other Blues Color
   "glyphViewOtherBluesColor": [0.5, 0.95, 0.95, 0.7],
   # Other Family Blues Color
   "glyphViewOtherFamilyBluesColor": [0.5, 0.95, 0.95, 0.7],
   # Outline errors offset
   "glyphViewOutlineErrors": 1,
   # Line Straightness Indicator Color
   "glyphViewOutlineErrorsColor": [0.2, 0.8, 1, 0.8],
   # Overlapping Indicator Color
   "glyphViewOutlineOverlappingErrorsColor": [0.2, 0.8, 1, 0.8],
   # Smooth Indicator Color
   "glyphViewOutlineSmoothErrorsColor": [0.2, 0.8, 1, 0.8],
   # Point Coordinate Background Color
   "glyphViewPointCoordinateBackgroundColor": [1, 1, 1, 0.3],
   # Point Coordinate Color
   "glyphViewPointCoordinateColor": [0.5, 0.5, 0.5, 0.75],
   # Point Index Text Background Color
   "glyphViewPointIndexBackgroundColor": [0, 0, 0, 0.4],
   # Point Index Text Color
   "glyphViewPointIndexColor": [1, 1, 1, 1],
   # Point Label Background Color
   "glyphViewPointLabelBackgroundColor": [0.34, 0.54, 0.92, 0.7],
   # Point Label Background Stroke Color
   "glyphViewPointLabelBackgroundStrokeColor": [1, 1, 1, 1],
   # Point Label Text Color
   "glyphViewPointLabelColor": [1, 1, 1, 1],
   # Preview Background Color
   "glyphViewPreviewBackgroundColor": [1, 1, 1, 1],
   # Preview Fill Color
   "glyphViewPreviewFillColor": [0, 0, 0, 1],
   # Previous Glyph
   "glyphViewPreviousGlyphKey": "",
   # Handle Stroke Color (Quadratic Beziers, TrueType)
   "glyphViewQuadraticHandlesStrokeColor": [0.8, 0.8, 0.8, 1],
   # Preview
   "glyphViewQuickPreviewKey": "`",
   # Remove Overlap
   "glyphViewRemoveOverlapKey": "",
   # Reverse Contours
   "glyphViewReverseContourKey": "",
   # Rotate glyph 45° clockwise
   "glyphViewRotate45clockWiseKey": "",
   # Rotate glyph 45° counterclockwise
   "glyphViewRotate45counterClockWiseKey": "",
   # Rotate glyph 90° clockwise
   "glyphViewRotate90clockWiseKey": "",
   # Rotate glyph 90° counterclockwise
   "glyphViewRotate90counterClockwiseKey": "",
   # Round coordinates
   "glyphViewRoundValues": 1,
   # Segment Index Text Background Color
   "glyphViewSegmentIndexBackgroundColor": [0, 0, 0, 0.4],
   # Segment Index Text Color
   "glyphViewSegmentIndexColor": [1, 1, 1, 1],
   # Selection Color
   "glyphViewSelectionColor": [0.65, 0.14, 0.36, 0.7],
   # Selection Marque Color
   "glyphViewSelectionMarqueColor": [0.5, 0.5, 0.5, 0.2],
   # Selection stroke width
   "glyphViewSelectionStrokeWidth": 2,
   # Set Starting Point
   "glyphViewSetStartPointKey": "",
   # Shift increment
   "glyphViewShiftIncrement": 10,
   # Draw background for coordinates
   "glyphViewShouldDrawCoordinateBackground": 0,
   # Draw dragging lines
   "glyphViewShouldDrawDragLines": 1,
   # Use offset coordinates for display
   "glyphViewShouldOffsetPointCoordinates": 0,
   # Preserve curve on deleting point
   "glyphViewShouldPreserveCurveWhenDeleteSinglePoint": 1,
   # Use slanted point coordinates for display
   "glyphViewShouldSlantPointCoordinates": 0,
   # Use 45 degree contrain
   "glyphViewShouldUse45Contrain": 1,
   # Use italic angle for display
   "glyphViewShouldUseItalicAngleForDisplay": 1,
   # Toggle Show Anchor Indexes
   "glyphViewShowAnchorIndexesKey": "",
   # Toggle Show Anchors
   "glyphViewShowAnchorsKey": "",
   # Toggle Bitmap
   "glyphViewShowBitmapKey": "",
   # Toggle Show Component Indexes
   "glyphViewShowComponentIndexesKey": "",
   # Toggle Show Component Info
   "glyphViewShowComponentInfoKey": "",
   # Toggle Show Contour Indexes
   "glyphViewShowContourIndexesKey": "",
   # Show point coordinates when contour count is smaller than
   "glyphViewShowCoordindatesLimit": 99,
   # Toggle Show Curve Length
   "glyphViewShowCurveLengthKey": "",
   # Toggle Show Fill
   "glyphViewShowFillKey": "",
   # Toggle Grid
   "glyphViewShowGridKey": "",
   # Toggle Guides
   "glyphViewShowGuidesKey": "",
   # Toggle Show Image Info
   "glyphViewShowImageInfoKey": "",
   # Toggle Show Image
   "glyphViewShowImageKey": "",
   # Toggle Show Labels
   "glyphViewShowLabelsKey": "",
   # Show Point Labels Sheet
   "glyphViewShowLabelsSheetKey": "",
   # Toggle Show Blues
   "glyphViewShowLineBluesKey": "",
   # Toggle Show Family Blues
   "glyphViewShowLineFamilyBluesKey": "",
   # Toggle Show Metrics
   "glyphViewShowMetricsKey": "",
   # Toggle Show Off Curve Points
   "glyphViewShowOffCurvePointsKey": "",
   # Toggle Show On Curve Points
   "glyphViewShowOnCurvePointsKey": "",
   # Toggle Show Outline Errors
   "glyphViewShowOutlineErrorsKey": "",
   # Toggle Show Point Coordinates
   "glyphViewShowPointCoordinatesKey": "",
   # Toggle Show Point Indexes
   "glyphViewShowPointIndexesKey": "",
   # Show point coordinates when selection count is smaller than
   "glyphViewShowSelectionCoordindatesLimit": 16,
   # Toggle Show Stroke
   "glyphViewShowStrokeKey": "",
   # Toggle Oncurve / Offcurve Selection
   "glyphViewSimpleTabKey": "",
   # Slicing Stroke and Intersections Color
   "glyphViewSliceColor": [1, 0.5, 0, 1],
   # Smooth Point Color
   "glyphViewSmoothPointStroke": [0, 0, 0, 0.7],
   # Start Point Arrow Color for a closed contour
   "glyphViewStartPointsArrowColor": [1, 0.3, 0.7, 1],
   # Stroke Color
   "glyphViewStrokeColor": [0, 0, 0, 1],
   # Stroke width
   "glyphViewStrokeWidth": 1,
   # Tangent Fill Color
   "glyphViewTangentPointsFill": [1, 0.3, 0.4, 1],
   # Tangent Stroke Color
   "glyphViewTangentPointsStroke": [1, 1, 1, 1],
   # Test Install the font
   "glyphViewTestInstall": "",
   # Toggle Lock Guides
   "glyphViewToggleLockGuidesKey": "",
   # Toggle Lock Image
   "glyphViewToggleLockImageKey": "",
   # Toggle Lock Sidebearings
   "glyphViewToggleSideBearingsKey": "",
   # Toggle Point Smoothness
   "glyphViewToggleSmoothKey": "s",
   # Trailing Handle Stroke Color
   "glyphViewTrailingHandlesStrokeColor": [0.6, 0.6, 0.6, 1],
   # Toggle Transform
   "glyphViewTransformKey": "t",
   # Transformation Color
   "glyphViewTransformationColor": [1, 0, 0, 1],
   # Zoom In
   "glyphViewZoomInKey": "z",
   # Zoom Out
   "glyphViewZoomOutKey": "x",
   # Indexes should follow contour
   "indexesShouldFollowContour": 1,
   # Maximum amount of glyphs to load all objects directly
   "loadFontGlyphLimit": 2000,
   # Mark colors
   "markColors": [
      [[1, 0, 0, 1], "red"],
      [[1, 1, 0, 1], "yellow"],
      [[0, 0, 1, 1], "blue"]
   ],
   # A list of menu items that needs a different short cut.
   "menuShortCuts": {
   },
   # The character set used for new documents
   "newDocumentCharset": "Latin-1",
   # New Documents should create template glyphs
   "newDocumentCreateDefaultset": 1,
   # Glyph width for new glyphs
   "newGlyphWidth": 500,
   # Normalizer should write modification times
   "normalizeWriteModTimes": 0,
   # Popup Output window when something is added
   "popupOutPutWindowOnWrite": 0,
   # Python Folder
   "pythonDir": "/Users/username/Library/Application Support/RoboFont/scripts",
   # Rename glyphs names in groups
   "removeGlyphNameInGroups": 0,
   # Rename glyph names in kerning
   "removeGlyphNameInKerning": 0,
   # On save remove unreferenced images
   "removeUnreferencedImages": 0,
   # Save FDK next to the UFO
   "saveFDKPartsNextToUFO": 0,
   # Draw with anti alias
   "shouldDrawWithAntialias": 0,
   # On save normalize the ufo file
   "shouldNormalizeOnSave": 0,
   # Use the embedded FDK
   "shouldUseEmbeddedFDK": 1,
   # Show full kerning group names
   "showFullKernGroupNames": 0,
   # Size of a Font Overview view in single window mode
   "singleWindowFontOverviewSplitViewSize": 500,
   # In single mode
   "singleWindowMode": 0,
   # Size of a Space Center view in single window mode
   "singleWindowSpaceCenterSize": 300,
   # Sort descriptors for new documents
   "sortDescriptors": [
      {
         "allowPseudoUnicode": 1,
         "type": "alphabetical"
      }, {
         "allowPseudoUnicode": 1,
         "type": "category"
      }, {
         "allowPseudoUnicode": 1,
         "type": "unicode"
      }, {
         "allowPseudoUnicode": 1,
         "type": "script"
      }, {
         "allowPseudoUnicode": 1,
         "type": "suffix"
      }, {
         "allowPseudoUnicode": 1,
         "type": "decompositionBase"
      }
   ],
   # Background Color
   "spaceCenterBackgroundColor": [1, 1, 1, 1],
   # Beam Color
   "spaceCenterBeamStrokeColor": [1, 0.4, 0, 1],
   # End position of the cut off line, can be a number or 'xHeight'.
   "spaceCenterCutOffEndPosition": "xHeight",
   # Start position of the cut off line.
   "spaceCenterCutOffStartPosition": 0,
   # Cut Off
   "spaceCenterDisplayModeCutOffKey": "",
   # Multi Line
   "spaceCenterDisplayModeMutliLineKey": "",
   # Water Fall
   "spaceCenterDisplayModeWaterFallKey": "",
   # Glyph Color
   "spaceCenterGlyphColor": [0, 0, 0, 1],
   # Selection Highlight Color
   "spaceCenterHightLightColor": [0.2, 0.2, 0.8, 0.1],
   # Maximum amount of history in space center combo box.
   "spaceCenterHistoryLimit": 10,
   # Space center input samples
   "spaceCenterInputSamples": [
       "abcdefghijklmnopqrstuvwxyz",
       "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
       "0123456789",
       "nn/? nn oo/? oo"
   ],
   # Input Highlight color
   "spaceCenterInputSelectionColor": [0, 0, 0, 0.08],
   # Margin Separator Color
   "spaceCenterMarginsColor": [0.5, 0.5, 0.5, 1],
   # Space center maximum glyph count for live updating
   "spaceCenterMaxGlyphsForLiveUpdate": 90,
   # Metrics Color
   "spaceCenterMetricsColor": [0.3, 0.3, 0.3, 1],
   # Space Center point size
   "spaceCenterPointSize": 150,
   # Space center point sizes
   "spaceCenterPointSizes": [
       "9", "10", "11", "12", "14", "16", "18", "20", "24", "30", "36", "48", "60", "72", "96", "144", "256", "512", "1024"
   ],
   # Jump to input
   "spaceCenterSelectInput": "",
   # Selection style in a space center, must be either 'square' or 'underline'.
   "spaceCenterSelectionStyle": "square",
   # Toggle Beam
   "spaceCenterToggleBeamKey": "",
   # Toggle Blues
   "spaceCenterToggleBluesKey": "",
   # Toggle Center
   "spaceCenterToggleCenter": "",
   # Toggle Family Blues
   "spaceCenterToggleFamilyBluesKey": "",
   # Toggle Fill
   "spaceCenterToggleFillKey": "",
   # Toggle Guides
   "spaceCenterToggleGuidesKey": "",
   # Toggle Images
   "spaceCenterToggleImage": "",
   # Toggle Inverse Color
   "spaceCenterToggleInverseColorKey": "",
   # Toggle Kerning
   "spaceCenterToggleKerningKey": "",
   # Toggle Space Matrix
   "spaceCenterToggleNumberInputView": "",
   # Toggle Right To Left
   "spaceCenterToggleRightToLeft": "",
   # Toggle Show Metrics
   "spaceCenterToggleShowLineKey": "",
   # Toggle Stroke
   "spaceCenterToggleStrokeKey": "",
   # Toggle Upside Down
   "spaceCenterToggleUpsideDown": "",
   # Space center matrix should use flexible cell width
   "spaceCenterUseFlexibleCellWidth": 0,
   # Space center water fall factors.
   "spaceCenterWaterFallSizes": [
      
   ],
   # Template glyphs replace character font name
   "templateGlyphFontName": "Helvetica",
   # Test install auto hint
   "testInstallAutoHint": 0,
   # Test install remove overlap
   "testInstallCheckOutlines": 1,
   # Test install decompose
   "testInstallDecompose": 0,
   # Name of the Font for displaying text
   "textFontName": "system",
   # Font size for displaying text
   "textFontSize": 9,
   # Enable validation while reading UFO files.
   "ufoLibReadValidate": 1,
   # Enable validation while writing UFO files
   "ufoLibWriteValidate": 1,
   # Warn when leaving RoboFont
   "warnWhenLeavingAppWithUnsavedDocuments": 0,
   # Log and print out the full stack where a warning happens
   "warningWithFullStack": 0,
   # Set a warning level, must be 'default', 'once', 'ignore', 'always'
   "warningsLevel": "once"
}
```