---
layout: page
title: Kern Center
tags:
  - kerning
---

* Table of Contents
{:toc}

The Kern Center is a sheet for editing the font’s kerning. It can be opened by clicking on the Kerning icon in the font’s toolbar.

{% image reference/workspace/font-overview/kern-center_groups.png %}

At the top of the sheet is a table with all kerning pairs in the font. The first two columns show the 1st and 2nd glyphs (or glyph groups) in the kerning pair, the third column shows the kerning value.

Below the table is a preview of all glyph combinations covered by the selected pair.


Some notes about kerning
------------------------

- Members of kerning pairs can be glyph names or group names.
- Kerning pairs are writing direction neutral. If text is written from left to right, the first glyph is the left-most glyph; if text is written from right to left, the first glyph is the right-most one.
- In UFO3, kerning group names must use a standard prefix: `public.kern1.` (1st glyph in kerning pair) or `public.kern2.` (2nd glyph in pair).

> - [UFO3 > kerning.plist](http://unifiedfontobject.org/versions/ufo3/kerning.plist/)
{: .seealso }


Options
-------

### Kerning table

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>select a kerning pair</td>
    <td>Update the input field and the kerning preview.</td>
  </tr>
  <tr>
    <td>enter</td>
    <td>Edit the kerning value.</td>
  </tr>
</table>

### Kern preview

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Select a kerning pair.</td>
  </tr>
  <tr>
    <td>click + drag (preview)</td>
    <td>Modify the kerning value interactively by moving the glyphs.</td>
  </tr>
  <tr>
    <td>arrow keys (left or right)</td>
    <td>Subtract or add 1 unit to the kerning value.</td>
  </tr>
  <tr>
    <td>⇧ + arrow left or right</td>
    <td>Subtract or add 10 units to the kerning value.</td>
  </tr>
  <tr>
    <td>⇧ + ⌘ + arrow keys</td>
    <td>Subtract or add 100 units to the kerning value.</td>
  </tr>
  <tr>
    <td>tab</td>
    <td>Select next kerning pair.</td>
  </tr>
  <tr>
    <td>⇧ + tab</td>
    <td>Select previous kerning pair.</td>
  </tr>
</table>

### Control Buttons

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>+</td>
    <td>Add kerning pair.</td>
  </tr>
  <tr>
    <td>-</td>
    <td>Remove kerning pair.</td>
  </tr>
  <tr>
    <td>Kerning</td>
    <td>Apply kerning to the font.</td>
  </tr>
  <tr>
    <td>Left To Right</td>
    <td>Toggle between left-to-right and right-to-left.</td>
  </tr>
  <tr>
    <td>Prefer Group Kerning</td>
    <td>If a kerning group is found for the edited glyph, it is used. Otherwise, the glyph is used.</td>
  </tr>
</table>

> - {% internallink "tutorials/kerning" %}
{: .seealso }

> Advanced users are advised to use MetricsMachine, a more powerful kerning tool than Kern Center. MetricsMachine is available as an [extension](http://extensionstore.robofont.com/extensions/metricsMachine/) for RoboFont 3 / UFO3, and as a [separate app](http://tools.typesupply.com/) for RoboFont 1 / UFO2.
>
> In RoboFont 1.8, if MetricsMachine is installed, clicking on the Kerning icon will send the font to MetricsMachine for editing. Use ⌥ + click to open the Kern Center instead.
{: .tip }
