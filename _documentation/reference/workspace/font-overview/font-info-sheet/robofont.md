---
layout: page
title: RoboFont
tags:
---

The *RoboFont* tab contains RoboFont-specific settings.

## Generate

The *Generate* section contains settings used by RoboFont when generating fonts.

{% image reference/workspace/font-overview/font-info-sheet/font-info_robofont.png %}

<table>
<tr>
<th width='35%'>option</th>
<th width='65%'>description</th>
</tr>
<tr>
<td>Decompose</td>
<td>Decompose all components when generating a font.</td>
</tr>
<tr>
<td>Remove Overlap</td>
<td>Remove overlaps in all glyphs when generating a font.</td>
</tr>
<tr>
<td>Auto Hint</td>
<td>Auto-hint the font with the Adobe FDK autohinter (PostScript fonts) or ttfautohint (TrueType fonts, only if it’s installed in the system) using data from the PostScript hinting info.</td>
</tr>
<tr>
<td>Release Mode</td>
<td>Set the Adobe FDK in <em>release mode</em> when generating the font. This will turn on subroutinization (makes final OTF file smaller), applies your desired glyph order, and removes the word <em>Development</em> from the font’s version string.</td>
</tr>
<tr>
<td>Format</td>
<td>Default output format when generating a font:
<ul>
  <li><code>OTF</code> – OpenType CFF (PostScript)</li>
  <li><code>TTF</code> – OpenType TT (TrueType)</li>
  <li><code>PFA</code> – PostScript Type 1</li>
</ul>
</td>
</tr>
<tr>
<td>Italic Slant Offset</td>
<td>Shift all glyphs horizontally to the left when generating fonts. The number must have a negative value. This is used mainly when drawing italics with slanted metrics.</td>
</tr>
<tr>
<td>Spline Conversion</td>
<td>When changing between Cubic (PostScript) and Quadratic (TrueType) curves, the conversion can either preserve the number of points or preserve the curves (adding points when necessary).</td>
</tr>
<tr>
<td>Add Dummy DSIG table</td>
<td>Add a dummy digital signature table while generating a binary font file.</td>
</tr>
<tr>
<td>Skip Glyphs</td>
<td markdown='1'>
A space separated list of glyph names representing glyphs that should not be included in the generated fonts. See the [UFO3 specification][publicskipexportglyphs].
</td>
</tr>
<tr>
<td>Production names</td>
<td markdown='1'>
A table mapping production glyph names to final PostScript glyph names. See the [UFO3 specification][publicpostscriptnames] and {% internallink "tutorials/using-production-names" %}.
</td>
</tr>
</table>

[publicskipexportglyphs]: http://unifiedfontobject.org/versions/ufo3/lib.plist/#publicskipexportglyphs'
[publicpostscriptnames]: http://unifiedfontobject.org/versions/ufo3/lib.plist/#publicpostscriptnames
