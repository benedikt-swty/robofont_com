---
layout: page
title: Editing Tool
tags:
  - contours
---

* Table of Contents
{:toc}

{% image reference/workspace/toolbarToolsArrow.png %}

The *Editing Tool* is an interactive tool to edit visual glyph data such as contours, anchors, components, guidelines, images, etc.

To activate the tool, you can click on the arrow icon in the toolbar of the {% internallink 'workspace/glyph-editor' %} or select "Editing Tool" in the {% internallink "reference/workspace/application-menu#glyph" text="Glyph application menu" %}.


Actions
-------

{% video tutorials/drawingGlyphs_edit-tool.mp4 %}

### Point Selection

<table class="table-35-65">
  <tr>
    <th>action</th>
    <th>description</th>
  </tr>
  <tr>
    <td>click (no selection)</td>
    <td>Deselect all.</td>
  </tr>
  <tr>
    <td>click + drag (no selection)</td>
    <td>Select all points inside the selection marquee.</td>
  </tr>
  <tr>
    <td>click + drag (no selection) + ⌥</td>
    <td>Select only the off-curve points inside the selection marquee.</td>
  </tr>
  <tr>
    <td>click + drag (no selection) + ⌥ + ⌘</td>
    <td>Select all the anchors inside the selection marquee.</td>
  </tr>
  <tr>
    <td>click on a point</td>
    <td>Select the point.</td>
  </tr>
  <tr>
    <td>⇧ + click</td>
    <td>Add or remove from current selection.</td>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>Move selected points.</td>
  </tr>
  <tr>
    <td>⇧ + click + drag</td>
    <td>Move selected points on x, y or 45° axis.</td>
  </tr>
  <tr>
    <td>⌥ + click + drag</td>
    <td>
      <dl>
        <dt>If click over cubic on-curve point:</dt>
        <dd>The point will be turned non-smooth and only the on-curve point is moved.</dd>
        <dt>If click over quadratics off-curve point:</dt>
        <dd>Adds extra off-curve point to the segment</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>⌃ + click + drag</td>
    <td>Copies the selection while dragging.</td>
  </tr>
  <tr>
    <td>⌥ + click + drag (single BCP selection)</td>
    <td>BCP becomes non-smooth.</td>
  </tr>
  <tr>
    <td>⌘ + ⌥ + click + drag (single BCP selection)</td>
    <td>BCP becomes smooth, and the BCP value is mirrored on the other BCP (if there is one).</td>
  </tr>
  <tr>
    <td>⌘ + drag smooth point</td>
    <td>
      <dl>
        <dt>If selection is on-curve point:</dt>
        <dd>Point will be moved between the handles.</dd>
        <dt>If selection is off-curve point:</dt>
        <dd>Point will be moved in the same direction as the handle.</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>join / close contour</td>
    <td>If a contour is open, and first or last on-curve point is dragged on top of another first or last on-curve point, the contours are joined or closed.</td>
  </tr>
  <tr>
    <td>tab</td>
    <td>Jump to the next on-curve point.</td>
  </tr>
</table>

> Use ⇪ (Caps Lock) + ⌘ + arrow keys to move a smooth point between its handles using the keyboard instead of the mouse. <!-- See [this post](http://forum.robofont.com/topic/572/moving-on-curve-independent-of-off-curves-using-arrows-maintaining-smooth) in the RoboFont Forum. -->
{: .tip }

> If the option *Use Shift 45° constrain* is selected in the {% internallink 'workspace/preferences-window/glyph-view' %}, angles will be constrained to multiples of 45° degrees instead of 90°.
{: .note }

### Segment Selection

<table class="table-35-65">
  <tr>
    <th>action</th>
    <th>description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Select segment.</td>
  </tr>
  <tr>
    <td>⇧ + click</td>
    <td>Add segment to selection.</td>
  </tr>
  <tr>
    <td>click + drag (single segment selection)</td>
    <td>Move the selected segments around.</td>
  </tr>
  <tr>
    <td>⌥ + click + drag (single segment selection)</td>
    <td>
      <dl>
        <dt>For line segment:</dt>
        <dd>Create curve segment (adds two BCPs).</dd>
        <dt>For curve segment:</dt>
        <dd>Drag the handles around. (This will un-smooth on-curve points.)</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>⌘ + click drag (single segment selection)</td>
    <td>
      <dl>
        <dt>For line segments:</dt>
        <dd>Move the selected segment around.</dd>
        <dt>For curve segments:</dt>
        <dd>BCPs follow the handles direction.</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>⇧ + click + drag (single segment selection)</td>
    <td>Constrain translation angle to multiples of 90° (or 45°). Works also while ⌥ and/or ⌘ are down.</td>
  </tr>
  <tr>
    <td>⌃ + click + drag</td>
    <td>Copy the selected segment while dragging.</td>
  </tr>
  <tr>
    <td>⌘ + ⌥ (no selection)</td>
    <td>Add point to segment.</td>
  </tr>
</table>

### Contour Selection

| action | description |
| --- | --- |
| double click | Select contour. |
| click + drag (in/on selection) | Move the selection. |
| ⇧ + click + drag (in/on selection) | Constrain translation angle to multiples of 90° (or 45°). |
{: .table-35-65 }

### Component Selection

| action | description |
| --- | --- |
| click | Select the component. |
| ⇧ + click | Toggles the component selection. |
| drag | Move the components around. |
| ⇧ + drag | Constrain translation angle to multiples of 90° (or 45°). |
| triple click (in the component) | Go to the base glyph for editing. |
{: .table-35-65 }

### Copy & Paste

<table class="table-35-65">
  <tr>
    <th>action</th>
    <th>description</th>
  </tr>
  <tr>
    <td>copy</td>
    <td>
      <dl>
        <dt>in Glyph View:</dt>
        <dd>Copy the current glyph’s selection to clipboard.</dd>
        <dt>in Font Overview:</dt>
        <dd>Copy the selected glyph to clipboard.</dd>
      </dl>
    </td>
  </tr>
  <tr>
    <td>paste</td>
    <td>Paste clipboard contents into the current glyph.</td>
  </tr>
</table>

Copy & Paste from external sources like Adobe Illustrator or other vector drawing applications happens from a PDF paste board.

Crosshair cursor
----------------

The Editing Tool shows a crosshair cursor while moving a selection – this helps to visually align the selection bounds to another shape or guideline.

{% video reference/workspace/glyph-editor/tools/editing-tool_crosshair-cursor.mp4 %}

> This feature can be turned on/off in the {% internallink 'workspace/preferences-window/glyph-view' %} under the option *Draw dragging cross lines*.
{: .note }


Contextual menus
----------------

Contextual menus can be opened using a right-click. Depending on the current selection and on where you click, a different menu will be shown.

- [no selection](../../#contextual-menu)
- [contour selection](../../contours#contextual-menu)
- [component](../../components#contextual-menu)
- [anchor](../../anchors#contextual-menu)
- [guideline](../../guidelines#contextual-menu)
- [image](../../images#contextual-menu)

> - {% internallink 'tutorials/drawing-glyphs' %}
> - {% internallink 'topics/drawing-environment' %}
{: .seealso }
