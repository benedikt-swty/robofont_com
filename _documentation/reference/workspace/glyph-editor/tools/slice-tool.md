---
layout: page
title: Slice Tool
tags:
  - contours
---

* Table of Contents
{:toc}

{% image reference/workspace/toolbarToolsSlice.png %}

The *Slice* tool is an interactive tool to add points to contours or cut them apart.

To activate the tool, you can click on the knife icon in the toolbar of the Glyph Editor or select the "Slice Tool" in the {% internallink "reference/workspace/application-menu#glyph" text="Glyph application menu" %}.

{% image reference/workspace/glyph-editor/tools/slice-line.png %}

Click and drag to define a slicing line. When the line has 2 intersections with the same contour, it will cut the contour into separate shapes. Otherwise, it will just add points at the intersections.

> It’s also possible to slice contours using {% internallink "glyph-editor/guidelines" %}.
{: .seealso }


Actions
-------

{% video tutorials/drawingGlyphs_slice.mp4 %}

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Define the starting point of a slicing line.</td>
  </tr>
  <tr>
    <td>drag</td>
    <td>Define the end point of a slicing line.</td>
  </tr>
  <tr>
    <td>⇧ + drag</td>
    <td>Constrain the angle of the slicing line to multiples of 90° (or 45°).</td>
  </tr>
  <tr>
    <td>mouse up</td>
    <td>Slice the glyph with the slicing line.</td>
  </tr>
</table>

> If the option *Use Shift 45° constrain* is selected in the {% internallink 'workspace/preferences-window/glyph-view' %}, angles will be constrained to multiples of 45° degrees instead of 90°.
{: .note }

> - {% internallink 'tutorials/drawing-glyphs' %}
> - {% internallink 'topics/drawing-environment' %}
{: .seealso }
