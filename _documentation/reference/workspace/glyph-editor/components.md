---
layout: page
title: Components
tags:
---

* Table of Contents
{:toc}


Definition
----------

Components are part of the outline descriptions of a glyph. They are different from regular contours in that they are *references* to other glyphs in the font.

{% image reference/workspace/glyph-editor/components.png caption="contour (slash) and component (o)" %}

Components allow us to reuse glyph shapes without duplicating point data. The glyph which is referenced by a component is called the *base glyph*. When the base glyph is changed, all components which reference it are updated automatically.

Components can only reference glyphs within the same layer. The base glyph may contain components too, but they *must not create a circular reference* back to the parent glyph. Multiple levels of component references are allowed.

Components are particularly useful for {% internallink "how-tos/building-accented-glyphs" %}.

> In RoboFont 1 / UFO 2, components in any layer refer to glyphs in the *foreground* layer.
{: .note }


Adding components
-----------------

Components are added with the *Add Components sheet*. To open this sheet, choose *Add Component* from the Glyph Editor’s [contextual menu](../#contextual-menu).

{% image reference/workspace/glyph-editor/components_add.png %}

<!-- Typing a glyph name in the text input field will narrow down the list of glyph names below.

4. Click on the *Add Component* button to insert the selected glyph into the current glyph as a component.
 -->

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>typing…</td>
    <td>Filter the list of glyph names. Choose between searching only at the start or anywhere in the glyph names.</td>
  </tr>
  <tr>
    <td>tab</td>
    <td>Jumps between the two search modes.</td>
  </tr>
  <tr>
    <td>up/down arrow keys (↑↓)</td>
    <td>Move up or down in the selection.</td>
  </tr>
  <tr>
    <td>Add Components or Enter</td>
    <td>Add selected glyph as component.</td>
  </tr>
  <tr>
    <td>Copy Metrics</td>
    <td>Copy the component’s metrics to the current glyph.</td>
  </tr>
</table>


Removing components
-------------------

To remove one or more components, select them with the {% internallink 'workspace/glyph-editor/tools/editing-tool' %} and hit the backspace key.


Components Inspector
--------------------

Component properties can be edited in the {% internallink "workspace/inspector#components" text="Components" %} section of the Inspector.

{% image reference/workspace/glyph-editor/inspector_components.png %}


Contextual menu
---------------

The Glyph Editor’s [contextual menu](../#contextual-menu) includes a submenu with actions related to components.

To open the contextual menu, right-click in the Glyph View using the {% internallink 'glyph-editor/tools/editing-tool' %}.

{% image reference/workspace/glyph-editor/glyph-editor_contextual-menu_component.png %}

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Go to…</td>
    <td>Switch to the selected base glyph.</td>
  </tr>
  <tr>
    <td>Decompose Selected</td>
    <td>Decompose the selected components.</td>
  </tr>
  <tr>
    <td>Decompose All</td>
    <td>Decompose all components in the current glyph.</td>
  </tr>
</table>


Editing components
------------------

### Transformations

Components can be freely {% internallink "glyph-editor/transform" text="transformed" %} (translated, scaled, rotated and skewed).

To transform selected components interactively, activate Transform mode by pressing ⌘ + T or by choosing *Glyph > Transform* from the main application menu.

{% image reference/workspace/glyph-editor/glyph-editor_components-transform.png %}

The transformation matrix of each component can also be edited using the [Components Inspector](../../inspector#components).

### Editing base glyphs

To edit the base glyph of a component, use the [contextual menu](#contextual-menu) to jump to that glyph and edit it. These changes will reflect in *all* components of that glyph in the font.

### Decomposing components

To *decompose* a component means to replace the component with the actual contours of the base glyph, so that its shape can be edited like any other contour. Once a component is decomposed, the reference to the base glyph is lost, and any changes to it will no longer reflect in the glyph that contained the component.

<div class='row'>
<div class='col'>
{% image reference/workspace/glyph-editor/glyph-editor_components-decompose-after.png %}
</div>
<div class='col'>
{% image reference/workspace/glyph-editor/glyph-editor_components-decompose-before.png %}
</div>
</div>

To decompose one or more selected components, open the [contextual menu](#contextual-menu) and choose *Components > Decompose Selected*, or use *Decompose All* to decompose all components.

<!--
Accented glyphs
---------------

The main use case for components is building accented glyphs. For example, an accented character `/aacute` is typically built with components to `/a` (base) and `/acute` (accent).

> - {% internallink "how-tos/building-accented-glyphs" %}
{: .seealso }
-->

- - -

> - [UFO3 Specification > Component](http://unifiedfontobject.org/versions/ufo3/glyphs/glif/#component)
> - {% internallink 'reference/api/fontParts/rcomponent' text='FontParts API > RComponent' %}
{: .seealso }
