---
layout: page
title: Contours
tags:
  - contours
---

* Table of Contents
{:toc}


Description
-----------

Contours are visual descriptions of glyph shapes using bezier curves.

Bezier curves were introduced into type design in the 1980s as two competing flavors, PostScript (Adobe) and TrueType (Microsoft/Apple). The PostScript format used cubic bezier curves, while the TrueType used quadratic ones. The formats evolved and were merged into the OpenType format, but the distiction remains between OpenType-CFF (cubic) and OpenType-TTF (quadratic) fonts.

The Ikarus format which existed before that was based on circle segments.

{% image tutorials/glyph-editor.png %}

<!-- Segment types -->

Contours can be made of line and curve segments.

Line segments
: are made of two on-curve points

Curve segments
: are made of two on-curve points and off-curve points

<!-- Open and close contours -->

Contours can be open or closed. Open contours are allowed in UFO, but not in OpenType.

### Cubic and quadratic beziers

RoboFont uses cubic bezier curves by default. This is the type of contours which is most familiar to designers, since it’s used by vector design applications. Cubic beziers are part of Adobe’s PostScript page description language, and are used by OpenType-CFF fonts.

Since version 4.0, RoboFont offers full support for quadratic bezier curves. This allows for a finer control over outlines meant to be exported as TrueType fonts.

<div class='row'>
  <div class='col' style='vertical-align:top;'>
    {% image reference/workspace/glyph-editor/display-cubics.png caption="cubic curves" %}
  </div>
  <div class='col' style='vertical-align:top;'>
    {% image reference/workspace/glyph-editor/display-quadratics.png caption="quadratic curves" %}
  </div>
</div>

> - {% internallink "tutorials/drawing-with-quadratic-curves" %}
{: .seealso }

### Point smoothness

Bezier points can be *smooth* or *unsmooth*.

smooth point
: segments are continuous

unsmooth point
: segments are at an angle

Point smoothness can be toggled on / off by double clicking the point.

Smooth points retain continuity between segments when its handles are moved.

The angle of handle is locked to the angle of the opposing line segment of or to the angle of the handle of the oppposing curve segment.

### Point representations

Points are represented by different icons depending on their smoothness and the type of segments they connect:

<div class='row'>
  <div class='col'>
  <img style='display:inline-block;vertical-align:top;' src='{{site.baseurl}}/images/reference/workspace/glyph-editor_contours-point-types.png' />
  </div>
  <div class='col' markdown=1>
  Square
  : Unsmooth connection between line and curve segments, or two line segments

  Triangle
  : Smooth connection between line segment and curve segment

  Circle without stroke
  : Unsmooth connection between two curve segments, handles can move independently

  Circle with black stroke
  : Smooth connection between two curve segments

  Connected control points
  : Quadratic bezier control point. Quadratic beziers allow for an infinite amount of control points

  Control points only connected to anchor
  : Cubic bezier control points. Cubic beziers only allow for two control points, each one connected to an anchor
  </div>
</div>


Creating and editing contours
-----------------------------

Contours are usually created using the {% internallink 'workspace/glyph-editor/tools/bezier-tool' %} and edited using the {% internallink 'workspace/glyph-editor/tools/editing-tool' %}.

> It’s also possible to draw inside a glyph with code – see {% internallink "tutorials/scripts-glyph#drawing-inside-a-glyph-with-a-pen" text='this example' %}.
{: .seealso }


Contours Inspector
------------------

Contour and point properties can be edited in the {% internallink "inspector#points" text="Points" %} section of the Inspector.

{% image reference/workspace/glyph-editor/inspector_points.png %}


Contextual menu
---------------

<!-- The Glyph Editor’s contextual menu offers several actions to modify contours. -->

The contextual menu can be opened with a right-click using the {% internallink 'glyph-editor/tools/editing-tool' %}. A different set of options is displayed depending on the context:

<div class='row'>
<div class='col' style='vertical-align:top;'>
{% image reference/workspace/glyph-editor/glyph-editor_contextual-menu.png caption='no selection' %}
</div>
<div class='col' style='vertical-align:top;'>
{% image reference/workspace/glyph-editor/glyph-editor_contextual-menu-points.png caption='contour / multiple points' %}
</div>
<div class='col' style='vertical-align:top;'>
{% image reference/workspace/glyph-editor/glyph-editor_contextual-menu-point.png caption='single point' %}
</div>
</div>

### Contour / Multiple points selection actions

<table>
  <thead>
    <tr>
      <th width="35%">action</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Remove Overlap</td>
      <td>Remove overlaps in the selected contours</td>
    </tr>
    <tr>
      <td>Add Extreme Points</td>
      <td>Add extreme points to the selected contours</td>
    </tr>
    <tr>
      <td>Close Open Contours</td>
      <td>Connects two start or end points included in the selection, giving priority to start and end points of the same open contour. The selection might include non-end points, they will be ignored</td>
    </tr>
    <tr>
      <td>Reverse Contours</td>
      <td>Reverse selected contours</td>
    </tr>
    <tr>
      <td>Convert Contour to Cubic</td>
      <td>Convert selected contour or segments to cubic</td>
    </tr>
    <tr>
      <td>Convert Contour to Quadratic</td>
      <td>Convert selected contour or segments to quadratic</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Break Contours</td>
      <td>If an entier contour is selected, the contour is deleted. Otherwise only the selected segments are deleted</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Edit Labels</td>
      <td>Edits labels to the selected points</td>
    </tr>
  </tbody>
</table>


### Single point actions
<table>
  <thead>
    <tr>
      <th width="35%">action</th>
      <th width="65%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Break Contour</td>
      <td>Breaks contour at the selected point</td>
    </tr>
    <tr>
      <td>Reverse Contour</td>
      <td>Reverse contour</td>
    </tr>
    <tr>
      <td>Set Start Point</td>
      <td>Set the selected point as starting point</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Add Horizontal Guideline</td>
      <td>Adds a horizontal guidelines lying on the selected point</td>
    </tr>
  </tbody>
</table>



- - -

> - [UFO3 Specification > glyf > Point](http://unifiedfontobject.org/versions/ufo3/glyphs/glif/#point)
> - FontParts API > {% internallink 'reference/api/fontParts/rcontour' %} / {% internallink 'reference/api/fontParts/rpoint' %} / {% internallink 'reference/api/fontParts/rbpoint' %} / {% internallink 'reference/api/fontParts/rsegment' %}
{: .seealso }
