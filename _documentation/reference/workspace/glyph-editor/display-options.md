---
layout: page
title: Display options
tags:
---

* Table of Contents
{:toc}

The Glyph Editor includes two popup menus with display options at the bottom left:

{% image reference/workspace/glyph-editor/glyph-editor_display-options-buttons.png %}


Display scale
-------------

The first menu can be used to set the scale factor of the current Glyph View.

{% image reference/workspace/glyph-editor/glyph-editor_scale-options.png %}

<table>
  <tr>
    <th width="30%">title</th>
    <th width="70%">description</th>
  </tr>
  <tr>
    <td>Fit</td>
    <td>Fit the glyph to the current size of the Glyph View.</td>
  </tr>
  <tr>
    <td>percentage</td>
    <td>Scaling factor.</td>
  </tr>
</table>


Display layers
--------------

The second menu makes it possible to control the visibility of each layer of information displayed by the Glyph Editor.

{% image reference/workspace/glyph-editor_display-options.png %}

<table>
  <tr>
    <th width="30%">label</th>
    <th width="70%">description</th>
  </tr>
  <tr>
    <td>Fill</td>
    <td>Toggle display of the glyph’s fill.</td>
  </tr>
  <tr>
    <td>Stroke</td>
    <td>Toggle display of the glyph’s stroke.</td>
  </tr>
  <tr>
    <td>Metrics</td>
    <td>Toggle display of glyph metrics (advance width, left and right margins) and vertical metrics (baseline, x-height, descender, ascender, cap-height).</td>
  </tr>
  <tr>
    <td>Metrics Titles</td>
    <td>Toggle display of labels for metrics.</td>
  </tr>
  <tr>
    <td>On Curve Points</td>
    <td>Toggle display of on-curve points.</td>
  </tr>
  <tr>
    <td>Off Curve Points</td>
    <td>Toggle display of off-curve points.</td>
  </tr>
  <tr>
    <td>Point Coordinates</td>
    <td>Toggle display of point coordinates.</td>
  </tr>
  <tr>
    <td>Point Indexes</td>
    <td>Toggle display of point indexes.</td>
  </tr>
  <tr>
    <td>Segment Indexes</td>
    <td>Toggle display of segment indexes.</td>
  </tr>
  <tr>
    <td>Contour Indexes</td>
    <td>Toggle display of contour indexes.</td>
  </tr>
  <tr>
    <td>Component Indexes</td>
    <td>Toggle display of component indexes.</td>
  </tr>
  <tr>
    <td>Component Info</td>
    <td>Toggle display of component info (name of the base glyph).</td>
  </tr>
  <tr>
    <td>Anchors</td>
    <td>Toggle display of anchors.</td>
  </tr>
  <tr>
    <td>Anchor Indexes</td>
    <td>Toggle display of anchor indexes.</td>
  </tr>
  <tr>
    <td>Labels</td>
    <td>Toggle display of point labels.</td>
  </tr>
  <tr>
    <td>Image Info</td>
    <td>Toggle display of image info (file name, offset).</td>
  </tr>
  <tr>
    <td>Curve Length</td>
    <td>Toggle display of length of each curve segment.</td>
  </tr>
  <tr>
    <td>Outline Errors</td>
    <td>Toggle display of outline errors.</td>
  </tr>
  <tr>
    <td>Blues</td>
    <td>Toggle display of blue zones.</td>
  </tr>
  <tr>
    <td>Family Blues</td>
    <td>Toggle display of family blue zones.</td>
  </tr>
  <tr>
    <td>Rulers</td>
    <td>Toggle display of rulers.</td>
  </tr>
  <tr>
    <td>Guidelines</td>
    <td>Toggle display of guidelines.</td>
  </tr>
  <tr>
    <td>Measurement Info</td>
    <td>Toggle display of complementary measurement info (x/y axes, angle, x/y projections).</td>
  </tr>
  <tr>
    <td>Grid</td>
    <td>Toggle display of the grid.</td>
  </tr>
  <tr>
    <td>Bitmap</td>
    <td>Toggle display of the glyph’s bitmap representation.</td>
  </tr>
</table>

> - The grid size can be configured in the {% internallink 'preferences-window/glyph-view' %}.
> - The bitmap size is taken from the grid size settings.
{: .note }
