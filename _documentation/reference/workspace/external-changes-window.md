---
layout: page
title: External Changes window
tags:
  - UFO
---

* Table of Contents
{:toc}


The *External Changes* window is a modal dialog to selectively integrate external changes to UFOs which are currently open in RoboFont.


Update found dialog
-------------------

Whenever data in an open UFO font is changed by another application, an *Update found* dialog appears – prompting the user to choose between reviewing, rejecting or accepting the changes.

{% image reference/workspace/revert-to-saved_update-found.png %}

<table>
  <tr>
    <th width='20%'>action</th>
    <th width='80%'>description</th>
  </tr>
  <tr>
    <td>Review</td>
    <td>Open the <em>External Changes</em> window to review and reject/accept each change individually.</td>
  </tr>
  <tr>
    <td>Reject</td>
    <td>Keep the current font state and reject all external changes.</td>
  </tr>
  <tr>
    <td>Update</td>
    <td>Accept all external changes and update the current font.</td>
  </tr>
</table>


External Changes window
-----------------------

The *External Changes* window shows an overview of all changes in open fonts.

{% image reference/workspace/revert-to-saved.png %}

Select a font to view a list of all changed glyphs in that UFO, grouped by layer.

<table>
  <thead>
    <tr>
      <th width='35%'>action</th>
      <th width='65%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>check / uncheck glyph</td>
      <td>accept or reject the changes to that glyph</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Reject</td>
      <td>Abort the review process and reject all changes.</td>
    </tr>
    <tr>
      <td>Update…</td>
      <td>Accept external changes in the selected glyphs.</td>
    </tr>
  </tbody>
</table>


Visualize changes
-----------------

The *External Changes* window includes a diff popover which shows the exact changes in the `.glif` data for each glyph. It can be opened by double-clicking a glyph name.

{% image reference/workspace/revert-to-saved_diff-view.png %}

The diff view shows the underlying XML data for the glyph. Green lines show the newly added external data, red lines show the current glyph data which will be replaced.


> - {% internallink 'topics/the-ufo-format' %}
{: .seealso }
