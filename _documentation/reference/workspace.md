---
layout: page
title: Workspace
tree:
  - application-menu
  - window-modes
  - font-overview
  - glyph-editor
  - inspector
  - space-center
  - preferences-window
  - scripting-window
  - output-window
  - extension-builder
  - package-installer
  - keyboard-shortcuts
  - preferences-editor
  - external-changes-window
  - about-window
  - license-window
  - safe-mode
---

<style>
@media (min-width: 480px) {
  #workspace-index {
    column-count: 2;
  }
  #workspace-index li {
    /* NOT WORKING IN FIREFOX (YET) */
    break-inside: avoid-column;
  }
}
</style>

*What RoboFont looks like. A description of its menus, windows, panels etc.*

<div id='workspace-index'>
{% tree page.url levels=3 %}
</div>
