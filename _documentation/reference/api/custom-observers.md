---
layout: page
title: RoboFont observers
tags:
  - observers
---


Observers
---------

```python
from mojo.events import addObserver, removeObserver
```

`addObserver(observer, method, event)`
: Adds an observer for an event to the observer method.

`removeObserver(observer, event)`
: Removes the observer for an event.


Events
------

Any observer can subscribe to these events. The callback receives a dictionary with additional objects related to the sent event.

All notification dictionaries contain the following keys:

`glyph`
: the current/edited/active glyph

`tool`
: the current tool

`view`
: the current glyph view

----

<div class="api-docs">
{% for item in site.data.API.observersAPI %}
{% include showapi item=item %}
{% endfor %}
</div>
