---
layout: page
title: Extension Item Format
tags:
  - extensions
  - Mechanic
---

* Table of Contents
{:toc}


Extension Item Specification
----------------------------

An *Extension Item* is a simple dictionary with data about an extension.

The following key/value pairs are supported:

<table>
<tr>
  <th width='35%'>key</th>
  <th>value</th>
</tr>
{% for data in site.data.extensionItemSpec.extensionItem %}
<tr>
  <td rowspan='3'>
    <code class='highlighter-rouge'>{{ data.key }}</code>
  </td>
  <td>{{ data.description | markdownify }}</td>
</tr>
<tr>
  <td><code>{{ data.value }}</code></td>
</tr>
<tr>
  <td class="{{ data.required | slugify }}">{{ data.required | markdownify  }}</td>
</tr>
{% endfor %}
</table>

### Extension icon

If available, extension icons are displayed in the extensions overview in Mechanic 2.

Some recommendations for icon images:

file path
: save the image outside of the `.roboFontExt` package

file name
: `<extensionName>MechanicIcon.png`

image format
: `.png` with transparent background

image size
: 512 × 512 px


Usage
-----

Extension items can be distributed by themselves or collected into *extension streams*.

The extension item format is not tied to a particular data format:

- single extension items are stored in [yaml] format
- extension streams are served as [json] data

[yaml]: http://en.wikipedia.org/wiki/YAML
[json]: http://en.wikipedia.org/wiki/JSON

### Extension items

Here is an example extension item in `yaml` format for the [Boilerplate Extension]:

```yaml
extensionName: myExtension
description:   A boilerplate extension which serves as starting point for creating your own extensions.
repository:    http://github.com/robodocs/rf-extension-boilerplate
extensionPath: build/myExtension.roboFontExt
developer:     RoboDocs
developerURL:  http://github.com/roboDocs
tags:          [ demo, test ]
infoPath:      http://github.com/roboDocs/rf-extension-boilerplate/blob/master/build/myExtension.roboFontExt/info.plist
icon:          http://github.com/roboDocs/rf-extension-boilerplate/blob/master/build/myExtension.roboFontExt/resources/icon.png
zipPath:       http://github.com/roboDocs/rf-extension-boilerplate/archive/master.zip
```

Extension items may be stored using different file extensions, depending on the desired distribution channel:

`<extensionName>.yml`
: for registration in the Mechanic 2 extension stream

`<extensionName>.mechanic`
: for distribution as single extension item

> - {% internallink 'extensions/publishing-extensions' %}
{: .seealso }

[Boilerplate Extension]: http://github.com/roboDocs/rf-extension-boilerplate

### Extension streams

An *extension stream* is a `.json` file containing a list of extension items and a date (when the list was last updated).

Here is an example extension stream:

```json
{
  "lastUpdate" : "2018-06-08 21:21",
  "extensions" : [
    {
      "extensionName" : "myExtension",
      "description"   : "A boilerplate extension which serves as starting point for creating your own extensions.",
      "repository"    : "http://github.com/robodocs/rf-extension-boilerplate",
      "extensionPath" : "build/myExtension.roboFontExt",
      "developer"     : "RoboDocs",
      "developerURL"  : "http://github.com/roboDocs",
      "tags"          : ["demo", "test"],
      "icon"          : "http://github.com/roboDocs/rf-extension-boilerplate/blob/master/build/myExtension.roboFontExt/resources/icon.png"
    },
    /* ... more extensions here ... */
  ]
}
```

Extension streams can be created dynamically (by a server) by aggregating data from several single extension item files. See the [Mechanic 2 extension stream] for an example.

[Mechanic 2 extension stream]: http://robofontmechanic.com/api/v2/registry.json

> - {% internallink 'extensions/managing-extension-streams' %}
{: .seealso }


Private extensions
------------------

Extensions in private repositories are supported using **private access tokens**.

*We recommend [GitLab] for distributing private extensions with Mechanic 2.*

Here is an example of private extension item in `yaml` format, with private access tokens used for `infoPath`, `zipPath` and `icon`:

```yaml
extensionName: myExtension
description:   A boilerplate extension which serves as starting point for creating your own extensions.
repository:    https://gitlab.com/myUsername/boilerplate-extension
extensionPath: build/myExtension.roboFontExt
developer:     RoboDocs
developerURL:  http://github.com/roboDocs
tags:          [ demo, test ]
infoPath:      https://gitlab.com/api/v4/projects/projectID/repository/files/build/myExtension.roboFontExt%2Finfo.plist/raw?ref=master&private_token=myPrivateToken
icon:          https://gitlab.com/api/v4/projects/projectID/repository/files/myExtensionMechanicIcon.png/raw?ref=master&private_token=myPrivateToken
zipPath:       https://gitlab.com/api/v4/projects/projectID/repository/archive.zip?sha=master&private_token=myPrivateToken
```

`projectID`
: A number identifying your repository. You can find it in your project’s index page.

`myPrivateToken`
: A private access token for accessing files in your private repository.

### Creating private access tokens

You can create as many personal access tokens as you like from your GitLab profile.

1. Log in to [GitLab].
2. In the upper-right corner, click your avatar and select *Settings*.
3. On the *User Settings* menu, select *Access Tokens*.
4. Choose a name and optional expiry date for the token.
5. For Mechanic 2 access, choose `read_repository` as the token scope.
6. Click the *Create personal access token* button.
7. Save the personal access token somewhere safe. Once you leave or refresh the page, you won’t be able to access it again.

Access tokens can be revoked at any time using the same GitLab interface.

> - [Personal access tokens (GitLab)](http://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
{: .seealso }

[GitLab]: http://gitlab.com/
