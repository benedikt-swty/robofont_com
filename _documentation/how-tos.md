---
layout: page
title: How-Tos
---

*practical steps · problem-oriented · useful when working*

<div class='row'>
<div class='col' markdown='1'>

### Character set

- {% internallink 'how-tos/adding-and-removing-glyphs' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/building-accented-glyphs-with-glyph-construction' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/renaming-glyphs' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/custom-gnful-mapping' %} <span class='green'>✔︎</span>

### Variable fonts

- {% internallink 'how-tos/checking-interpolation-compatibility' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/creating-glyph-substitution-rules' %} <span class='green'>✔︎</span>

### Scripting

- {% internallink 'how-tos/adding-localized-name-table' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/adding-and-removing-glyphs-with-code' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/building-accented-glyphs-with-a-script' %} <span class='green'>✔︎</span>
<!-- - {% internallink 'how-tos/renaming-glyphs' %} -->

### Font generation

- {% internallink 'how-tos/hinting-ttfautohint' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/generating-woffs' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/generating-trial-fonts' %} <span class='green'>✔︎</span>

### Application & workspace

- {% internallink 'how-tos/installation' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/adding-custom-items-to-application-menu' %} <span class='green'>✔︎</span>

### Scripting environment

- {% internallink 'how-tos/overriding-embedded-libraries' %} <span class='green'>✔︎</span>

### Format conversions

- {% internallink 'how-tos/converting-between-ufo-versions' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/converting-from-cubic-to-quadratic' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/converting-from-glyphs-to-ufo' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/converting-from-opentype-to-ufo' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/converting-from-vfb-to-ufo' %} <span class='green'>✔︎</span>

### Documentation

- {% internallink 'how-tos/giving-feedback-on-docs' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/previewing-documentation-locally' %} <span class='green'>✔︎</span>

</div>
<div class='col' markdown='1'>

### DrawBot

- {% internallink 'how-tos/drawbot/drawbot-module' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/drawbot/drawbot-view' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/drawbot/proof-character-set' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/drawbot/proof-glyphs' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/drawbot/proof-words' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/drawbot/proof-features' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/drawbot/proof-spacing' %} <span class='green'>✔︎</span>

### vanilla

- {% internallink 'how-tos/vanilla/bulk-list' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/vanilla/window-with-move-sliders' %} <span class='green'>✔︎</span>

### mojo

- {% internallink 'how-tos/mojo/accordion-window' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/add-toolbar-item' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/decompose-point-pen' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/glyph-preview' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/help-window' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/subscribing-to-current-font' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/subscribing-to-current-glyph' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/subscribing-to-glyph-editor' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/subscribing-to-robofont-application' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/multiline-view' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/open-glyph-in-new-window' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/read-write-defaults' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/sort-fonts-list' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/smart-sets' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/mojo/space-center' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/updating-scripts-from-RF3-to-RF4' %} <span class='green'>✔︎</span>

### Interactive Tools

- {% internallink 'how-tos/interactive/simple-tool-example' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/interactive/polygonal-selection-tool' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/interactive/constrain-angle-tool' %} <span class='green'>✔︎</span>

### Events

- {% internallink 'how-tos/observers/custom-font-overview-contextual-menu' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/custom-glyph-preview' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/custom-glyph-window-display-menu' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/custom-inspector-panel' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/custom-label-in-glyph-cells' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/custom-space-center-preview' %} <span class='yellow'>✔︎</span>
- {% internallink 'how-tos/observers/draw-info-text-in-glyph-view' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/draw-reference-glyph' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/glyph-editor-subview' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/interactive-selection-distance' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/list-layers' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/multifont-glyph-preview' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/open-component-in-new-window' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/show-glyph-box' %} <span class='green'>✔︎</span>
- {% internallink 'how-tos/observers/stencil-preview' %} <span class='green'>✔︎</span>

### Extensions

- {% internallink 'extensions/getting-extensions' %} <span class='green'>✔︎</span>
- {% internallink 'extensions/using-extensions' %} <span class='green'>✔︎</span>
- {% internallink 'extensions/installing-extensions-mechanic' %} <span class='green'>✔︎</span>
- {% internallink 'extensions/installing-extensions-manually' % <span class='green'>✔︎</span>
- {% internallink 'extensions/managing-extension-streams' %} <span class='green'>✔︎</span>
- {% internallink 'extensions/building-extensions-with-extension-builder' %} <span class='green'>✔︎</span>
- {% internallink 'extensions/building-extensions-with-script' %} <span class='green'>✔︎</span>
- {% internallink 'extensions/publishing-extensions' %} <span class='green'>✔︎</span>

</div>
</div>